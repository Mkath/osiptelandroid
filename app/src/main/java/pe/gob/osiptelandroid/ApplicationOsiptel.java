package pe.gob.osiptelandroid;

import android.app.Application;

import pe.gob.osiptelandroid.R;

import uk.co.chrisjenx.calligraphy.CalligraphyConfig;

/**
 * Created by junior on 25/12/16.
 */

public class ApplicationOsiptel extends Application {

    @Override
    public void onCreate() {
        super.onCreate();
        CalligraphyConfig.initDefault(new CalligraphyConfig.Builder()
                .setDefaultFontPath("fonts/MuseoSans_500.otf")
                .setFontAttrId(R.attr.fontPath)
                .build()
        );


    }





}
