package pe.gob.osiptelandroid.utils;

import android.content.Context;
import android.webkit.WebView;
import android.webkit.WebViewClient;

public class AppWebViewClients extends WebViewClient {
    private ProgressDialogCustom mProgressDialogCustom;

    public AppWebViewClients(Context context) {
        mProgressDialogCustom = new ProgressDialogCustom(context, "Cargando...");
    }

    @Override
    public boolean shouldOverrideUrlLoading(WebView view, String url) {
        // TODO Auto-generated method stub
        mProgressDialogCustom.show();
        view.loadUrl(url);
        return true;
    }
    @Override
    public void onPageFinished(WebView view, String url) {
        // TODO Auto-generated method stub
        super.onPageFinished(view, url);
        mProgressDialogCustom.dismiss();
    }
}
