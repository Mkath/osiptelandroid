package pe.gob.osiptelandroid.utils;

import pe.gob.osiptelandroid.data.entities.APIError;
import pe.gob.osiptelandroid.data.remote.ServiceFactory;

import java.io.IOException;
import java.lang.annotation.Annotation;

import okhttp3.ResponseBody;
import retrofit2.Converter;
import retrofit2.Response;

public class ErrorUtils {

    public static APIError sigemParseError(Response<?> response) {

        Converter<ResponseBody, APIError> converter =
                ServiceFactory.sigemRetrofit()
                        .responseBodyConverter(APIError.class, new Annotation[0]);

        APIError error;

        try {
            error = converter.convert(response.errorBody());
        } catch (IOException e) {
            return new APIError();
        }

        return error;
    }

    public static APIError trasuParseError(Response<?> response) {

        Converter<ResponseBody, APIError> converter =
                ServiceFactory.trasuRetrofit()
                        .responseBodyConverter(APIError.class, new Annotation[0]);

        APIError error;

        try {
            error = converter.convert(response.errorBody());
        } catch (IOException e) {
            return new APIError();
        }

        return error;
    }

    public static APIError coberturaParserError(Response<?> response) {

        Converter<ResponseBody, APIError> converter =
                ServiceFactory.coberturaRetrofit()
                        .responseBodyConverter(APIError.class, new Annotation[0]);

        APIError error;

        try {
            error = converter.convert(response.errorBody());
        } catch (IOException e) {
            return new APIError();
        }

        return error;
    }

    public static APIError firstParserError(Response<?> response) {

        Converter<ResponseBody, APIError> converter =
                ServiceFactory.firstRetrofit()
                        .responseBodyConverter(APIError.class, new Annotation[0]);

        APIError error;

        try {
            error = converter.convert(response.errorBody());
        } catch (IOException e) {
            return new APIError();
        }

        return error;
    }
}
