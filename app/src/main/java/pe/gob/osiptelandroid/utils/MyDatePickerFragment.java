package pe.gob.osiptelandroid.utils;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import java.util.Calendar;

public class MyDatePickerFragment  extends DialogFragment {

    private DatePickerDialog.OnDateSetListener listener;
    private Calendar calendar;

    public static MyDatePickerFragment newInstance(DatePickerDialog.OnDateSetListener listener) {
        MyDatePickerFragment fragment = new MyDatePickerFragment();
        fragment.setListener(listener);
       /* Bundle bundle = new Bundle();
        bundle.putString("date", date );
        fragment.setArguments(bundle);*/
        return fragment;
    }

    public void setListener(DatePickerDialog.OnDateSetListener listener) {
        this.listener = listener;
    }


    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {

        final Calendar c = Calendar.getInstance();
        int year = c.get(Calendar.YEAR);
        int month = c.get(Calendar.MONTH);
        int day = c.get(Calendar.DAY_OF_MONTH);
        return new DatePickerDialog(getActivity(), listener, year, month, day);
    }

}