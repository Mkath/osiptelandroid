package pe.gob.osiptelandroid.data.entities;

import java.io.Serializable;

public class PreguntaEntity implements Serializable {

    private int idPregunta;
    private String descripcion;
    private String estado;
    private int idTipoPregunta;
    private String fechaRegistroPregunta;

    public int getIdPregunta() {
        return idPregunta;
    }

    public void setIdPregunta(int idPregunta) {
        this.idPregunta = idPregunta;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    public String getFechaRegistroPregunta() {
        return fechaRegistroPregunta;
    }

    public void setFechaRegistroPregunta(String fechaRegistroPregunta) {
        this.fechaRegistroPregunta = fechaRegistroPregunta;
    }

    public int getIdTipoPregunta() {
        return idTipoPregunta;
    }

    public void setIdTipoPregunta(int idTipoPregunta) {
        this.idTipoPregunta = idTipoPregunta;
    }
}
