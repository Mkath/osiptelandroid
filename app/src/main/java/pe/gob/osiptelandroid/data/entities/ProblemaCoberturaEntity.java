package pe.gob.osiptelandroid.data.entities;

import java.io.Serializable;

public class ProblemaCoberturaEntity implements Serializable {
    private int idTipoProblema;
    private String tipoProblema;

    public int getIdTipoProblema() {
        return idTipoProblema;
    }

    public void setIdTipoProblema(int idTipoProblema) {
        this.idTipoProblema = idTipoProblema;
    }

    public String getTipoProblema() {
        return tipoProblema;
    }

    public void setTipoProblema(String tipoProblema) {
        this.tipoProblema = tipoProblema;
    }
}
