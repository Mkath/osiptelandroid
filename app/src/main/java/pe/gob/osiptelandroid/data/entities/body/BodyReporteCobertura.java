package pe.gob.osiptelandroid.data.entities.body;

import java.io.Serializable;

public class BodyReporteCobertura implements Serializable {

    private String nombre;
    private String apellidoPaterno;
    private String apellidoMaterno;
    private String numeroTelefono;
    private String marcaEquipo;
    private String modeloEquipo;
    private String sistemaOperativo;
    private int idEmpresa;
    private String fechaReporte;
    private String correoElectronico;
    private int idProblema;
    private String departamento;
    private String provincia;
    private String distrito;
    private String localidad;
    private String marcaCelular;
    private String modeloCelular;
    private String númeroCelular;
    private String mac;
    private String nombreUsuario;
    private String idServicio;
    private String latitudReporte;
    private String longitudReporte;
    private String latitudUbicacionPersona;
    private String longitudUbicacionPersona;

    public String getIdServicio() {
        return idServicio;
    }

    public void setIdServicio(String idServicio) {
        this.idServicio = idServicio;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getApellidoPaterno() {
        return apellidoPaterno;
    }

    public void setApellidoPaterno(String apellidoPaterno) {
        this.apellidoPaterno = apellidoPaterno;
    }

    public String getApellidoMaterno() {
        return apellidoMaterno;
    }

    public void setApellidoMaterno(String apellidoMaterno) {
        this.apellidoMaterno = apellidoMaterno;
    }

    public String getNumeroTelefono() {
        return numeroTelefono;
    }

    public void setNumeroTelefono(String numeroTelefono) {
        this.numeroTelefono = numeroTelefono;
    }

    public String getMarcaEquipo() {
        return marcaEquipo;
    }

    public void setMarcaEquipo(String marcaEquipo) {
        this.marcaEquipo = marcaEquipo;
    }

    public String getModeloEquipo() {
        return modeloEquipo;
    }

    public void setModeloEquipo(String modeloEquipo) {
        this.modeloEquipo = modeloEquipo;
    }

    public String getSistemaOperativo() {
        return sistemaOperativo;
    }

    public void setSistemaOperativo(String sistemaOperativo) {
        this.sistemaOperativo = sistemaOperativo;
    }

    public int getIdEmpresa() {
        return idEmpresa;
    }

    public void setIdEmpresa(int idEmpresa) {
        this.idEmpresa = idEmpresa;
    }

    public String getFechaReporte() {
        return fechaReporte;
    }

    public void setFechaReporte(String fechaReporte) {
        this.fechaReporte = fechaReporte;
    }

    public String getCorreoElectronico() {
        return correoElectronico;
    }

    public void setCorreoElectronico(String correoElectronico) {
        this.correoElectronico = correoElectronico;
    }

    public int getIdProblema() {
        return idProblema;
    }

    public void setIdProblema(int idProblema) {
        this.idProblema = idProblema;
    }

    public String getDepartamento() {
        return departamento;
    }

    public void setDepartamento(String departamento) {
        this.departamento = departamento;
    }

    public String getProvincia() {
        return provincia;
    }

    public void setProvincia(String provincia) {
        this.provincia = provincia;
    }

    public String getDistrito() {
        return distrito;
    }

    public void setDistrito(String distrito) {
        this.distrito = distrito;
    }

    public String getLocalidad() {
        return localidad;
    }

    public void setLocalidad(String localidad) {
        this.localidad = localidad;
    }

    public String getMarcaCelular() {
        return marcaCelular;
    }

    public void setMarcaCelular(String marcaCelular) {
        this.marcaCelular = marcaCelular;
    }

    public String getModeloCelular() {
        return modeloCelular;
    }

    public void setModeloCelular(String modeloCelular) {
        this.modeloCelular = modeloCelular;
    }

    public String getNúmeroCelular() {
        return númeroCelular;
    }

    public void setNúmeroCelular(String númeroCelular) {
        this.númeroCelular = númeroCelular;
    }

    public String getMac() {
        return mac;
    }

    public void setMac(String mac) {
        this.mac = mac;
    }

    public String getNombreUsuario() {
        return nombreUsuario;
    }

    public void setNombreUsuario(String nombreUsuario) {
        this.nombreUsuario = nombreUsuario;
    }

    public String getLatitudReporte() {
        return latitudReporte;
    }

    public void setLatitudReporte(String latitudReporte) {
        this.latitudReporte = latitudReporte;
    }

    public String getLongitudReporte() {
        return longitudReporte;
    }

    public void setLongitudReporte(String longitudReporte) {
        this.longitudReporte = longitudReporte;
    }

    public String getLatitudUbicacionPersona() {
        return latitudUbicacionPersona;
    }

    public void setLatitudUbicacionPersona(String latitudUbicacionPersona) {
        this.latitudUbicacionPersona = latitudUbicacionPersona;
    }

    public String getLongitudUbicacionPersona() {
        return longitudUbicacionPersona;
    }

    public void setLongitudUbicacionPersona(String longitudUbicacionPersona) {
        this.longitudUbicacionPersona = longitudUbicacionPersona;
    }
}
