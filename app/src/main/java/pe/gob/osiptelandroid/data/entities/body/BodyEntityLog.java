package pe.gob.osiptelandroid.data.entities.body;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
public class BodyEntityLog {
    @SerializedName("sistemaOperativo")
    @Expose
    private String sistemaOperativo;
    @SerializedName("marcaCelular")
    @Expose
    private String marcaCelular;
    @SerializedName("modeloCelular")
    @Expose
    private String modeloCelular;
    @SerializedName("numeroCelular")
    @Expose
    private String numeroCelular;
    @SerializedName("accion")
    @Expose
    private String accion;
    @SerializedName("mac")
    @Expose
    private String mac;
    @SerializedName("nombreUsuario")
    @Expose
    private String nombreUsuario;
    public String getSistemaOperativo() {
        return sistemaOperativo;
    }
    public void setSistemaOperativo(String sistemaOperativo) {
        this.sistemaOperativo = sistemaOperativo;
    }
    public String getMarcaCelular() {
        return marcaCelular;
    }
    public void setMarcaCelular(String marcaCelular) {
        this.marcaCelular = marcaCelular;
    }
    public String getModeloCelular() {
        return modeloCelular;
    }
    public void setModeloCelular(String modeloCelular) {
        this.modeloCelular = modeloCelular;
    }
    public String getNumeroCelular() {
        return numeroCelular;
    }
    public void setNumeroCelular(String numeroCelular) {
        this.numeroCelular = numeroCelular;
    }
    public String getAccion() {
        return accion;
    }
    public void setAccion(String accion) {
        this.accion = accion;
    }
    public String getMac() {
        return mac;
    }
    public void setMac(String mac) {
        this.mac = mac;
    }
    public String getNombreUsuario() {
        return nombreUsuario;
    }
    public void setNombreUsuario(String nombreUsuario) {
        this.nombreUsuario = nombreUsuario;
    }
}