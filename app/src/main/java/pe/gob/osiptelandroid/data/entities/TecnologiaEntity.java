package pe.gob.osiptelandroid.data.entities;

import java.io.Serializable;
import java.util.ArrayList;

public class TecnologiaEntity implements Serializable {
    private int codEmpresa;
    private String empresa;
    private String tecnologia;
    private ArrayList<String> tecnologiaArray;

    public int getIdEmpresa() {
        return codEmpresa;
    }

    public void setIdEmpresa(int idEmpresa) {
        this.codEmpresa = idEmpresa;
    }

    public String getEmpresa() {
        return empresa;
    }

    public void setEmpresa(String empresa) {
        this.empresa = empresa;
    }

    public String getTecnologia() {
        return tecnologia;
    }

    public void setTecnologia(String tecnologia) {
        this.tecnologia = tecnologia;
    }

    public ArrayList<String> getTecnologiaArray() {
        return tecnologiaArray;
    }

    public void setTecnologiaArray(ArrayList<String> tecnologiaArray) {
        this.tecnologiaArray = tecnologiaArray;
    }
}
