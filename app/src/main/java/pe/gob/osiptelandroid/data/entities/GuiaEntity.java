package pe.gob.osiptelandroid.data.entities;

import android.graphics.drawable.Drawable;

public class GuiaEntity {
    private int idTipoReclamo;
    private String descripcion;
    private String nombreImagen;
    private int estado;
    private Drawable icon;
    private Drawable imagen;

    public GuiaEntity(int idTipoReclamo, Drawable icon, String descripcion) {
        this.idTipoReclamo = idTipoReclamo;
        this.descripcion = descripcion;
        this.icon = icon;
    }

    public Drawable getImagen() {
        return imagen;
    }

    public void setImagen(Drawable imagen) {
        this.imagen = imagen;
    }

    public int getIdTipoReclamo() {
        return idTipoReclamo;
    }

    public void setIdTipoReclamo(int idTipoReclamo) {
        this.idTipoReclamo = idTipoReclamo;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public String getNombreImagen() {
        return nombreImagen;
    }

    public void setNombreImagen(String nombreImagen) {
        this.nombreImagen = nombreImagen;
    }

    public int getEstado() {
        return estado;
    }

    public void setEstado(int estado) {
        this.estado = estado;
    }

    public Drawable getIcon() {
        return icon;
    }

    public void setIcon(Drawable icon) {
        this.icon = icon;
    }
}
