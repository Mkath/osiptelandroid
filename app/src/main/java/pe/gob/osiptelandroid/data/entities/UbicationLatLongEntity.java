package pe.gob.osiptelandroid.data.entities;

import java.io.Serializable;

public class UbicationLatLongEntity implements Serializable {
    private String longitud;
    private String latitud;

    public String getLongitud() {
        return longitud;
    }

    public void setLongitud(String longitud) {
        this.longitud = longitud;
    }

    public String getLatitud() {
        return latitud;
    }

    public void setLatitud(String latitud) {
        this.latitud = latitud;
    }
}
