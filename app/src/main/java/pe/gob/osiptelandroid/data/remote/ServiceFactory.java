package pe.gob.osiptelandroid.data.remote;

import android.util.Log;

import pe.gob.osiptelandroid.BuildConfig;

import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Service Factory for Retrofit
 */
public class ServiceFactory {

    public static final String API_BASE_URL = BuildConfig.BASE;
    public static final String COBERTURA_API_BASE_URL = BuildConfig.COBERTURA;
    public static final String SIGEM_API_BASE_URL = BuildConfig.SIGEM;
    public static final String TRASU_API_BASE_URL = BuildConfig.TRASU;
    public static final String IDENTITY_API_BASE_URL = BuildConfig.IDENTITY;

// set your desired log level

    private static OkHttpClient.Builder httpClient = new OkHttpClient.Builder();

    private static OkHttpClient.Builder okHttpClientTimeout = new OkHttpClient.Builder()
            .connectTimeout(30, TimeUnit.SECONDS);

    private static Retrofit.Builder builder =
            new Retrofit.Builder()
                    .baseUrl(API_BASE_URL)
                    .addConverterFactory(GsonConverterFactory.create());

    private static Retrofit.Builder coberturaBuilder =
            new Retrofit.Builder()
                    .baseUrl(COBERTURA_API_BASE_URL)
                    .addConverterFactory(GsonConverterFactory.create());

    private static Retrofit.Builder sigemBuilder =
            new Retrofit.Builder()
                    .baseUrl(SIGEM_API_BASE_URL)
                    .addConverterFactory(GsonConverterFactory.create());

    private static Retrofit.Builder trasuBuilder =
            new Retrofit.Builder()
                    .baseUrl(TRASU_API_BASE_URL)
                    .addConverterFactory(GsonConverterFactory.create());

    private static Retrofit.Builder tokenBuilder =
            new Retrofit.Builder()
                    .baseUrl(IDENTITY_API_BASE_URL)
                    .addConverterFactory(GsonConverterFactory.create());

    public static <S> S createTokenService(Class<S> serviceClass) {

        HttpLoggingInterceptor logging = new HttpLoggingInterceptor();
        logging.setLevel(HttpLoggingInterceptor.Level.BODY);

        /*CertificatePinner certificatePinner = new CertificatePinner.Builder().add(
                "www.example.com",
                "sha256/ZC3lTYTDBJQVf1P2V7+fibTqbIsWNR/X7CWNVW+CEEA="
        ).build();*/

        OkHttpClient client = new OkHttpClient.Builder().addInterceptor(logging)
                .build();
      //  Retrofit retrofit = tokenBuilder.client(httpClient.build()).build();
       Retrofit retrofit = tokenBuilder.client(httpClient.build()).client(client).build();

        return retrofit.create(serviceClass);
    }

    public static <S> S createService(Class<S> serviceClass) {
        HttpLoggingInterceptor logging = new HttpLoggingInterceptor();
        logging.setLevel(HttpLoggingInterceptor.Level.BODY);
        OkHttpClient client = new OkHttpClient.Builder().addInterceptor(logging)
                .build();
        Retrofit retrofit = builder.client(okHttpClientTimeout.build()).client(client).build();
      //  Retrofit retrofit = builder.client(httpClient.build()).build();

        return retrofit.create(serviceClass);
    }

    public static <S> S createCoberturaService(Class<S> serviceClass) {

        HttpLoggingInterceptor logging = new HttpLoggingInterceptor();
        logging.setLevel(HttpLoggingInterceptor.Level.BODY);
        OkHttpClient client = new OkHttpClient.Builder()
                .addInterceptor(logging)
                .connectTimeout(60, TimeUnit.SECONDS)
                .readTimeout(60, TimeUnit.SECONDS)
                .build();

        Retrofit retrofit = coberturaBuilder.client(httpClient.build()).client(client).build();
      //  Retrofit retrofit = coberturaBuilder.client(httpClient.build()).build();
        return retrofit.create(serviceClass);
    }

    public static <S> S createSigemService(Class<S> serviceClass) {

        HttpLoggingInterceptor logging = new HttpLoggingInterceptor();
        logging.setLevel(HttpLoggingInterceptor.Level.BODY);
        OkHttpClient client = new OkHttpClient.Builder()
                .addInterceptor(logging)
                .connectTimeout(120, TimeUnit.SECONDS)
                .readTimeout(120, TimeUnit.SECONDS)
                .build();;
      //  Retrofit retrofit = sigemBuilder.client(httpClient.build()).build();
        Retrofit retrofit = sigemBuilder.client(httpClient.build()).client(client).build();

        return retrofit.create(serviceClass);
    }

    public static <S> S createTrasuService(Class<S> serviceClass) {

        HttpLoggingInterceptor logging = new HttpLoggingInterceptor();
        logging.setLevel(HttpLoggingInterceptor.Level.BODY);
        OkHttpClient client = new OkHttpClient.Builder().addInterceptor(logging)
                .build();
     //   Retrofit retrofit = trasuBuilder.client(httpClient.build()).build();
        Retrofit retrofit = trasuBuilder.client(httpClient.build()).client(client).build();
        return retrofit.create(serviceClass);
    }


    public static
    Retrofit sigemRetrofit() {
        Retrofit retrofit =
                sigemBuilder
                        .client(
                                httpClient.build()
                        )
                        .build();
        return retrofit;
    }

    public static
    Retrofit trasuRetrofit() {
        Retrofit retrofit =
                trasuBuilder
                        .client(
                                httpClient.build()
                        )
                        .build();
        return retrofit;
    }

    public static
    Retrofit coberturaRetrofit() {
        Retrofit retrofit =
                coberturaBuilder
                        .client(
                                httpClient.build()
                        )
                        .build();
        return retrofit;
    }
    public static
    Retrofit firstRetrofit() {
        Retrofit retrofit =
                builder
                        .client(
                                httpClient.build()
                        )
                        .build();
        return retrofit;
    }

}
