package pe.gob.osiptelandroid.data.entities;

import java.io.Serializable;
import java.util.ArrayList;

public class CoberturaByOperadoraEntity implements Serializable {

    private String idUbigeo;
    private String latitud;
    private String longitud;
    private String lugar;
    private String cobertura;
    private String img;
    private String nroReporte;
    private String departamento;
    private String provincia;
    private String distrito;
    private String localidad;
    private boolean tieneCobertura;

    public boolean isTieneCobertura() {
        return tieneCobertura;
    }

    public void setTieneCobertura(boolean tieneCobertura) {
        this.tieneCobertura = tieneCobertura;
    }

    private ArrayList<TecnologiaEntity> tecnologisOperador;

    public ArrayList<TecnologiaEntity> getTecnologisOperador() {
        return tecnologisOperador;
    }

    public void setTecnologisOperador(ArrayList<TecnologiaEntity> tecnologisOperador) {
        this.tecnologisOperador = tecnologisOperador;
    }

    public String getDepartamento() {
        return departamento;
    }

    public void setDepartamento(String departamento) {
        this.departamento = departamento;
    }

    public String getProvincia() {
        return provincia;
    }

    public void setProvincia(String provincia) {
        this.provincia = provincia;
    }

    public String getDistrito() {
        return distrito;
    }

    public void setDistrito(String distrito) {
        this.distrito = distrito;
    }

    public String getLocalidad() {
        return localidad;
    }

    public void setLocalidad(String localidad) {
        this.localidad = localidad;
    }

    public String getIdUbigeo() {
        return idUbigeo;
    }

    public void setIdUbigeo(String idUbigeo) {
        this.idUbigeo = idUbigeo;
    }

    public String getLatitud() {
        return latitud;
    }

    public void setLatitud(String latitud) {
        this.latitud = latitud;
    }

    public String getLongitud() {
        return longitud;
    }

    public void setLongitud(String longitud) {
        this.longitud = longitud;
    }

    public String getLugar() {
        return lugar;
    }

    public void setLugar(String lugar) {
        this.lugar = lugar;
    }

    public String getCobertura() {
        return cobertura;
    }

    public void setCobertura(String cobertura) {
        this.cobertura = cobertura;
    }

    public String getImg() {
        return img;
    }

    public void setImg(String img) {
        this.img = img;
    }

    public String getNroReporte() {
        return nroReporte;
    }

    public void setNroReporte(String nroReporte) {
        this.nroReporte = nroReporte;
    }
}
