package pe.gob.osiptelandroid.data.entities;

import android.graphics.drawable.Drawable;

import java.io.Serializable;

public class MenuEntity implements Serializable {
    public int id;
    public Drawable icon;
    public String name;

    public MenuEntity(int id, Drawable icon, String name) {
        this.icon = icon;
        this.id = id;
        this.name = name;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Drawable getIcon() {
        return icon;
    }

    public void setIcon(Drawable icon) {
        this.icon = icon;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
