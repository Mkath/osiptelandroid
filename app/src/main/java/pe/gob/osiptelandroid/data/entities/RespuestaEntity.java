package pe.gob.osiptelandroid.data.entities;

import java.io.Serializable;

public class RespuestaEntity implements Serializable {
    public int idPregunta;
    public String valor;

    public RespuestaEntity(int idPregunta, String valor) {
        this.idPregunta = idPregunta;
        this.valor = valor;
    }

    public int getIdPregunta() {
        return idPregunta;
    }

    public void setIdPregunta(int idPregunta) {
        this.idPregunta = idPregunta;
    }

    public String getValor() {
        return valor;
    }

    public void setValor(String valor) {
        this.valor = valor;
    }
}
