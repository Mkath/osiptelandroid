package pe.gob.osiptelandroid.data.entities;

import java.io.Serializable;

public class ProvinciaEntity implements Serializable {
    private int codigo;
    private String provincia;


    public int getCodigo() {
        return codigo;
    }

    public void setCodigo(int codigo) {
        this.codigo = codigo;
    }

    public String getProvincia() {
        return provincia;
    }

    public void setProvincia(String provincia) {
        this.provincia = provincia;
    }
}
