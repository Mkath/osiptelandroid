package pe.gob.osiptelandroid.data.entities;

import java.io.Serializable;

public class ConsultaImeiEntity implements Serializable {
    private int idTermMov;
    private String ne;
    private String fechaHora;
    private String fechaHoraAchivo;
    private String reporte;
    private String pais;
    private String codOPerador;
    private String operador;
    private String tecnologia;

    public int getIdTermMov() {
        return idTermMov;
    }

    public void setIdTermMov(int idTermMov) {
        this.idTermMov = idTermMov;
    }

    public String getNe() {
        return ne;
    }

    public void setNe(String ne) {
        this.ne = ne;
    }

    public String getFechaHora() {
        return fechaHora;
    }

    public void setFechaHora(String fechaHora) {
        this.fechaHora = fechaHora;
    }

    public String getFechaHoraAchivo() {
        return fechaHoraAchivo;
    }

    public void setFechaHoraAchivo(String fechaHoraAchivo) {
        this.fechaHoraAchivo = fechaHoraAchivo;
    }

    public String getReporte() {
        return reporte;
    }

    public void setReporte(String reporte) {
        this.reporte = reporte;
    }

    public String getPais() {
        return pais;
    }

    public void setPais(String pais) {
        this.pais = pais;
    }

    public String getCodOPerador() {
        return codOPerador;
    }

    public void setCodOPerador(String codOPerador) {
        this.codOPerador = codOPerador;
    }

    public String getOperador() {
        return operador;
    }

    public void setOperador(String operador) {
        this.operador = operador;
    }

    public String getTecnologia() {
        return tecnologia;
    }

    public void setTecnologia(String tecnologia) {
        this.tecnologia = tecnologia;
    }
}
