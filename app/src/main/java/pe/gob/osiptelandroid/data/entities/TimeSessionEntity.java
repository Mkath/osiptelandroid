package pe.gob.osiptelandroid.data.entities;

public class TimeSessionEntity {
    private String tiempo;

    public String getTiempo() {
        return tiempo;
    }

    public void setTiempo(String tiempo) {
        this.tiempo = tiempo;
    }
}
