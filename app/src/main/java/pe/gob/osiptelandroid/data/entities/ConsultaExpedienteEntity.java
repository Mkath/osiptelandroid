package pe.gob.osiptelandroid.data.entities;

import java.io.Serializable;

public class ConsultaExpedienteEntity implements Serializable {
    private String idExpediente;
    private String recurso;
    private String estado;
    private String fechaIngreso;
    private String nroReclamo;
    private String empresa;

    public String getIdExpediente() {
        return idExpediente;
    }

    public void setIdExpediente(String idExpediente) {
        this.idExpediente = idExpediente;
    }

    public String getRecurso() {
        return recurso;
    }

    public void setRecurso(String recurso) {
        this.recurso = recurso;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    public String getFechaIngreso() {
        return fechaIngreso;
    }

    public void setFechaIngreso(String fechaIngreso) {
        this.fechaIngreso = fechaIngreso;
    }

    public String getNroReclamo() {
        return nroReclamo;
    }

    public void setNroReclamo(String nroReclamo) {
        this.nroReclamo = nroReclamo;
    }

    public String getEmpresa() {
        return empresa;
    }

    public void setEmpresa(String empresa) {
        this.empresa = empresa;
    }
}
