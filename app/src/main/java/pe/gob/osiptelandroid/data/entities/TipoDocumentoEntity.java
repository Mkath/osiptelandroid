package pe.gob.osiptelandroid.data.entities;

import java.io.Serializable;

public class TipoDocumentoEntity implements Serializable {
    private String idcodigo;
    private String descripcion;

    public String getIdcodigo() {
        return idcodigo;
    }

    public void setIdcodigo(String idcodigo) {
        this.idcodigo = idcodigo;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }
}
