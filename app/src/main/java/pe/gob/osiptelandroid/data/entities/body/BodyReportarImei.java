package pe.gob.osiptelandroid.data.entities.body;

public class BodyReportarImei {

    private String codigoImei;
    private int empresaOperadora;
    private String descripcionEmpresaOperadora;
    private String nombres;
    private String apellidoPaterno;
    private String apellidoMaterno;
    private int idMarca;
    private String descripcionMarca;
    private int idModelo;
    private String descripcionModelo;
    private String numeroMovil;
    private String codigoBloqueo;
    private String fechaReporte;
    private String fechaRecuperacion;
    private String numeroContacto;
    private int problemaDetectado;
    private String correoElectronico;
    private String guid;
    private String sistemaOperativo;
    private String marcaCelular;
    private String modeloCelular;
    private String númeroCelular;
    private String mac;
    private String nombreUsuario;


    public String getCodigoImei() {
        return codigoImei;
    }

    public void setCodigoImei(String codigoImei) {
        this.codigoImei = codigoImei;
    }

    public int getIdModelo() {
        return idModelo;
    }

    public void setIdModelo(int idModelo) {
        this.idModelo = idModelo;
    }

    public String getDescripcionModelo() {
        return descripcionModelo;
    }

    public void setDescripcionModelo(String descripcionModelo) {
        this.descripcionModelo = descripcionModelo;
    }

    public int getIdMarca() {
        return idMarca;
    }

    public void setIdMarca(int idMarca) {
        this.idMarca = idMarca;
    }

    public String getDescripcionMarca() {
        return descripcionMarca;
    }

    public void setDescripcionMarca(String descripcionMarca) {
        this.descripcionMarca = descripcionMarca;
    }

    public int getEmpresaOperadora() {
        return empresaOperadora;
    }

    public void setEmpresaOperadora(int empresaOperadora) {
        this.empresaOperadora = empresaOperadora;
    }

    public String getDescripcionEmpresaOperadora() {
        return descripcionEmpresaOperadora;
    }

    public void setDescripcionEmpresaOperadora(String descripcionEmpresaOperadora) {
        this.descripcionEmpresaOperadora = descripcionEmpresaOperadora;
    }

    public String getFechaReporte() {
        return fechaReporte;
    }

    public void setFechaReporte(String fechaReporte) {
        this.fechaReporte = fechaReporte;
    }

    public String getCodigoBloqueo() {
        return codigoBloqueo;
    }

    public void setCodigoBloqueo(String codigoBloqueo) {
        this.codigoBloqueo = codigoBloqueo;
    }

    public String getFechaRecuperacion() {
        return fechaRecuperacion;
    }

    public void setFechaRecuperacion(String fechaRecuperacion) {
        this.fechaRecuperacion = fechaRecuperacion;
    }

    public String getNumeroContacto() {
        return numeroContacto;
    }

    public void setNumeroContacto(String numeroContacto) {
        this.numeroContacto = numeroContacto;
    }


    public String getCorreoElectronico() {
        return correoElectronico;
    }

    public void setCorreoElectronico(String correoElectronico) {
        this.correoElectronico = correoElectronico;
    }

    public String getNombres() {
        return nombres;
    }

    public void setNombres(String nombres) {
        this.nombres = nombres;
    }

    public String getApellidoPaterno() {
        return apellidoPaterno;
    }

    public void setApellidoPaterno(String apellidoPaterno) {
        this.apellidoPaterno = apellidoPaterno;
    }

    public String getApellidoMaterno() {
        return apellidoMaterno;
    }

    public void setApellidoMaterno(String apellidoMaterno) {
        this.apellidoMaterno = apellidoMaterno;
    }

    public String getNumeroMovil() {
        return numeroMovil;
    }

    public void setNumeroMovil(String numeroMovil) {
        this.numeroMovil = numeroMovil;
    }


    public int getProblemaDetectado() {
        return problemaDetectado;
    }

    public void setProblemaDetectado(int problemaDetectado) {
        this.problemaDetectado = problemaDetectado;
    }

    public String getGuid() {
        return guid;
    }

    public void setGuid(String guid) {
        this.guid = guid;
    }

    public String getSistemaOperativo() {
        return sistemaOperativo;
    }

    public void setSistemaOperativo(String sistemaOperativo) {
        this.sistemaOperativo = sistemaOperativo;
    }

    public String getMarcaCelular() {
        return marcaCelular;
    }

    public void setMarcaCelular(String marcaCelular) {
        this.marcaCelular = marcaCelular;
    }

    public String getModeloCelular() {
        return modeloCelular;
    }

    public void setModeloCelular(String modeloCelular) {
        this.modeloCelular = modeloCelular;
    }

    public String getNúmeroCelular() {
        return númeroCelular;
    }

    public void setNúmeroCelular(String númeroCelular) {
        this.númeroCelular = númeroCelular;
    }

    public String getMac() {
        return mac;
    }

    public void setMac(String mac) {
        this.mac = mac;
    }

    public String getNombreUsuario() {
        return nombreUsuario;
    }

    public void setNombreUsuario(String nombreUsuario) {
        this.nombreUsuario = nombreUsuario;
    }
}
