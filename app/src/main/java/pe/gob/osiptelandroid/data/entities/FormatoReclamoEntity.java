package pe.gob.osiptelandroid.data.entities;


import java.io.Serializable;

public class FormatoReclamoEntity implements Serializable {
    private int idPlantillaReclamo;
    private String nombreArchivo;
    private String descripcion;
    private String descripcionPDF;
    private String descripcionWORD;

    public String getDescripcionPDF() {
        return descripcionPDF;
    }

    public void setDescripcionPDF(String descripcionPDF) {
        this.descripcionPDF = descripcionPDF;
    }

    public String getDescripcionWORD() {
        return descripcionWORD;
    }

    public void setDescripcionWORD(String descripcionWORD) {
        this.descripcionWORD = descripcionWORD;
    }

    public int getIdPlantillaReclamo() {
        return idPlantillaReclamo;
    }

    public void setIdPlantillaReclamo(int idPlantillaReclamo) {
        this.idPlantillaReclamo = idPlantillaReclamo;
    }

    public String getNombreArchivo() {
        return nombreArchivo;
    }

    public void setNombreArchivo(String nombreArchivo) {
        this.nombreArchivo = nombreArchivo;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

}
