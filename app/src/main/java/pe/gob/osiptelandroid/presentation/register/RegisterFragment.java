package pe.gob.osiptelandroid.presentation.register;

import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;
import android.support.annotation.Nullable;
import android.util.Patterns;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.mobsandgeeks.saripaar.ValidationError;
import com.mobsandgeeks.saripaar.Validator;
import com.mobsandgeeks.saripaar.annotation.Email;
import com.mobsandgeeks.saripaar.annotation.Length;
import com.mobsandgeeks.saripaar.annotation.NotEmpty;
import pe.gob.osiptelandroid.R;
import pe.gob.osiptelandroid.core.BaseFragment;
import pe.gob.osiptelandroid.data.entities.body.BodyUser;
import pe.gob.osiptelandroid.data.local.SessionManager;
import pe.gob.osiptelandroid.presentation.main.MainActivity;
import pe.gob.osiptelandroid.presentation.register.dialogs.DialogConfirm;
import pe.gob.osiptelandroid.utils.ProgressDialogCustom;
import pe.gob.osiptelandroid.utils.ValidateUtils;

import java.util.List;
import java.util.Timer;
import java.util.TimerTask;
import java.util.regex.Pattern;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;

/**
 * Created by junior on 27/08/16.
 */
public class RegisterFragment extends BaseFragment implements RegisterContract.View, Validator.ValidationListener {

    private static final String TAG = RegisterFragment.class.getSimpleName();
    @NotEmpty
    @Length(min = 9)
    @BindView(R.id.et_num_cel)
    EditText etNumCel;
    @NotEmpty
    @Email
    @BindView(R.id.et_correo_electronico)
    EditText etCorreoElectronico;
    @NotEmpty
    @BindView(R.id.et_nombres)
    EditText etNombres;
    @NotEmpty
    @BindView(R.id.et_ap_paterno)
    EditText etApPaterno;
    @NotEmpty
    @BindView(R.id.et_ap_materno)
    EditText etApMaterno;
    @BindView(R.id.button_ingresar)
    RelativeLayout buttonConsult;
    @BindView(R.id.tv_error_num_cel)
    TextView tvErrorNumCel;
    @BindView(R.id.tv_error_correo)
    TextView tvErrorCorreo;
    @BindView(R.id.tv_error_nombre)
    TextView tvErrorNombre;
    @BindView(R.id.tv_error_ap_paterno)
    TextView tvErrorApPaterno;
    @BindView(R.id.tv_error_ap_materno)
    TextView tvErrorApMaterno;
    Unbinder unbinder;
    private ProgressDialogCustom mProgressDialogCustom;
    private RegisterContract.Presenter mPresenter;
    private BodyUser bodyUser;
    private SessionManager mSessionManager;
    Validator validator;
    private ValidateUtils validateUtils;

    public RegisterFragment() {
        // Requires empty public constructor
    }

    public static RegisterFragment newInstance() {
        return new RegisterFragment();
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mPresenter = new RegisterPresenter(this, getContext());
        validator = new Validator(this);
        mSessionManager = new SessionManager(getContext());
        validateUtils = new ValidateUtils(getContext(), getActivity());
    }

    @Override
    public void onResume() {
        super.onResume();
    }


    @Override
    public void onStart() {
        super.onStart();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.register_fragment, container, false);
        unbinder = ButterKnife.bind(this, root);
        return root;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mProgressDialogCustom = new ProgressDialogCustom(getContext(), "Enviando información...");
        validator.setValidationListener(this);
        updateEditTextUi();
    }

    private void updateEditTextUi() {
        validateUtils.cleanUIRegister(etNumCel, tvErrorNumCel, true, 9);
        validateUtils.cleanUIRegister(etNombres, tvErrorNombre, false, 0);
        validateUtils.cleanUIRegister(etApPaterno, tvErrorApPaterno, false, 0);
        validateUtils.cleanUIRegister(etApMaterno, tvErrorApMaterno, false, 0);
        validateUtils.cleanUIRegister(etCorreoElectronico, tvErrorCorreo, false, 0);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }


    @Override
    public boolean isActive() {
        return isAdded();
    }

    @Override
    public void setPresenter(RegisterContract.Presenter presenter) {
        this.mPresenter = presenter;
    }

    @Override
    public void setLoadingIndicator(boolean active) {
        if (active) {
            mProgressDialogCustom.show();
        } else {
            if (mProgressDialogCustom.isShowing()) {
                mProgressDialogCustom.dismiss();
            }
        }
    }

    @Override
    public void showMessage(String message) {
        final DialogConfirm dialogConfirm = new DialogConfirm(getContext(), message, false, true);
        dialogConfirm.show();
        dialogConfirm.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialog) {
                mSessionManager.setEncuestaActiva(true);
                Intent intent = new Intent(getContext(), MainActivity.class);
                startActivity(intent);
                getActivity().finish();
            }
        });
        dialogConfirm.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));


        TimerTask task = new TimerTask() {
            @Override
            public void run() {
                dialogConfirm.dismiss();
            }
        };

        // Simulate a long loading process on application startup.
        Timer timer = new Timer();
        timer.schedule(task, 3000);
    }

    @Override
    public void showErrorMessage(String message) {
        DialogConfirm dialogConfirm = new DialogConfirm(getContext(), message, true, false);
        dialogConfirm.show();
        dialogConfirm.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
    }


    @Override
    public void onDestroy() {
        super.onDestroy();
        getActivity().finish();
    }

    @OnClick(R.id.button_ingresar)
    public void onViewClicked() {
        setUI();
        if (!validarEmail(etCorreoElectronico.getText().toString())) {
            tvErrorCorreo.setVisibility(View.VISIBLE);
            tvErrorCorreo.setText("Por favor ingrese un formato correcto de correo electrónico");
        }
        validator.validate();
        if(isOpenKeyboard()){
            hideKeyboard();
        }
    }

    private boolean validarEmail(String email) {
        Pattern pattern = Patterns.EMAIL_ADDRESS;
        return pattern.matcher(email).matches();
    }

    private void setUI() {
        tvErrorNumCel.setVisibility(View.GONE);
        tvErrorCorreo.setVisibility(View.GONE);
        tvErrorNombre.setVisibility(View.GONE);
        tvErrorApMaterno.setVisibility(View.GONE);
        tvErrorApPaterno.setVisibility(View.GONE);
    }

    @Override
    public void onValidationSucceeded() {

            if (!validarEmail(etCorreoElectronico.getText().toString())) {
                tvErrorCorreo.setVisibility(View.VISIBLE);
                tvErrorCorreo.setText("Por favor ingrese un formato correcto de correo electrónico");
            } else {
                bodyUser = new BodyUser();
                bodyUser.setNroTelefono(etNumCel.getText().toString());
                bodyUser.setNombre(etNombres.getText().toString());
                bodyUser.setApellidoPaterno(etApPaterno.getText().toString());
                bodyUser.setApellidoMaterno(etApMaterno.getText().toString());
                bodyUser.setUsuarioRegistro(etNombres.getText().toString() + " " + etApPaterno.getText().toString() + " " + etApMaterno.getText().toString());
                bodyUser.setCorreoElectronico(etCorreoElectronico.getText().toString());
                bodyUser.setMarcaCelular(Build.BRAND);
                bodyUser.setModeloCelular(Build.MODEL);
                String androidId = Settings.Secure.getString(getActivity().getContentResolver(),
                        Settings.Secure.ANDROID_ID);
                bodyUser.setMac(androidId);
                mPresenter.sendRegister(bodyUser);
            }
    }


    @Override
    public void onValidationFailed(List<ValidationError> errors) {
        String msg = "Por favor ingrese sus datos correctamente";
        Toast.makeText(getContext(), msg, Toast.LENGTH_LONG).show();

        for (ValidationError error : errors) {
            View view = error.getView();
            String message = error.getCollatedErrorMessage(getContext());

            // Display error messages ;)
            if (view instanceof EditText) {

                if (view.getId() == R.id.et_correo_electronico) {
                    tvErrorCorreo.setVisibility(View.VISIBLE);
                    if (((EditText) view).length() != 0) {
                        tvErrorCorreo.setText("Por favor ingrese un formato correcto de correo electrónico");
                    } else {
                        tvErrorCorreo.setText("Este campo no puede estar vacío");
                    }
                }
                if (view.getId() == R.id.et_num_cel) {
                    tvErrorNumCel.setVisibility(View.VISIBLE);
                    if (((EditText) view).length() != 0) {
                        tvErrorNumCel.setText("Debe tener mínimo 9 dígitos");
                    } else {
                        tvErrorNumCel.setText("Este campo no puede estar vacío");
                    }
                }

                if (view.getId() == R.id.et_nombres) {
                    tvErrorNombre.setVisibility(View.VISIBLE);
                }
                if (view.getId() == R.id.et_ap_paterno) {
                    tvErrorApPaterno.setVisibility(View.VISIBLE);
                }
                if (view.getId() == R.id.et_ap_materno) {
                    tvErrorApMaterno.setVisibility(View.VISIBLE);
                }

            } else {
                Toast.makeText(getContext(), message, Toast.LENGTH_LONG).show();

            }
        }

    }
}
