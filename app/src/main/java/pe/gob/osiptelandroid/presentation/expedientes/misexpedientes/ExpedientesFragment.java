package pe.gob.osiptelandroid.presentation.expedientes.misexpedientes;

import android.Manifest;
import android.annotation.TargetApi;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.DownloadManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.FileProvider;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.MimeTypeMap;
import android.webkit.URLUtil;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import pe.gob.osiptelandroid.R;
import pe.gob.osiptelandroid.core.BaseFragment;
import pe.gob.osiptelandroid.data.entities.ConsultaExpedienteEntity;
import pe.gob.osiptelandroid.data.entities.EstadoEntity;
import pe.gob.osiptelandroid.data.entities.UserTrasuEntity;
import pe.gob.osiptelandroid.data.local.SessionManager;
import pe.gob.osiptelandroid.presentation.expedientes.ConsultarExpedienteFragment;
import pe.gob.osiptelandroid.presentation.expedientes.dialogs.CerrarSesionDialog;
import pe.gob.osiptelandroid.presentation.expedientes.dialogs.ExpedientesInterface;
import pe.gob.osiptelandroid.presentation.expedientes.dialogs.FilterDialog;
import pe.gob.osiptelandroid.presentation.register.dialogs.DialogConfirm;
import pe.gob.osiptelandroid.utils.ActivityUtils;
import pe.gob.osiptelandroid.utils.ProgressDialogCustom;

import java.io.File;
import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;

/**
 * Created by junior on 27/08/16.
 */
public class ExpedientesFragment extends BaseFragment implements ExpedientesInterface, ExpedientesContract.View {

    private static final String TAG = ExpedientesFragment.class.getSimpleName();
    public static final int MY_PERMISSIONS_REQUEST_WRITE_EXTERNAL_STORAGE = 1024;

    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.rv_list)
    RecyclerView rvList;
    Unbinder unbinder;
    @BindView(R.id.im_filter)
    ImageView imFilter;
    @BindView(R.id.noListIcon)
    ImageView noListIcon;
    @BindView(R.id.noListMain)
    TextView noListMain;
    @BindView(R.id.noList)
    LinearLayout noList;

    private ExpedienteAdapter mAdapter;
    private LinearLayoutManager mlinearLayoutManager;
    private SessionManager mSessionManager;
    private CerrarSesionDialog cerrarSesionDialog;
    private activateButtonsExpedientes mCallback;
    private FilterDialog filterDialog;
    private ExpedientesContract.Presenter mPresenter;
    private ProgressDialogCustom mProgressDialogCustom;
    private ProgressDialogCustom mProgressLoadingPdf;

    private String correo, contrasenia;
    private ArrayList<ConsultaExpedienteEntity> list;
    private ArrayList<EstadoEntity> listEstados;
    private String nroDoc, idUsuario, nombreCompleto, urlExp, idExpediente;
    String nameOfFile;

    public ExpedientesFragment() {
        // Requires empty public constructor
    }


    public interface activateButtonsExpedientes {
        void sendActivateExpedientesButtons(Boolean isActive);
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);

        // This makes sure that the container activity has implemented
        // the callback interface. If not, it throws an exception
        try {
            mCallback = (activateButtonsExpedientes) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement TextClicked");
        }
    }

    @Override
    public void onDetach() {
        mCallback = null; // => avoid leaking, thanks @Deepscorn
        super.onDetach();
    }


    public static ExpedientesFragment newInstance(Bundle bundle) {
        ExpedientesFragment fragment = new ExpedientesFragment();
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mSessionManager = new SessionManager(getContext());
        mPresenter = new ExpedientesPresenter(this, getContext());
        nroDoc = getArguments().getString("nroDoc");
        idUsuario = getArguments().getString("idUsuario");
        nombreCompleto = getArguments().getString("nombreCompleto");
        getActivity().registerReceiver(onComplete, new IntentFilter(DownloadManager.ACTION_DOWNLOAD_COMPLETE));
        list = (ArrayList<ConsultaExpedienteEntity>) getArguments().get(ConsultarExpedienteFragment.LISTA_EXPEDIENTES);
    }

    BroadcastReceiver onComplete = new BroadcastReceiver() {
        public void onReceive(Context context, Intent intent) {
            // Do Something
            try {
                DownloadManager.Query query = new DownloadManager.Query();
                query.setFilterById(intent.getLongExtra(DownloadManager.EXTRA_DOWNLOAD_ID, 0));
                DownloadManager manager = (DownloadManager) context.getSystemService(Context.DOWNLOAD_SERVICE);
                Cursor cursor = manager.query(query);
                while (cursor.moveToNext()) {
                    if (cursor.getCount() > 0) {
                        int status = cursor.getInt(cursor.getColumnIndex(DownloadManager.COLUMN_STATUS));
                        if (status == DownloadManager.STATUS_SUCCESSFUL) {
                            String file = cursor.getString(cursor.getColumnIndex(DownloadManager.COLUMN_LOCAL_URI));
                            if (file != null) {
                                File mFile = new File(Uri.parse(file).getPath());
                                if (mFile.exists()) {
                                    Toast.makeText(getContext(), "Archivo descargado", Toast.LENGTH_LONG).show();
                                    //here you can open your pdf
                              /*  1intent = new Intent(Intent.ACTION_VIEW);
                                Uri photoURI = FileProvider.getUriForFile(getContext(),
                                        getContext().getApplicationContext().getPackageName() + ".com.osiptel.osiptelApp.provider",mFile);

                                1intent.setDataAndType(photoURI, "application/pdf");
                                1intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
                               1intent.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
                                startActivity(1intent);*/
                                    if (Build.VERSION.SDK_INT < 24) {
                                        intent = new Intent(Intent.ACTION_VIEW);
                                        intent.setDataAndType(Uri.fromFile(mFile), "application/pdf");
                                        intent.setFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
                                    } else {
                                        intent = new Intent(Intent.ACTION_VIEW);
                                        Uri photoURI = FileProvider.getUriForFile(getContext(),
                                                getContext().getApplicationContext().getPackageName() + ".com.gob.osiptelandroid.provider", mFile);
                                        intent.setDataAndType(photoURI, "application/pdf");
                                        intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
                                        intent.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
                                        startActivity(intent);
                                    }
                                    try {
                                        if (intent.resolveActivity(getActivity().getPackageManager()) != null)
                                            startActivity(intent);
                                        else
                                            Toast.makeText(context, "No se encontraron aplicaciones para abrir el pdf", Toast.LENGTH_SHORT).show();
                                    } catch (Exception e) {
                                        Toast.makeText(context, e.getMessage(), Toast.LENGTH_SHORT).show();
                                    }
                                }
                            }
                            // So something here on success
                        } else if (status == DownloadManager.STATUS_FAILED) {
                            int message = cursor.getInt(cursor.getColumnIndex(DownloadManager.COLUMN_REASON));
                            // So something here on failed.
                            Toast.makeText(getActivity(), "El archivo no se encuentra disponible", Toast.LENGTH_LONG).show();
                        }
                    }
                }
            }catch(Exception e){

            }
        }
    };

    @Override
    public void onResume() {
        super.onResume();
        mPresenter.getEstados();
        if (!mSessionManager.getLoginTrasu()) {
            remove();
        }
    }


    @Override
    public void onStart() {
        super.onStart();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.expedientes_fragment, container, false);
        unbinder = ButterKnife.bind(this, root);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                closeDialog();
            }
        });
        mProgressDialogCustom = new ProgressDialogCustom(getContext(), "Filtrando...");
        mProgressLoadingPdf = new ProgressDialogCustom(getContext(), "Obteniendo Expediente...");

        mSessionManager.setLoginTrasu(true);
        return root;
    }

    public void closeDialog() {
        cerrarSesionDialog = new CerrarSesionDialog(getContext(), this);
        cerrarSesionDialog.show();
        cerrarSesionDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

    }

    @Override
    public void setRetainInstance(boolean retain) {
        super.setRetainInstance(retain);
    }

    void remove() {
        mCallback.sendActivateExpedientesButtons(true);
        ActivityUtils.removeFragment(getActivity().getSupportFragmentManager(),
                this);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        //mAdapter = new VerReportesAdapter(getList());
        mAdapter = new ExpedienteAdapter(new ArrayList<ConsultaExpedienteEntity>(), getContext(), this);
        mlinearLayoutManager = new LinearLayoutManager(getContext());
        mlinearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        rvList.setAdapter(mAdapter);
        rvList.setLayoutManager(mlinearLayoutManager);
        mAdapter.setItems(list);
        // getNewList();
    }


    @OnClick(R.id.im_filter)
    public void onViewClicked() {
        if (listEstados != null) {
            filterDialog = new FilterDialog(getContext(), this, listEstados, getActivity().getSupportFragmentManager());
            filterDialog.show();
            filterDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        }
    }


    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @Override
    public void closeSession() {
        remove();
        mSessionManager.setLoginTrasu(false);
        if(mSessionManager.getUserTrasu()!=null){
            UserTrasuEntity userTrasuEntity = new UserTrasuEntity();
            userTrasuEntity = mSessionManager.getUserTrasu();
            userTrasuEntity.setNroDoc("");
            mSessionManager.setUserTrasu(userTrasuEntity);
        }
        cerrarSesionDialog.dismiss();
    }

    @Override
    public void filterAction(String desde, String hasta, String estado, String palabra) {
        filterDialog.dismiss();
        if (mSessionManager.getUserEntity() != null) {
            mPresenter.getMisExpedientes(nroDoc, estado, desde, hasta,
                    mSessionManager.getUserEntity().getIdUsuario()+"",
                    mSessionManager.getUserEntity().getNroTelefono());
        } else {
            mPresenter.getMisExpedientes(nroDoc, estado, desde, hasta,
                    " ",
                    " ");
        }
        //get Filter
    }

    @Override
    public void getExpedientePDF(String idExpediente) {
        this.idExpediente = idExpediente;
        if (checkSelfPermission()) {
            mPresenter.getMyExpedientePDF(nombreCompleto, nroDoc, idUsuario, idExpediente);
        }
    }


    @Override
    public void isLogin(UserTrasuEntity userTrasuEntity) {
    }

    @Override
    public void getListMisExpedientes(ArrayList<ConsultaExpedienteEntity> list) {
        mAdapter.setItems(list);
        noList.setVisibility(View.GONE);

        if (list != null && mAdapter != null) {
            mAdapter.setItems(list);

            if (list.size() > 0) {
                noList.setVisibility(View.GONE);
            } else {
                noList.setVisibility(View.VISIBLE);
            }

        } else {
            noList.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void getMyExpediente(String urlExpediente) {
        urlExp = urlExpediente;
        if (checkSelfPermission()) {
            if (urlExpediente != null) {
                downloadFile(urlExpediente);
            } else {
                Toast.makeText(getContext(), "El expediente no se encuentra disponible", Toast.LENGTH_SHORT).show();
            }
        }
    }

    @Override
    public void loadingPdf(boolean isLoading) {
        if (isLoading) {
            mProgressLoadingPdf.show();
        } else {
            if (mProgressLoadingPdf.isShowing()) {
                mProgressLoadingPdf.dismiss();
            }
        }
    }

    @Override
    public void onPause() {
        super.onPause();
        if (filterDialog != null) {
            filterDialog.dismiss();
        }
        if (cerrarSesionDialog != null) {
            cerrarSesionDialog.dismiss();
        }
    }

    private boolean checkSelfPermission() {
        int currentAPIVersion = Build.VERSION.SDK_INT;
        if (currentAPIVersion >= Build.VERSION_CODES.M) {
            if (ContextCompat.checkSelfPermission(getContext(), Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                if (ActivityCompat.shouldShowRequestPermissionRationale((Activity) getContext(), Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
                    AlertDialog.Builder alertBuilder = new AlertDialog.Builder(getContext());
                    alertBuilder.setCancelable(true);
                    alertBuilder.setTitle("Permisos Necesarios!");
                    alertBuilder.setMessage("Para descargar el archivo es necesario brindar los permisos correspondientes");
                    alertBuilder.setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                        @TargetApi(Build.VERSION_CODES.JELLY_BEAN)
                        public void onClick(DialogInterface dialog, int which) {
                            requestPermissions(new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, MY_PERMISSIONS_REQUEST_WRITE_EXTERNAL_STORAGE);
                        }
                    });
                    AlertDialog alert = alertBuilder.create();
                    alert.show();
                } else {
                    requestPermissions(new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, MY_PERMISSIONS_REQUEST_WRITE_EXTERNAL_STORAGE);
                }
                return false;
            } else {
                return true;
            }
        } else {
            return true;
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode) {
            case MY_PERMISSIONS_REQUEST_WRITE_EXTERNAL_STORAGE:
                if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    mPresenter.getMyExpedientePDF(nombreCompleto, nroDoc, idUsuario, idExpediente);
                } else {
                    Toast.makeText(getContext(), "Debe aceptar los permisos para iniciar la descarga", Toast.LENGTH_SHORT).show();
                }
                break;
        }
    }


    public void downloadFile(String url) {
        DownloadManager.Request request = new DownloadManager.Request(Uri.parse(url));
        request.setTitle("Descargando archivo");
        request.setDescription("File is being downloaded...");
        request.allowScanningByMediaScanner();
        nameOfFile = URLUtil.guessFileName(url, null, MimeTypeMap.getFileExtensionFromUrl(url));
        File file = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS).getPath(), nameOfFile);
        request.setNotificationVisibility(DownloadManager.Request.VISIBILITY_VISIBLE_NOTIFY_COMPLETED);
        request.setDestinationUri(Uri.fromFile(file));
        DownloadManager manager = (DownloadManager) getContext().getSystemService(Context.DOWNLOAD_SERVICE);
        manager.enqueue(request);
    }

    @Override
    public void getListEstados(ArrayList<EstadoEntity> list) {
        listEstados = new ArrayList<>();
        listEstados = list;
    }

    @Override
    public boolean isActive() {
        return isAdded();
    }

    @Override
    public void setPresenter(ExpedientesContract.Presenter presenter) {
        this.mPresenter = presenter;
    }

    @Override
    public void setLoadingIndicator(boolean active) {
        if (active) {
            mProgressDialogCustom.show();
        } else {
            if (mProgressDialogCustom.isShowing()) {
                mProgressDialogCustom.dismiss();
            }
        }
    }

    @Override
    public void showMessage(String message) {

    }

    @Override
    public void showErrorMessage(String message) {
        DialogConfirm dialogConfirm = new DialogConfirm(getContext(), message, true, false);
        dialogConfirm.show();
        dialogConfirm.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialog) {
                mCallback.sendActivateExpedientesButtons(true);
            }
        });
        dialogConfirm.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
    }


}
