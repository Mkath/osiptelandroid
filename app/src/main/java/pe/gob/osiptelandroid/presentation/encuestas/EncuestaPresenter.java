package pe.gob.osiptelandroid.presentation.encuestas;

import android.content.Context;

import pe.gob.osiptelandroid.data.entities.APIError;
import pe.gob.osiptelandroid.data.entities.AccessTokenEntity;
import pe.gob.osiptelandroid.data.entities.body.BodyEncuestaEntity;
import pe.gob.osiptelandroid.data.entities.PreguntaEntity;
import pe.gob.osiptelandroid.data.local.SessionManager;
import pe.gob.osiptelandroid.data.remote.ServiceFactory;
import pe.gob.osiptelandroid.data.remote.request.ListRequest;
import pe.gob.osiptelandroid.data.remote.request.LoginRequest;
import pe.gob.osiptelandroid.data.remote.request.PostRequest;
import pe.gob.osiptelandroid.utils.ErrorUtils;

import java.util.ArrayList;

import okhttp3.MediaType;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by katherine on 28/06/17.
 */

public class EncuestaPresenter implements EncuestaContract.Presenter {

    private EncuestaContract.View mView;
    private Context context;
    private SessionManager mSessionManager;

    public EncuestaPresenter(EncuestaContract.View mView, Context context) {
        this.context = context;
        this.mView = mView;
        this.mView.setPresenter(this);
        mSessionManager = new SessionManager(context);
    }


    @Override
    public void start() {

    }

    @Override
    public void getPreguntas() {
      //  mView.showMessage(imei);
        mView.setLoadingIndicator(true);
        ListRequest listRequest = ServiceFactory.createService(ListRequest.class);
        Call<ArrayList<PreguntaEntity>> orders = listRequest.getPreguntas("Bearer "+mSessionManager.getUserToken());
        orders.enqueue(new Callback<ArrayList<PreguntaEntity>>() {
            @Override
            public void onResponse(Call<ArrayList<PreguntaEntity>> call, Response<ArrayList<PreguntaEntity>> response) {

                if (!mView.isActive()) {
                    return;
                }
                mView.setLoadingIndicator(false);
                if (response.isSuccessful()) {
                    mView.getListPreguntas(response.body());
                } else {
                    APIError apiError = ErrorUtils.firstParserError(response);
                    switch (response.code()){
                        case 400:
                            mView.showErrorMessage(apiError.getMessages().get(0));
                            break;
                        case 401:
                            getRefreshToken();
                            break;
                        case 500:
                            mView.showErrorMessage("Error al enviar su encuesta, intente nuevamente por favor");
                            break;
                        default:
                            mView.showErrorMessage("Error desconocido");
                            break;
                    }                }
            }

            @Override
            public void onFailure(Call<ArrayList<PreguntaEntity>> call, Throwable t) {
                if (!mView.isActive()) {
                    return;
                }
                mView.setLoadingIndicator(false);
                mView.showErrorMessage("Error al conectar con el servidor");
            }
        });
    }

    @Override
    public void setEncuesta(BodyEncuestaEntity encuesta) {
        mView.setLoadingIndicator(true);
        PostRequest postRequest = ServiceFactory.createService(PostRequest.class);
        Call<Void> orders = postRequest.sendEncuesta("Bearer "+mSessionManager.getUserToken(),encuesta);
        orders.enqueue(new Callback<Void>() {
            @Override
            public void onResponse(Call<Void> call, Response<Void> response) {

                if (!mView.isActive()) {
                    return;
                }
                mView.setLoadingIndicator(false);
                if (response.isSuccessful()) {
                    if (response.code() == 200){
                        mView.showMessage("Su encuesta fue enviada con éxito");
                    }else {
                        APIError apiError = ErrorUtils.firstParserError(response);
                        switch (response.code()){
                            case 400:
                                mView.showErrorMessage(apiError.getMessages().get(0));
                                break;
                            case 401:
                                getRefreshToken();
                                break;
                            case 500:
                                mView.showErrorMessage("Error al enviar su encuesta, intente nuevamente por favor");
                                break;
                            default:
                                mView.showErrorMessage("Error desconocido");
                                break;
                        }

                    }

                } else {
                    mView.showErrorMessage("Error al enviar su encuesta, intente nuevamente por favor");
                }
            }

            @Override
            public void onFailure(Call<Void> call, Throwable t) {
                if (!mView.isActive()) {
                    return;
                }
                mView.setLoadingIndicator(false);
                mView.showErrorMessage("Error al conectar con el servidor, intente nuevamente");
            }
        });
    }

    @Override
    public void getRefreshToken() {
        String text = "client_id=OsitelApp&client_secret=d60d6b88-bf36-463f-ae90-b423188bf1bf&grant_type=client_credentials";
        RequestBody body =
                RequestBody.create(MediaType.parse("application/x-www-form-urlencoded"), text);
        LoginRequest loginRequest = ServiceFactory.createTokenService(LoginRequest.class);
        Call<AccessTokenEntity> orders = loginRequest.login(body);
        orders.enqueue(new Callback<AccessTokenEntity>() {
            @Override
            public void onResponse(Call<AccessTokenEntity> call, Response<AccessTokenEntity> response) {
                if (!mView.isActive()) {
                    return;
                }

                if (response.isSuccessful()) {
                    mSessionManager.setUserToken(response.body().getAccess_token());
                    getPreguntas();
                }else{

                }
            }

            @Override
            public void onFailure(Call<AccessTokenEntity> call, Throwable t) {
                if (!mView.isActive()) {
                    return;
                }
                mView.showErrorMessage("Error al conectar con el servidor");
            }
        });
    }
}
