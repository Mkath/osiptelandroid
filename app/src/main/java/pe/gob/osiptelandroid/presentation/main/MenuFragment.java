package pe.gob.osiptelandroid.presentation.main;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.customtabs.CustomTabsIntent;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import pe.gob.osiptelandroid.R;
import pe.gob.osiptelandroid.core.BaseFragment;
import pe.gob.osiptelandroid.data.entities.MenuEntity;
import pe.gob.osiptelandroid.data.entities.body.BodyEntityLog;
import pe.gob.osiptelandroid.data.local.SessionManager;
import pe.gob.osiptelandroid.presentation.encuestas.EncuestaFragment;
import pe.gob.osiptelandroid.presentation.guia.GuiaFragment;
import pe.gob.osiptelandroid.presentation.verficarlinea.VerificarLineaFragment;
import pe.gob.osiptelandroid.utils.ActivityUtils;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import pe.gob.osiptelandroid.utils.AppConstants;
import pe.gob.osiptelandroid.utils.CustomTabActivityHelper;
import pe.gob.osiptelandroid.utils.LogContract;
import pe.gob.osiptelandroid.utils.LogPresenter;
import pe.gob.osiptelandroid.utils.ProgressDialogCustom;
import pe.gob.osiptelandroid.utils.WebviewFallback;

/**
 * Created by junior on 27/08/16.
 */
public class MenuFragment extends BaseFragment implements MenuItem, MenuContract.View{

    private static final String TAG = MenuFragment.class.getSimpleName();
    @BindView(R.id.rv_list)
    RecyclerView rvList;
    private MenuAdapter mAdapter;
    private GridLayoutManager mlinearLayoutManager;

    private ProgressDialogCustom mProgressDialogCustom;
    private MenuContract.Presenter mPresenter;
    private SessionManager mSessionManager;
    private BodyEntityLog bodyEntityLog;
    private LogContract.Presenter mPresenterLog;

    public MenuFragment() {
        // Requires empty public constructor
    }

    public static MenuFragment newInstance() {
        return new MenuFragment();
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mPresenter = new MenuPresenter(this, getContext());
        mSessionManager = new SessionManager(getContext());

        mProgressDialogCustom = new ProgressDialogCustom(getContext(), "Espere...");
        //Get urls
        mPresenter.getEnlacesAppMovil(getResources().getString(R.string.name_application),getResources().getString(R.string.name_global_comparatel));
        mPresenter.getEnlacesAppMovil(getResources().getString(R.string.name_application),getResources().getString(R.string.name_global_comparamovil));
        mPresenter.getEnlacesAppMovil(getResources().getString(R.string.name_application),getResources().getString(R.string.name_global_punku));

        mPresenterLog = new LogPresenter( getContext());
        mSessionManager = new SessionManager( getContext() );
        bodyEntityLog = new BodyEntityLog();
    }

    @Override
    public void onResume() {
        super.onResume();
        getList();
    }


    @Override
    public void onStart() {
        super.onStart();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.menu_fragment, container, false);
        ButterKnife.bind(this, root);

        return root;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mAdapter = new MenuAdapter(new ArrayList<MenuEntity>(), getContext(), (MenuItem) this);
        mlinearLayoutManager = new GridLayoutManager(getContext(), 2);
        mlinearLayoutManager.setOrientation(GridLayoutManager.VERTICAL);
        rvList.setAdapter(mAdapter);
        rvList.setLayoutManager(mlinearLayoutManager);
    }

    private void getList() {
        ArrayList<MenuEntity> list = new ArrayList<>();
        list.add(new MenuEntity(1, getResources().getDrawable(R.drawable.ic_consulta_imei), getResources().getString(R.string.consulta_imei)));
        list.add(new MenuEntity(2, getResources().getDrawable(R.drawable.ic_signal_osiptel), getResources().getString(R.string.senial_osiptel)));
        list.add(new MenuEntity(4, getResources().getDrawable(R.drawable.ic_expedietes), getResources().getString(R.string.expedientes)));
        list.add(new MenuEntity(5, getResources().getDrawable(R.drawable.ic_guia_informativa), getResources().getString(R.string.guia)));
        list.add(new MenuEntity(6, getResources().getDrawable(R.drawable.ic_verificar_linea), getResources().getString(R.string.verifica)));
        list.add(new MenuEntity(7, getResources().getDrawable(R.drawable.ic_nuestras_oficinas), getResources().getString(R.string.nuestras_oficinas)));

        list.add(new MenuEntity(9, getResources().getDrawable(R.drawable.ic_comparatel_menu), getResources().getString(R.string.string_comparatel)));
        list.add(new MenuEntity(10, getResources().getDrawable(R.drawable.ic_comparamovil_menu), getResources().getString(R.string.string_comparamovil)));
        list.add(new MenuEntity(11, getResources().getDrawable(R.drawable.ic_punku_menu), getResources().getString(R.string.string_punku)));

        if(mSessionManager.isEncuestaActiva() && mSessionManager.isLogin()){
            list.add(new MenuEntity(8, getResources().getDrawable(R.drawable.ic_calificanos), getResources().getString(R.string.calificanos)));
        }
        mAdapter.setItems(list);
    }

    @Override
    public void clickItem(MenuEntity menuEntity) {
        switch (menuEntity.getId()) {
            case 1:
                sendLog(AppConstants.ACTION_GETIN_CONSULT_IMEI);
                ((MainActivity) getActivity()).setConsultImei();
                break;
            case 2:
                sendLog(AppConstants.ACTION_GETIN_OSIPTEL_SIGNAL);
                ((MainActivity) getActivity()).setCosultSignal();
                break;
            case 3:
                //  ShareUtils.shareTwitter(getActivity(),
                //  "Prueba Twitter", "http://www2.osiptel.gob.pe/Coberturamovil/", null, null);
                //ShareUtils.shareFacebook(getActivity(),"Prueba", "http://www2.osiptel.gob.pe/Coberturamovil/");
                break;
            case 4:
                sendLog(AppConstants.ACTION_GETIN_TRASU_PROCEEDINGS);
                ((MainActivity) getActivity()).setCosultRecords();
                break;
            case 5:
                //Guia comparativa
                activateMenu(false);
                sendLog(AppConstants.ACTION_GETIN_INFORMATION_GUIDE);
                GuiaFragment guiaFragment = GuiaFragment.newInstance();
                ActivityUtils.addFragmentToFragment(getActivity().getSupportFragmentManager(),
                        guiaFragment, R.id.body, "ActivateMenu");
                break;
            case 6:
                activateMenu(false);
                sendLog(AppConstants.ACTION_GETIN_VERIFY_LINE);
                VerificarLineaFragment verificarLineaFragment = VerificarLineaFragment.newInstance();
                ActivityUtils.addFragmentToFragment(getActivity().getSupportFragmentManager(),
                        verificarLineaFragment, R.id.body,"ActivateMenu");
                break;
            case 7:
                sendLog(AppConstants.ACTION_GETIN_OUR_OFFICES);
                ((MainActivity) getActivity()).setCosultOffices();
                break;
            case 8:
                activateMenu(false);
                sendLog(AppConstants.ACTION_GETIN_QUALIFY);
                EncuestaFragment encuestaFragment = EncuestaFragment.newInstance();
                ActivityUtils.addFragmentToFragment(getActivity().getSupportFragmentManager(),
                        encuestaFragment, R.id.body,"ActivateMenu");
                break;

            case 9:
                final Bitmap backButton = BitmapFactory.decodeResource(getResources(), R.drawable.ic_keyboard_arrow_left_24dp);
                sendLog(AppConstants.ACTION_GETIN_COMPARATEL);
                getCustomTabIntent(backButton,mSessionManager.getEnlaceComparatel());
                break;

            case 10:
                final Bitmap backButton1 = BitmapFactory.decodeResource(getResources(), R.drawable.ic_keyboard_arrow_left_24dp);
                sendLog(AppConstants.ACTION_GETIN_COMPARAMOVIL);
                getCustomTabIntent(backButton1,mSessionManager.getEnlaceComparamovil());
                break;

            case 11:
                final Bitmap backButton2 = BitmapFactory.decodeResource(getResources(), R.drawable.ic_keyboard_arrow_left_24dp);
                sendLog(AppConstants.ACTION_GETIN_PUNKU);
                getCustomTabIntent(backButton2,mSessionManager.getEnlacePunku());
                break;
        }
    }

    private String getUrl(String url) {
        if (!url.startsWith("http://") && !url.startsWith("https://"))
            url = "http://" + url;
        return url;
    }

    private void getCustomTabIntent(Bitmap button,String url){
        CustomTabsIntent customTabsIntent2 = new CustomTabsIntent.Builder()
                .setToolbarColor(getActivity().getResources().getColor(R.color.colorPrimary))
                .setCloseButtonIcon(button)
                .setStartAnimations(getContext(), android.R.anim.fade_in, android.R.anim.fade_out)
                .setExitAnimations(getContext(), android.R.anim.fade_in, android.R.anim.fade_out)
                .build();
        CustomTabActivityHelper.openCustomTab(
                getActivity(), customTabsIntent2, Uri.parse(getUrl(url)), new WebviewFallback());
    }

    public void activateMenu(Boolean isActive){
        if(isActive){
            getList();
            rvList.setVisibility(View.VISIBLE);
        }else{
            rvList.setVisibility(View.GONE);
        }
    }

    public void setGps(){
        mSessionManager.setGPSstatus(true);

    }

    public void sendLog(String action){
        bodyEntityLog.setAccion(action);
        bodyEntityLog.setMac(mSessionManager.getMac());
        bodyEntityLog.setMarcaCelular(Build.BRAND);
        bodyEntityLog.setModeloCelular(Build.MODEL);
        if(mSessionManager.getUserEntity()!= null){
            bodyEntityLog.setNombreUsuario(mSessionManager.getUserEntity().getIdUsuario()+"");
            bodyEntityLog.setNumeroCelular(mSessionManager.getUserEntity().getNroTelefono());
        }else{
            bodyEntityLog.setNombreUsuario(" ");
            bodyEntityLog.setNumeroCelular(" ");
        }

        bodyEntityLog.setSistemaOperativo("Android");

        mPresenterLog.sendLog(bodyEntityLog);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        setGps();
    }

    @Override
    public boolean isActive() {
        return isAdded();
    }

    @Override
    public void setPresenter(MenuContract.Presenter presenter) {
        this.mPresenter = presenter;
    }

    @Override
    public void setLoadingIndicator(boolean active) {
        if (active) {
            mProgressDialogCustom.show();
        } else {
            if (mProgressDialogCustom.isShowing()) {
                mProgressDialogCustom.dismiss();
            }
        }
    }

    @Override
    public void showMessage(String message) {

    }

    @Override
    public void showErrorMessage(String message) {

    }
}
