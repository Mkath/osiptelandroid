package pe.gob.osiptelandroid.presentation.guia;

import android.app.Activity;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.AppBarLayout;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import pe.gob.osiptelandroid.R;
import pe.gob.osiptelandroid.core.BaseFragment;
import pe.gob.osiptelandroid.data.entities.GuiaEntity;
import pe.gob.osiptelandroid.presentation.guia.formatos.FormatosReclamoFragment;
import pe.gob.osiptelandroid.utils.ActivityUtils;
import pe.gob.osiptelandroid.utils.ProgressDialogCustom;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by junior on 27/08/16.
 */
public class GuiaFragment extends BaseFragment implements GuiaItem, GuiaContract.View {

    private static final String TAG = GuiaFragment.class.getSimpleName();
    @BindView(R.id.rv_list)
    RecyclerView rvList;
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.appBarLayout)
    AppBarLayout appBarLayout;

    private GuiaAdapter mAdapter;
    private GridLayoutManager mlinearLayoutManager;
    private GuiaContract.Presenter mPresenter;
    private ProgressDialogCustom mProgressDialogCustom;

    ArrayList<GuiaEntity> listNombreImagen;

    private activateMenu mCallback;

    public GuiaFragment() {
        // Requires empty public constructor
    }

    public static GuiaFragment newInstance() {
        return new GuiaFragment();
    }

    public interface activateMenu {
        void sendActivateMenu(Boolean isActive);
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);

        // This makes sure that the container activity has implemented
        // the callback interface. If not, it throws an exception
        try {
            mCallback = (GuiaFragment.activateMenu) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement TextClicked");
        }
    }

    @Override
    public void onDetach() {
        mCallback = null; // => avoid leaking, thanks @Deepscorn
        super.onDetach();
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mPresenter = new GuiaPresenter(this, getContext());
    }

    @Override
    public void onResume() {
        super.onResume();
    }


    @Override
    public void onStart() {
        super.onStart();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.guia_informativa_fragment, container, false);
        ButterKnife.bind(this, root);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                remove();
            }
        });

        return root;
    }

    private void remove() {
        mCallback.sendActivateMenu(true);
        ActivityUtils.removeFragment(getActivity().getSupportFragmentManager(),
                this);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mProgressDialogCustom = new ProgressDialogCustom(getContext(), "Cargando...");
        mAdapter = new GuiaAdapter(new ArrayList<GuiaEntity>(), getContext(), (GuiaItem) this);
        mlinearLayoutManager = new GridLayoutManager(getContext(), 2);
        mlinearLayoutManager.setOrientation(GridLayoutManager.VERTICAL);
        rvList.setAdapter(mAdapter);
        rvList.setLayoutManager(mlinearLayoutManager);
        getList();
        mPresenter.getItems();
    }

    private void getList() {
        ArrayList<GuiaEntity> listIcons = new ArrayList<>();
        listIcons.add(new GuiaEntity(1, getResources().getDrawable(R.drawable.ic_averia), getResources().getString(R.string.averia)));
        listIcons.add(new GuiaEntity(2, getResources().getDrawable(R.drawable.ic_facturacionu), getResources().getString(R.string.facturacion)));
        listIcons.add(new GuiaEntity(3, getResources().getDrawable(R.drawable.ic_cobro_servicio), getResources().getString(R.string.cobro_de_servicio)));
        listIcons.add(new GuiaEntity(4, getResources().getDrawable(R.drawable.ic_calidad), getResources().getString(R.string.calidad_e_idoneidad)));
        listIcons.add(new GuiaEntity(5, getResources().getDrawable(R.drawable.ic_suspecion), getResources().getString(R.string.suspencion)));
        listIcons.add(new GuiaEntity(6, getResources().getDrawable(R.drawable.ic_falta_recibo), getResources().getString(R.string.falta_recibo)));
        listIcons.add(new GuiaEntity(7, getResources().getDrawable(R.drawable.ic_instalacion), getResources().getString(R.string.instalación)));
        listIcons.add(new GuiaEntity(8, getResources().getDrawable(R.drawable.ic_contratacion), getResources().getString(R.string.contratacion)));
        listIcons.add(new GuiaEntity(9, getResources().getDrawable(R.drawable.ic_incumplimiento), getResources().getString(R.string.incumplimiento_oferta)));
        listIcons.add(new GuiaEntity(10, getResources().getDrawable(R.drawable.ic_formato_reclamo), getResources().getString(R.string.formato_reclamo)));
        mAdapter.setItems(listIcons);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
    }

    @Override
    public void clickItem(GuiaEntity guiaEntity) {
        switch (guiaEntity.getIdTipoReclamo()) {
            case 10:
                FormatosReclamoFragment formatosReclamoFragment = FormatosReclamoFragment.newInstance();
                ActivityUtils.addFragmentToActivity(getActivity().getSupportFragmentManager(),
                        formatosReclamoFragment, R.id.body);
                break;
            default:
                AddFragment(setListNombreImagen(guiaEntity));
                break;
        }
    }

    private void AddFragment(GuiaEntity guiaEntity) {
        Bundle bundle = new Bundle();
        bundle.putString("descripcion", guiaEntity.getDescripcion());
        bundle.putString("nombreImagen", guiaEntity.getNombreImagen());
        DetalleGuiaFragment detalleGuiaFragment = DetalleGuiaFragment.newInstance(bundle);
        ActivityUtils.addFragmentToFragment(getActivity().getSupportFragmentManager(),
                detalleGuiaFragment, R.id.body, "Return");
    }

    private GuiaEntity setListNombreImagen(GuiaEntity guiaEntity) {

        if (listNombreImagen != null) {
            for (int i = 0; i < listNombreImagen.size(); i++) {
                if (listNombreImagen.get(i).getIdTipoReclamo() == guiaEntity.getIdTipoReclamo()) {
                    guiaEntity.setNombreImagen(listNombreImagen.get(i).getNombreImagen());
                }
            }
        }
        return guiaEntity;
    }

    @Override
    public void getListItems(ArrayList<GuiaEntity> list) {
        if (list != null) {
            listNombreImagen = new ArrayList<>();
            listNombreImagen = list;
        }
    }

    @Override
    public boolean isActive() {
        return isAdded();
    }

    @Override
    public void setPresenter(GuiaContract.Presenter presenter) {
        this.mPresenter = presenter;
    }

    @Override
    public void setLoadingIndicator(boolean active) {
        if (active) {
            mProgressDialogCustom.show();
        } else {
            if (mProgressDialogCustom.isShowing()) {
                mProgressDialogCustom.dismiss();
            }
        }
    }

    @Override
    public void showMessage(String message) {

    }

    @Override
    public void showErrorMessage(String message) {
        mPresenter.getItems();
    }

}
