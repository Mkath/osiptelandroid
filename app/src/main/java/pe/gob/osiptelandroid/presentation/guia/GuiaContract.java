package pe.gob.osiptelandroid.presentation.guia;
import pe.gob.osiptelandroid.core.BasePresenter;
import pe.gob.osiptelandroid.core.BaseView;
import pe.gob.osiptelandroid.data.entities.GuiaEntity;

import java.util.ArrayList;

/**
 * Created by katherine on 12/05/17.
 */

public interface GuiaContract {
    interface View extends BaseView<Presenter> {

        void getListItems(ArrayList<GuiaEntity> list);

        boolean isActive();
    }

    interface Presenter extends BasePresenter {

        void getItems();
        void getRefreshToken();


      /*  void loadOrdersFromPage(int page, int id);

        void loadfromNextPage(int id);

        void startLoad(int id);

        void getPromos(int page, int id);*/

    }
}
