package pe.gob.osiptelandroid.presentation.verficarlinea;

import android.content.Context;
import android.os.Build;
import android.support.annotation.RequiresApi;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import pe.gob.osiptelandroid.R;
import pe.gob.osiptelandroid.core.LoaderAdapter;
import pe.gob.osiptelandroid.data.entities.OperadoraEntity;
import pe.gob.osiptelandroid.utils.OnClickListListener;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;


/**
 * Created by katherine on 15/05/17.
 */

public class VerificarLineaAdapter extends LoaderAdapter<OperadoraEntity> implements OnClickListListener {



    private Context context;
    private VerificarLineaItem verificarLineaItem;

    public VerificarLineaAdapter(ArrayList<OperadoraEntity> verificarLineaEntities, Context context, VerificarLineaItem verificarLineaItem) {
        super(context);
        setItems(verificarLineaEntities);
        this.context = context;
        this.verificarLineaItem = verificarLineaItem;

    }

    public ArrayList<OperadoraEntity> getItems() {
        return (ArrayList<OperadoraEntity>) getmItems();
    }

    @Override
    public long getYourItemId(int position) {
        return getmItems().get(position).getIdOperador();
    }

    @Override
    public RecyclerView.ViewHolder getYourItemViewHolder(ViewGroup parent) {
        View root = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_verificar_linea, parent, false);
        return new ViewHolder(root, this);
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    @Override
    public void bindYourViewHolder(RecyclerView.ViewHolder holder, int position) {
        OperadoraEntity verificarLineaEntity = getItems().get(position);
        Picasso.get().load(verificarLineaEntity.getNombreImagen()).into(((ViewHolder) holder).imLogo);
    }


    @Override
    public void onClick(int position) {
        //Open Fragment
        OperadoraEntity verificarLineaEntity = getItems().get(position);
        verificarLineaItem.clickItem(verificarLineaEntity);
    }


    static class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        @BindView(R.id.im_logo)
        ImageView imLogo;
        @BindView(R.id.im_ir)
        ImageView imIr;

        private OnClickListListener onClickListListener;

        ViewHolder(View itemView, OnClickListListener onClickListListener) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            this.onClickListListener = onClickListListener;
            this.itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            onClickListListener.onClick(getAdapterPosition());
        }
    }
}
