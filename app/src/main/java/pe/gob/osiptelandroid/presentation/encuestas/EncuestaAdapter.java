package pe.gob.osiptelandroid.presentation.encuestas;

import android.content.Context;
import android.os.Build;
import android.support.annotation.RequiresApi;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import pe.gob.osiptelandroid.R;
import pe.gob.osiptelandroid.core.LoaderAdapter;
import pe.gob.osiptelandroid.data.entities.RespuestaEntity;
import pe.gob.osiptelandroid.data.entities.PreguntaEntity;
import pe.gob.osiptelandroid.data.local.SessionManager;
import pe.gob.osiptelandroid.utils.OnClickListListener;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;


/**
 * Created by katherine on 15/05/17.
 */

public class EncuestaAdapter extends LoaderAdapter<PreguntaEntity> implements OnClickListListener {

    private Context context;
    private EncuestaItem encuestaItem;
    private ArrayList<RespuestaEntity> list;
    private SessionManager mSessionManager;
    private String cadena;
    private int idPregunta;

    public EncuestaAdapter(ArrayList<PreguntaEntity> preguntaEntities, Context context, EncuestaItem encuestaItem) {
        super(context);
        setItems(preguntaEntities);
        this.context = context;
        this.encuestaItem = encuestaItem;
        list = new ArrayList<>();
        mSessionManager = new SessionManager(context);
    }

    public ArrayList<PreguntaEntity> getItems() {
        return (ArrayList<PreguntaEntity>) getmItems();
    }

    @Override
    public long getYourItemId(int position) {
        return getmItems().get(position).getIdPregunta();
    }

    @Override
    public RecyclerView.ViewHolder getYourItemViewHolder(ViewGroup parent) {
        View root = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_encuesta, parent, false);
        return new ViewHolder(root, this);
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    @Override
    public void bindYourViewHolder(final RecyclerView.ViewHolder holder, int position) {
        final PreguntaEntity preguntaEntity = getItems().get(position);
        if (preguntaEntity.getIdTipoPregunta() == 1) {
            ((ViewHolder) holder).tvQuestion.setText(preguntaEntity.getDescripcion());
            ((ViewHolder) holder).buttonYes.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    ((ViewHolder) holder).buttonYes.setBackground(context.getResources().getDrawable(R.drawable.button_selected));
                    ((ViewHolder) holder).buttonYes.setTextColor(context.getResources().getColor(R.color.white));
                    ((ViewHolder) holder).buttonNo.setBackground(context.getResources().getDrawable(R.drawable.button_border));
                    ((ViewHolder) holder).buttonNo.setTextColor(context.getResources().getColor(R.color.colorPrimary));
                    if(mSessionManager.getArrayList(SessionManager.ARRAY_ENCUESTA)==null){
                        list.add(new RespuestaEntity(preguntaEntity.getIdPregunta(), "Si"));
                        mSessionManager.saveArrayList(list);

                    }else{
                        ArrayList<RespuestaEntity> newList = new ArrayList<>();
                        newList = mSessionManager.getArrayList(SessionManager.ARRAY_ENCUESTA);
                        for (int i = 0; i <newList.size() ; i++) {
                            if(newList.get(i).getIdPregunta() == preguntaEntity.getIdPregunta()){
                                newList.remove(i);
                            }
                        }
                        list = newList;
                        list.add(new RespuestaEntity(preguntaEntity.getIdPregunta(), "Si"));
                        mSessionManager.saveArrayList(list);
                    }
                }
            });
            ((ViewHolder) holder).buttonNo.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    ((ViewHolder) holder).buttonNo.setBackground(context.getResources().getDrawable(R.drawable.button_selected));
                    ((ViewHolder) holder).buttonNo.setTextColor(context.getResources().getColor(R.color.white));
                    ((ViewHolder) holder).buttonYes.setBackground(context.getResources().getDrawable(R.drawable.button_border));
                    ((ViewHolder) holder).buttonYes.setTextColor(context.getResources().getColor(R.color.colorPrimary));
                    if(mSessionManager.getArrayList(SessionManager.ARRAY_ENCUESTA)==null){
                        list.add(new RespuestaEntity(preguntaEntity.getIdPregunta(), "No"));
                        mSessionManager.saveArrayList(list);

                    }else{
                        ArrayList<RespuestaEntity> newList = new ArrayList<>();
                        newList = mSessionManager.getArrayList(SessionManager.ARRAY_ENCUESTA);
                        for (int i = 0; i <newList.size() ; i++) {
                            if(newList.get(i).getIdPregunta() == preguntaEntity.getIdPregunta()){
                                newList.remove(i);
                            }
                        }
                        list = newList;
                        list.add(new RespuestaEntity(preguntaEntity.getIdPregunta(), "No"));
                        mSessionManager.saveArrayList(list);
                    }
                }
            });

        } else {
            if (preguntaEntity.getIdTipoPregunta() == 2) {
                ((ViewHolder) holder).lyQuestion.setVisibility(View.GONE);
                ((ViewHolder) holder).buttons.setVisibility(View.GONE);
                ((ViewHolder) holder).lyComentarios.setVisibility(View.VISIBLE);
                ((ViewHolder) holder).etComentarios.setVisibility(View.VISIBLE);
                ((ViewHolder) holder).etComentarios.addTextChangedListener(new TextWatcher() {
                    @Override
                    public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                        mSessionManager.setIdComentario(preguntaEntity.getIdPregunta());
                    }

                    @Override
                    public void onTextChanged(CharSequence s, int start, int before, int count) {

                    }

                    @Override
                    public void afterTextChanged(Editable s) {
                        cadena = ((ViewHolder) holder).etComentarios.getText().toString();
                        mSessionManager.setComentario(cadena);
                    }
                });

            } else {
                ((ViewHolder) holder).lyQuestion.setVisibility(View.GONE);
                ((ViewHolder) holder).buttons.setVisibility(View.GONE);
                ((ViewHolder) holder).rlCalificanos.setVisibility(View.VISIBLE);
                ((ViewHolder) holder).rating.setOnRatingBarChangeListener(new RatingBar.OnRatingBarChangeListener() {
                    @Override
                    public void onRatingChanged(RatingBar ratingBar, float rating, boolean fromUser) {
                        //list.add(new RespuestaEntity(idPregunta, cadena));
                        //list.add(new RespuestaEntity(preguntaEntity.getIdPregunta(), String.valueOf(((ViewHolder) holder).rating.getRating())) );
                        mSessionManager.setIdCalificanos(preguntaEntity.getIdPregunta());
                        mSessionManager.setRating(String.valueOf(((ViewHolder) holder).rating.getRating()));
                        //mSessionManager.saveArrayList(list);
                    }
                });

            }
        }

    }

    @Override
    public void onClick(int position) {

        // MenuEntity menuEntity = getItems().get(position);
        //  menuItem.clickItem(menuEntity);
    }


    static class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        @BindView(R.id.tv_question)
        TextView tvQuestion;
        @BindView(R.id.ly_question)
        LinearLayout lyQuestion;
        @BindView(R.id.button_yes)
        Button buttonYes;
        @BindView(R.id.button_no)
        Button buttonNo;
        @BindView(R.id.buttons)
        LinearLayout buttons;
        @BindView(R.id.ly_comentarios)
        LinearLayout lyComentarios;
        @BindView(R.id.et_comentarios)
        EditText etComentarios;
        @BindView(R.id.rl_calificanos)
        RelativeLayout rlCalificanos;
        @BindView(R.id.rating)
        RatingBar rating;
        private OnClickListListener onClickListListener;


        ViewHolder(View itemView, OnClickListListener onClickListListener) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            this.onClickListListener = onClickListListener;
            this.itemView.setOnClickListener(this);
        }


        @Override
        public void onClick(View v) {
            onClickListListener.onClick(getAdapterPosition());
        }
    }
}
