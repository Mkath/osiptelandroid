package pe.gob.osiptelandroid.presentation.expedientes.dialogs;

public interface ExpedientesInterface {

    void closeSession();
    void filterAction(String desde, String hasta, String estado, String palabra);
    void getExpedientePDF(String idExpediente);
}
