package pe.gob.osiptelandroid.presentation.signal.consultar;

import android.content.Context;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import pe.gob.osiptelandroid.R;
import pe.gob.osiptelandroid.core.LoaderAdapter;
import pe.gob.osiptelandroid.data.entities.TecnologiaEntity;
import pe.gob.osiptelandroid.data.local.SessionManager;
import pe.gob.osiptelandroid.presentation.encuestas.EncuestaItem;
import pe.gob.osiptelandroid.utils.OnClickListListener;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;


/**
 * Created by katherine on 15/05/17.
 */

public class ItemConsultaAdapter extends LoaderAdapter<TecnologiaEntity> {

    private Context context;
    private EncuestaItem encuestaItem;
    private SessionManager mSessionManager;
    private StringTecnologiaAdapter mAdapter;
    private LinearLayoutManager mLayoutManager;
    private ArrayList<TecnologiaEntity> list;
    private int size;

    public ItemConsultaAdapter(ArrayList<TecnologiaEntity> tecnologiaEntities, Context context, int listSize) {
        super(context);
        setItems(tecnologiaEntities);
        this.context = context;
        mSessionManager = new SessionManager(context);
        size = listSize;
    }

    public ArrayList<TecnologiaEntity> getItems() {
        return (ArrayList<TecnologiaEntity>) getmItems();
    }

    @Override
    public long getYourItemId(int position) {
        return getmItems().get(position).getIdEmpresa();
    }

    @Override
    public RecyclerView.ViewHolder getYourItemViewHolder(ViewGroup parent) {
        View root = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_tecnologias, parent, false);
        return new ViewHolder(root);
    }

    @Override
    public void bindYourViewHolder(final RecyclerView.ViewHolder holder, final int position) {
        final TecnologiaEntity tecnologiaEntity = getItems().get(position);
        for (int i = 0; i < mSessionManager.getArrayListOperadores(SessionManager.ARRAY_OPERADORES).size(); i++) {
            if (String.valueOf(tecnologiaEntity.getIdEmpresa()).equals(mSessionManager.getArrayListOperadores(SessionManager.ARRAY_OPERADORES).get(i).getCodigoOperador())) {
                Picasso.get()
                        .load(mSessionManager.getArrayListOperadores(SessionManager.ARRAY_OPERADORES).get(i).getNombreImagen())
                        .resize(350, 100).into(((ViewHolder) holder).imLogo);
            }
        }
        mLayoutManager = new LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false);
        ((ViewHolder) holder).rvList.setLayoutManager(mLayoutManager);
        mAdapter = new StringTecnologiaAdapter(new ArrayList<String>(), context);
        ((ViewHolder) holder).rvList.setAdapter(mAdapter);
        ((ViewHolder) holder).rvList.setNestedScrollingEnabled(false);
        mAdapter.setItems(tecnologiaEntity.getTecnologiaArray());
        if (position == size - 1) {
            ((ViewHolder) holder).line.setVisibility(View.GONE);
        }

    }


    static class ViewHolder extends RecyclerView.ViewHolder {

        private OnClickListListener onClickListListener;

        @BindView(R.id.im_logo)
        ImageView imLogo;
        @BindView(R.id.rv_list)
        RecyclerView rvList;
        @BindView(R.id.line)
        View line;

        ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
