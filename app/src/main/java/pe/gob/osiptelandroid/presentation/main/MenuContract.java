package pe.gob.osiptelandroid.presentation.main;

import pe.gob.osiptelandroid.core.BasePresenter;
import pe.gob.osiptelandroid.core.BaseView;

public interface MenuContract {
    interface View extends BaseView<MenuContract.Presenter> {
        boolean isActive();
    }

    interface Presenter extends BasePresenter {
        void getEnlacesAppMovil(String aplicacion, String global);
        void getRefreshToken();

    }
}
