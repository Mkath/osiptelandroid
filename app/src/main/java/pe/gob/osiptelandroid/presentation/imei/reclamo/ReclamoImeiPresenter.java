package pe.gob.osiptelandroid.presentation.imei.reclamo;

import android.content.Context;

import pe.gob.osiptelandroid.data.entities.APIError;
import pe.gob.osiptelandroid.data.entities.AccessTokenEntity;
import pe.gob.osiptelandroid.data.entities.MarcaEntity;
import pe.gob.osiptelandroid.data.entities.ModeloEntity;
import pe.gob.osiptelandroid.data.entities.OperadoraEntity;
import pe.gob.osiptelandroid.data.entities.ProblemaEntity;
import pe.gob.osiptelandroid.data.entities.body.BodyReportarImei;
import pe.gob.osiptelandroid.data.local.SessionManager;
import pe.gob.osiptelandroid.data.remote.ServiceFactory;
import pe.gob.osiptelandroid.data.remote.request.ListRequest;
import pe.gob.osiptelandroid.data.remote.request.LoginRequest;
import pe.gob.osiptelandroid.data.remote.request.PostRequest;
import pe.gob.osiptelandroid.utils.ErrorUtils;

import java.util.ArrayList;

import okhttp3.MediaType;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by katherine on 28/06/17.
 */

public class ReclamoImeiPresenter implements ReclamoImeiContract.Presenter {

    private ReclamoImeiContract.View mView;
    private Context context;
    private SessionManager mSessionManager;

    public ReclamoImeiPresenter(ReclamoImeiContract.View mView, Context context) {
        this.context = context;
        this.mView = mView;
        this.mView.setPresenter(this);
        mSessionManager = new SessionManager(context);
    }


    @Override
    public void start() {

    }

    @Override
    public void sendReport(BodyReportarImei bodyReportarImei) {
        mView.setLoadingIndicator(true);
        PostRequest postRequest = ServiceFactory.createSigemService(PostRequest.class);
        Call<Void> orders = postRequest.sendReporteImei("Bearer "+mSessionManager.getUserToken(),bodyReportarImei);
        orders.enqueue(new Callback<Void>() {
            @Override
            public void onResponse(Call<Void> call, Response<Void> response) {

                if (!mView.isActive()) {
                    return;
                }
                  mView.setLoadingIndicator(false);
                if (response.isSuccessful()) {
                    if (response.code() == 200) {
                        mView.showMessage("Su registro fue enviado con éxito");
                    }else{
                        mView.showErrorMessage("Se presentó un error al registrar su reporte, intente nuevamente por favor");
                    }

                } else {
                    APIError apiError = ErrorUtils.sigemParseError(response);
                    switch (response.code()){
                        case 400:
                            mView.showErrorMessage(apiError.getMessages().get(0));
                            break;
                        case 401:
                            getRefreshToken();
                            break;
                        case 500:
                            mView.showErrorMessage("Se presentó un error al registrar su reporte, intente nuevamente por favor");
                            break;
                        default:
                            mView.showErrorMessage("Error desconocido");
                            break;
                    }
                }
            }

            @Override
            public void onFailure(Call<Void> call, Throwable t) {
                if (!mView.isActive()) {
                    return;
                }
                mView.setLoadingIndicator(false);
                mView.showErrorMessage("Error al conectar con el servidor");
            }
        });
    }

    @Override
    public void getMarca() {
     //   mView.setLoadingIndicator(true);
        ListRequest listRequest = ServiceFactory.createSigemService(ListRequest.class);
        Call<ArrayList<MarcaEntity>> orders = listRequest.getMarcas("Bearer "+mSessionManager.getUserToken());
        orders.enqueue(new Callback<ArrayList<MarcaEntity>>() {
            @Override
            public void onResponse(Call<ArrayList<MarcaEntity>> call, Response<ArrayList<MarcaEntity>> response) {

                if (!mView.isActive()) {
                    return;
                }
              //  mView.setLoadingIndicator(false);
                if (response.isSuccessful()) {
                    if (response.body().size() != 0) {
                        mView.getListMarca(response.body());
                    }else{
                        mView.showErrorMessage("Error al obtener la lista de marcas");
                    }

                } else {

                    APIError apiError = ErrorUtils.sigemParseError(response);
                    switch (response.code()){
                        case 400:
                            mView.showErrorMessage(apiError.getMessages().get(0));
                            break;
                        case 401:
                            getRefreshToken();
                            break;
                        case 500:
                            mView.showErrorMessage("Error al obtener la lista de marcas");
                            break;
                        default:
                            mView.showErrorMessage("Error desconocido");
                            break;
                    }
                }
            }

            @Override
            public void onFailure(Call<ArrayList<MarcaEntity>> call, Throwable t) {
                if (!mView.isActive()) {
                    return;
                }
                mView.setLoadingIndicator(false);
                mView.showErrorMessage("Error al conectar con el servidor");
            }
        });
    }

    @Override
    public void getModelo(final int idMarca) {
        ListRequest listRequest = ServiceFactory.createSigemService(ListRequest.class);
        Call<ArrayList<ModeloEntity>> orders = listRequest.getModelobyMarca("Bearer "+mSessionManager.getUserToken(),idMarca);
        orders.enqueue(new Callback<ArrayList<ModeloEntity>>() {
            @Override
            public void onResponse(Call<ArrayList<ModeloEntity>> call, Response<ArrayList<ModeloEntity>> response) {

                if (!mView.isActive()) {
                    return;
                }
                if (response.isSuccessful()) {
                    if (response.body().size() != 0) {
                        mView.getListModelo(response.body());
                    }else{
                        mView.showErrorMessage("Error al obtener la lista de modelos");
                    }

                } else {
                    APIError apiError = ErrorUtils.sigemParseError(response);
                    switch (response.code()){
                        case 400:
                            mView.showErrorMessage(apiError.getMessages().get(0));
                            break;
                        case 401:
                            getRefreshToken();
                            break;
                        case 500:
                            mView.showErrorMessage("Error al obtener la lista de modelos");
                            break;
                        default:
                            mView.showErrorMessage("Error desconocido");
                            break;
                    }
                }
            }

            @Override
            public void onFailure(Call<ArrayList<ModeloEntity>> call, Throwable t) {
                if (!mView.isActive()) {
                    return;
                }
                mView.showErrorMessage("Error al conectar con el servidor");
            }
        });
    }

    @Override
    public void getEmpresas() {
        mView.setLoadingIndicator(true);
        ListRequest listRequest = ServiceFactory.createService(ListRequest.class);
        Call<ArrayList<OperadoraEntity>> orders = listRequest.getOperadores("Bearer "+mSessionManager.getUserToken());
        orders.enqueue(new Callback<ArrayList<OperadoraEntity>>() {
            @Override
            public void onResponse(Call<ArrayList<OperadoraEntity>> call, Response<ArrayList<OperadoraEntity>> response) {

                if (!mView.isActive()) {
                    return;
                }
                mView.setLoadingIndicator(false);

                if (response.isSuccessful()) {
                    if (response.body().size() != 0) {
                        mView.getListEmpresa(response.body());
                        getMarca();
                    }else{
                        mView.showErrorMessage("Error al obtener la lista de empresas");
                    }

                } else {
                    APIError apiError = ErrorUtils.sigemParseError(response);
                    switch (response.code()){
                        case 400:
                            mView.showErrorMessage(apiError.getMessages().get(0));
                            break;
                        case 401:
                            getRefreshToken();
                            break;
                        case 500:
                            mView.showErrorMessage("Error al obtener la lista de empresas");
                            break;
                        default:
                            mView.showErrorMessage("Error desconocido");
                            break;
                    }
                }
            }

            @Override
            public void onFailure(Call<ArrayList<OperadoraEntity>> call, Throwable t) {
                if (!mView.isActive()) {
                    return;
                }
                mView.showErrorMessage("Error al conectar con el servidor");
            }
        });
    }

    @Override
    public void getProblemas() {
        mView.setLoadingIndicator(true);
        ListRequest listRequest = ServiceFactory.createSigemService(ListRequest.class);
        Call<ArrayList<ProblemaEntity>> orders = listRequest.getProblemas("Bearer "+mSessionManager.getUserToken());
        orders.enqueue(new Callback<ArrayList<ProblemaEntity>>() {
            @Override
            public void onResponse(Call<ArrayList<ProblemaEntity>> call, Response<ArrayList<ProblemaEntity>> response) {

                if (!mView.isActive()) {
                    return;
                }
                mView.setLoadingIndicator(false);
                if (response.isSuccessful()) {
                    if (response.body().size() != 0) {
                        mView.getListProblema(response.body());
                    }else{
                        mView.showErrorMessage("Error al obtener la lista de problemas encontrados");
                    }

                } else {
                    APIError apiError = ErrorUtils.sigemParseError(response);
                    switch (response.code()){
                        case 400:
                            mView.showErrorMessage(apiError.getMessages().get(0));
                            break;
                        case 401:
                            getRefreshToken();
                            break;
                        case 500:
                            mView.showErrorMessage("Error al obtener la lista de problemas encontrados");
                            break;
                        default:
                            mView.showErrorMessage("Error desconocido");
                            break;
                    }
                }
            }

            @Override
            public void onFailure(Call<ArrayList<ProblemaEntity>> call, Throwable t) {
                if (!mView.isActive()) {
                    return;
                }
                mView.setLoadingIndicator(false);
                mView.showErrorMessage("Error al conectar con el servidor");
            }
        });
    }

    @Override
    public void getRefreshToken() {
        String text = "client_id=OsitelApp&client_secret=d60d6b88-bf36-463f-ae90-b423188bf1bf&grant_type=client_credentials";
        RequestBody body =
                RequestBody.create(MediaType.parse("application/x-www-form-urlencoded"), text);
        LoginRequest loginRequest = ServiceFactory.createTokenService(LoginRequest.class);
        Call<AccessTokenEntity> orders = loginRequest.login(body);
        orders.enqueue(new Callback<AccessTokenEntity>() {
            @Override
            public void onResponse(Call<AccessTokenEntity> call, Response<AccessTokenEntity> response) {
                if (!mView.isActive()) {
                    return;
                }

                if (response.isSuccessful()) {
                    mSessionManager.setUserToken(response.body().getAccess_token());
                    getEmpresas();
                }else{
                    mView.showErrorMessage("Error al auntenticar su sesión, intente nuevamente por favor");
                }
            }

            @Override
            public void onFailure(Call<AccessTokenEntity> call, Throwable t) {
                if (!mView.isActive()) {
                    return;
                }
                mView.showErrorMessage("Error al conectar con el servidor");
            }
        });
    }
}