package pe.gob.osiptelandroid.presentation.splash.dialogs;


import android.app.AlertDialog;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;

import pe.gob.osiptelandroid.R;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class NotConnectionDialog extends AlertDialog {


    @BindView(R.id.cancel)
    Button cancel;
    @BindView(R.id.aceptar)
    Button aceptar;
    @BindView(R.id.im_close)
    ImageView imClose;
    private Context mContext;

    private SplashInterface mSplashInterface;

    public NotConnectionDialog(Context context, SplashInterface splashInterface) {
        super(context);
        this.mSplashInterface = splashInterface;
        LayoutInflater inflater = LayoutInflater.from(getContext());
        final View view = inflater.inflate(R.layout.dialog_no_connection, null);
        setView(view);
        ButterKnife.bind(this, view);
        mContext = context;
    }


    @OnClick({R.id.cancel, R.id.aceptar})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.cancel:
                mSplashInterface.finishActivity();
                break;
            case R.id.aceptar:
                mSplashInterface.resend();
                break;
        }
    }
}
