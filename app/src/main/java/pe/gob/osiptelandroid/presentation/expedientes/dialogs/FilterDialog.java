package pe.gob.osiptelandroid.presentation.expedientes.dialogs;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.content.Context;
import android.support.v4.app.FragmentManager;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.github.florent37.singledateandtimepicker.dialog.SingleDateAndTimePickerDialog;
import pe.gob.osiptelandroid.R;
import pe.gob.osiptelandroid.data.entities.EstadoEntity;
import pe.gob.osiptelandroid.utils.MyDatePickerFragment;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class FilterDialog extends AlertDialog {

    @BindView(R.id.tv_desde)
    TextView tvDesde;
    @BindView(R.id.tv_hasta)
    TextView tvHasta;
    @BindView(R.id.spinner_tipo_expediente)
    Spinner spinnerTipoExpediente;
    @BindView(R.id.spinner_estado_expediente)
    Spinner spinnerEstadoExpediente;
    @BindView(R.id.et_palabra_clave)
    EditText etPalabraClave;
    @BindView(R.id.button_ingresar)
    RelativeLayout buttonIngresar;
    ExpedientesInterface expedientesInterface;
    SingleDateAndTimePickerDialog.Builder singleBuilder;
    Context context;
    @BindView(R.id.ly_desde)
    LinearLayout lyDesde;
    @BindView(R.id.ly_hasta)
    LinearLayout lyHasta;
    private int type;
    //  private DatePickerDialog dpd;
    private String tipoExp, estadoExp, palabraClave, codEstado;
    private String desde, hasta;
    private String desdeServicio, hastaServicio;
    private Activity activity;
    private FragmentManager fragmentManager;

    public FilterDialog(Context context, ExpedientesInterface expedientesInterface, ArrayList<EstadoEntity> list, FragmentManager fragmentManager) {
        super(context);
        this.expedientesInterface = expedientesInterface;
        LayoutInflater inflater = LayoutInflater.from(getContext());
        final View view = inflater.inflate(R.layout.dialg_filtrar_expedientes, null);
        setView(view);
        ButterKnife.bind(this, view);
        setUiData(list);
        this.context = context;
        this.fragmentManager = fragmentManager;
        lyHasta.setEnabled(false);
    }

    private void setUiData(ArrayList<EstadoEntity> list) {
        ArrayList<String> listProblemas = new ArrayList<>();
        listProblemas.add("Estado");
        for (int i = 0; i < list.size(); i++) {
            listProblemas.add(list.get(i).getDescripcion());
        }

        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(getContext(),
                android.R.layout.simple_spinner_item, listProblemas);
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinnerEstadoExpediente.setAdapter(dataAdapter);

        spinnerEstadoExpediente.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                estadoExp = spinnerEstadoExpediente.getItemAtPosition(position).toString();
                if (estadoExp.equals("Estado")) {
                    codEstado = "0";
                } else {
                    for (int i = 0; i < list.size(); i++) {
                        if (list.get(i).getDescripcion().equals(estadoExp)) {
                            codEstado = list.get(i).getIdcodigo();
                        }
                    }
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }

    private void getDataFromUi() {
        desdeServicio = tvDesde.getText().toString();
        hastaServicio = tvHasta.getText().toString();
        palabraClave = etPalabraClave.getText().toString();
    }


    @OnClick({R.id.im_close, R.id.button_ingresar, R.id.ly_desde, R.id.ly_hasta})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.im_close:
                dismiss();
                break;
            case R.id.button_ingresar:
                getDataFromUi();
                validateData();
                break;
            case R.id.ly_desde:
                showDatePickerDialogDesde(tvDesde);
                // getDateTime();
                break;
            case R.id.ly_hasta:
                showDatePickerDialogHasta(tvHasta);
                // getDateTime();
                break;
        }
    }

    private void validateData() {
        if (desde != null) {
            if (hasta == null) {
                Toast.makeText(getContext(), "Por favor ingrese ambas fechas", Toast.LENGTH_SHORT).show();
            } else {
                expedientesInterface.filterAction(desdeServicio, hastaServicio, codEstado, palabraClave);
            }
        } else {
            expedientesInterface.filterAction(desdeServicio, hastaServicio, codEstado, palabraClave);
        }


    }

    private void showDatePickerDialogDesde(final TextView textView) {
        MyDatePickerFragment newFragment = MyDatePickerFragment.newInstance(new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker datePicker, int year, int month, int day) {
                // +1 because january is zero

                final String selectedDate = twoDigits(day) + "/" + twoDigits(month + 1) + "/" + year;


                if (hasta != null) {
                    SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");

                    Date date3 = null;
                    Date date4 = null;
                    try {
                        date3 = format.parse(year + "-" + twoDigits(month + 1) + "-" + twoDigits(day));
                        date4 = format.parse(hasta);

                    } catch (ParseException e) {
                        e.printStackTrace();
                    }

                    if (date3.compareTo(date4) > 0) {
                        textView.setText("");
                        Toast.makeText(context, "Por favor seleccione una fecha válida", Toast.LENGTH_SHORT).show();
                    } else {
                        desde = year + "-" + twoDigits(month + 1) + "-" + twoDigits(day);
                        ;
                        textView.setText(selectedDate);
                    }
                } else {
                    desde = year + "-" + twoDigits(month + 1) + "-" + twoDigits(day);
                    textView.setText(selectedDate);
                    lyHasta.setEnabled(true);
                }

            }
        });
        newFragment.show(fragmentManager, "datePicker");
    }

    private void showDatePickerDialogHasta(final TextView textView) {
        MyDatePickerFragment newFragment = MyDatePickerFragment.newInstance(new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker datePicker, int year, int month, int day) {
                // +1 because january is zero

                final String selectedDate = twoDigits(day) + "/" + twoDigits(month + 1) + "/" + year;

                if (desde != null) {
                    SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");

                    Date date1 = null;
                    Date date2 = null;
                    try {
                        date1 = format.parse(year + "-" + twoDigits(month + 1) + "-" + twoDigits(day));
                        date2 = format.parse(desde);

                    } catch (ParseException e) {
                        e.printStackTrace();
                    }

                    if (date1.compareTo(date2) < 0) {
                        textView.setText("");
                        Toast.makeText(context, "Por favor seleccione una fecha válida", Toast.LENGTH_SHORT).show();
                    } else {
                        hasta = year + "-" + twoDigits(month + 1) + "-" + twoDigits(day);
                        textView.setText(selectedDate);
                    }
                } else {
                    hasta = year + "-" + twoDigits(month + 1) + "-" + twoDigits(day);
                    textView.setText(selectedDate);
                }

            }
        });
        newFragment.show(fragmentManager, "datePicker");
    }

    private String twoDigits(int n) {
        return (n <= 9) ? ("0" + n) : String.valueOf(n);
    }
}
