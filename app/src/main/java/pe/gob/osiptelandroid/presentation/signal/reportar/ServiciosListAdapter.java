package pe.gob.osiptelandroid.presentation.signal.reportar;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.TextView;

import pe.gob.osiptelandroid.R;
import pe.gob.osiptelandroid.core.LoaderAdapter;
import pe.gob.osiptelandroid.data.entities.ServicioOperadorEntity;
import pe.gob.osiptelandroid.data.local.SessionManager;
import pe.gob.osiptelandroid.utils.OnClickListListener;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;


/**
 * Created by katherine on 15/05/17.
 */

public class ServiciosListAdapter extends LoaderAdapter<ServicioOperadorEntity> {

    private Context context;
    private SessionManager mSessionManager;
    private ArrayList<ServicioOperadorEntity> list;
    private ArrayList<ServicioOperadorEntity> listChecked;

    public ServiciosListAdapter(ArrayList<ServicioOperadorEntity> servicioOperadorEntities, Context context, ArrayList<ServicioOperadorEntity> listChecked) {
        super(context);
        setItems(servicioOperadorEntities);
        this.context = context;
        list = new ArrayList<>();
        mSessionManager = new SessionManager(context);
        this.listChecked = listChecked;
    }

    public ArrayList<ServicioOperadorEntity> getItems() {
        return (ArrayList<ServicioOperadorEntity>) getmItems();
    }

    @Override
    public long getYourItemId(int position) {
        return 0;
    }

    @Override
    public RecyclerView.ViewHolder getYourItemViewHolder(ViewGroup parent) {
        View root = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_lista_servicios, parent, false);
        return new ViewHolder(root);
    }

    @Override
    public void bindYourViewHolder(final RecyclerView.ViewHolder holder, final int position) {
        final ServicioOperadorEntity servicioOperadorEntity = getItems().get(position);
        if (listChecked != null) {
            for (int i = 0; i < listChecked.size(); i++) {
                if (listChecked.get(i).getIdServicio().equals(servicioOperadorEntity.getIdServicio())) {
                    ((ViewHolder) holder).cbxServicio.setChecked(true);
                }
            }
            list = listChecked;
        }

        ((ViewHolder) holder).tvNameServicio.setText(servicioOperadorEntity.getDescripcion());
        ((ViewHolder) holder).cbxServicio.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {

                if (isChecked) {
                    list.add(servicioOperadorEntity);
                    mSessionManager.saveArrayListaServiciosOperadores(list);
                } else {
                    ArrayList<ServicioOperadorEntity> newList = new ArrayList<>();
                    newList = mSessionManager.getArrayListaServiciosOperadores(SessionManager.ARRAY_LISTA_SERVICIOS);
                    for (int i = 0; i < newList.size(); i++) {
                        if (newList.get(i).getDescripcion().equals(servicioOperadorEntity.getDescripcion())) {
                            newList.remove(i);
                        }
                    }
                    list = newList;
                    mSessionManager.saveArrayListaServiciosOperadores(list);
                }
            }
        });
    }


    static class ViewHolder extends RecyclerView.ViewHolder {

        private OnClickListListener onClickListListener;

        @BindView(R.id.cbx_servicio)
        CheckBox cbxServicio;
        @BindView(R.id.tv_name_servicio)
        TextView tvNameServicio;

        ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
