package pe.gob.osiptelandroid.presentation.expedientes.registro_trasu;

import android.app.Activity;
import android.content.DialogInterface;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.constraint.ConstraintLayout;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.InputFilter;
import android.text.TextWatcher;
import android.util.Log;
import android.util.Patterns;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.mobsandgeeks.saripaar.ValidationError;
import com.mobsandgeeks.saripaar.Validator;
import com.mobsandgeeks.saripaar.annotation.NotEmpty;
import com.mobsandgeeks.saripaar.annotation.Select;
import pe.gob.osiptelandroid.R;
import pe.gob.osiptelandroid.core.BaseFragment;
import pe.gob.osiptelandroid.data.entities.ServiceEntity;
import pe.gob.osiptelandroid.data.entities.TipoDocumentoEntity;
import pe.gob.osiptelandroid.data.entities.UserEntity;
import pe.gob.osiptelandroid.data.entities.body.BodyEntityLog;
import pe.gob.osiptelandroid.data.entities.body.BodyRegistrarTrasu;
import pe.gob.osiptelandroid.data.local.SessionManager;
import pe.gob.osiptelandroid.presentation.register.dialogs.DialogConfirm;
import pe.gob.osiptelandroid.utils.ActivityUtils;
import pe.gob.osiptelandroid.utils.AppConstants;
import pe.gob.osiptelandroid.utils.LogContract;
import pe.gob.osiptelandroid.utils.LogPresenter;
import pe.gob.osiptelandroid.utils.ProgressDialogCustom;
import pe.gob.osiptelandroid.utils.ValidateUtils;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Pattern;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;

/**
 * Created by junior on 27/08/16.
 */
public class RegistroTrasufragment extends BaseFragment implements RegistroTrasuContract.View, Validator.ValidationListener {


    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @Select
    @BindView(R.id.spinner_tipo_documento)
    Spinner spinnerTipoDocumento;
    @NotEmpty
    @BindView(R.id.et_numero_documento)
    EditText etNumeroDocumento;
    @NotEmpty
    @BindView(R.id.et_nombres)
    EditText etNombres;
    @NotEmpty
    @BindView(R.id.et_ap_paterno)
    EditText etApPaterno;
    @NotEmpty
    @BindView(R.id.et_ap_materno)
    EditText etApMaterno;
    @NotEmpty
    @BindView(R.id.et_correo_electronico)
    EditText etCorreoElectronico;
    @NotEmpty
    @BindView(R.id.et_confirm_correo)
    EditText etConfirmCorreo;
    @NotEmpty
    @BindView(R.id.et_contrasenia)
    EditText etContrasenia;
    @NotEmpty
    @BindView(R.id.et_confirm_contrasenia)
    EditText etConfirmContrasenia;
    @BindView(R.id.button_registro)
    RelativeLayout buttonRegistro;
    Unbinder unbinder;
    @BindView(R.id.tv_error_documento)
    TextView tvErrorDocumento;
    Unbinder unbinder1;
    @BindView(R.id.tv_error_correo)
    TextView tvErrorCorreo;
    @BindView(R.id.tv_error_contrasenia)
    TextView tvErrorContrasenia;
    @BindView(R.id.layout_container)
    ConstraintLayout layoutContainer;
    @BindView(R.id.tv_title_nombre)
    TextView tvTitleNombre;
    @BindView(R.id.ly_ap_paterno)
    LinearLayout lyApPaterno;
    @BindView(R.id.ly_ap_materno)
    LinearLayout lyApMaterno;
    @BindView(R.id.ly_razon_social)
    LinearLayout lyRazonSocial;
    @BindView(R.id.et_razon_social)
    EditText etRazonSocial;
    @BindView(R.id.ly_nombres)
    LinearLayout lyNombres;
    @NotEmpty
    @BindView(R.id.tv_tipo_servicio)
    TextView tvTipoServicio;
    @Select
    @BindView(R.id.spinner_codigo_asociado)
    Spinner spinnerCodigoAsociado;
    @NotEmpty
    @BindView(R.id.et_codigo_asociado)
    EditText etCodigoAsociado;
    private RegistroTrasuContract.Presenter mPresenter;
    private int itemPositionTipoDocumento;
    private String type;
    private String typeService;
    private boolean isNumDocumentCorrect = false;
    UserEntity userEntity;
    private ServiceEntity mService;
    SessionManager mSessionManager;
    Validator validator;
    private String codService;
    private activateButtonsExpedientes mCallback;
    private ProgressDialogCustom mProgressDialogCustom;
    private ValidateUtils validateUtils;
    private ArrayList<ServiceEntity> listService;
    private int typeCodAsociado;
    private BodyEntityLog bodyEntityLog;
    private LogContract.Presenter mPresenterLog;

    public RegistroTrasufragment() {
        // Requires empty public constructor
    }

    public interface activateButtonsExpedientes {
        void sendActivateExpedientesButtons(Boolean isActive);
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);

        // This makes sure that the container activity has implemented
        // the callback interface. If not, it throws an exception
        try {
            mCallback = (activateButtonsExpedientes) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement TextClicked");
        }
    }

    @Override
    public void onDetach() {
        mCallback = null; // => avoid leaking, thanks @Deepscorn
        super.onDetach();
    }


    public static RegistroTrasufragment newInstance() {
        return new RegistroTrasufragment();
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mPresenter = new RegistroTrasuPresenter(this, getContext());
        mSessionManager = new SessionManager(getContext());
        validator = new Validator(this);
        mProgressDialogCustom = new ProgressDialogCustom(getContext(), "Enviando...");
        validateUtils = new ValidateUtils(getContext(), getActivity());
        mPresenterLog = new LogPresenter( getContext());
        bodyEntityLog = new BodyEntityLog();
    }

    public void hideKeyboardView(View view) {
        InputMethodManager inputMethodManager = (InputMethodManager) getActivity().getSystemService(Activity.INPUT_METHOD_SERVICE);
        inputMethodManager.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onStart() {
        super.onStart();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.registrar_trasu_fragment, container, false);
        unbinder = ButterKnife.bind(this, root);

        //VALIDAR DATOS GUARDADOS EN SESSION MANAGEER
        if (mSessionManager.getUserEntity() != null) {
            // userEntity = mSessionManager.getUserEntity();
            setDataUser();
        }
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                remove();
            }
        });

        layoutContainer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                hideKeyboardView(layoutContainer);
            }
        });
        return root;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mPresenter.getTipoDocumento();
        validator.setValidationListener(this);
        cleanUi();
        getListCodigoAsociado();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        mSessionManager.setServiceTrasuNull();
        unbinder.unbind();
    }

    private void remove() {
        mCallback.sendActivateExpedientesButtons(true);
        mSessionManager.setServiceTrasuNull();
        ActivityUtils.removeFragment(getActivity().getSupportFragmentManager(),
                this);
    }

    private void setDataUser() {
        etNombres.setText(mSessionManager.getUserEntity().getNombre());
        etApPaterno.setText(mSessionManager.getUserEntity().getApellidoPaterno());
        etApMaterno.setText(mSessionManager.getUserEntity().getApellidoMaterno());
        etCorreoElectronico.setText(mSessionManager.getUserEntity().getCorreoElectronico());
    }

    private void cleanUi() {
        validateUtils.cleanUI(etNombres, null, false, 0);
        validateUtils.cleanUI(etApPaterno, null, false, 0);
        validateUtils.cleanUI(etApMaterno, null, false, 0);
        validateUtils.cleanUI(etNumeroDocumento, tvErrorDocumento, false, 0);
        validateUtils.cleanUI(etCorreoElectronico, null, false, 0);
        validateUtils.cleanUI(etConfirmCorreo, tvErrorCorreo, false, 0);
        validateUtils.cleanUI(etContrasenia, null, false, 0);
        validateUtils.cleanUI(etConfirmContrasenia, tvErrorContrasenia, false, 0);
        validateUtils.cleanUI(etCodigoAsociado, null, false, 0);
    }

    @Override
    public void getListTipoServicios(final ArrayList<ServiceEntity> list) {
        listService = new ArrayList<>();
        listService = list;
        if (mService != null) {
            mSessionManager.setServiceTrasu(mService);
        }
        Bundle bundle = new Bundle();
        bundle.putSerializable("listService", list);
        ServiciosTrasuListFragment serviciosTrasuListFragment = ServiciosTrasuListFragment.newInstance(bundle);
        ActivityUtils.addFragment(getActivity().getSupportFragmentManager(),
                serviciosTrasuListFragment, R.id.body_consultar, "Servicio Trasu List Fragment");

    }


    @Override
    public void getListTipoDocumentos(ArrayList<TipoDocumentoEntity> list) {
        ArrayList<String> listDocumento = new ArrayList<>();
        listDocumento.add("Seleccionar");
        for (int i = 0; i < list.size(); i++) {
            listDocumento.add(list.get(i).getDescripcion());
        }
        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(getContext(),
                android.R.layout.simple_spinner_item, listDocumento);
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinnerTipoDocumento.setAdapter(dataAdapter);

        spinnerTipoDocumento.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                itemPositionTipoDocumento = position;
                type = spinnerTipoDocumento.getItemAtPosition(position).toString();
                if (type.equals("DNI")) {
                    spinnerTipoDocumento.setBackground(getResources().getDrawable(R.drawable.border_edittext));
                    InputFilter[] filterArray = new InputFilter[1];
                    filterArray[0] = new InputFilter.LengthFilter(8);
                    etNumeroDocumento.setEnabled(true);
                    etNumeroDocumento.setText("");
                    etNumeroDocumento.setFilters(filterArray);
                    setEnabledNames(true);
                    etNumeroDocumento.addTextChangedListener(new TextWatcher() {
                        @Override
                        public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                        }

                        @Override
                        public void onTextChanged(CharSequence s, int start, int before, int count) {

                        }

                        @Override
                        public void afterTextChanged(Editable s) {
                            if (etNumeroDocumento.getText().toString().length() == 8) {
                                if (isOpenKeyboard()) {
                                    hideKeyboard();
                                }
                            }
                        }
                    });

                } else {
                    if (type.equals("Seleccionar")) {
                        etNumeroDocumento.setText("");
                        etNumeroDocumento.setEnabled(false);
                        setEnabledNames(true);
                    } else {
                        if (type.equals("RUC")) {
                            spinnerTipoDocumento.setBackground(getResources().getDrawable(R.drawable.border_edittext));
                            InputFilter[] filterRuc = new InputFilter[1];
                            filterRuc[0] = new InputFilter.LengthFilter(11);
                            etNumeroDocumento.setEnabled(true);
                            etNumeroDocumento.setText("");
                            etNumeroDocumento.setFilters(filterRuc);
                            setEnabledNames(false);
                            etNumeroDocumento.addTextChangedListener(new TextWatcher() {
                                @Override
                                public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                                }

                                @Override
                                public void onTextChanged(CharSequence s, int start, int before, int count) {

                                }

                                @Override
                                public void afterTextChanged(Editable s) {
                                    if (etNumeroDocumento.getText().toString().length() == 11) {
                                        if (isOpenKeyboard()) {
                                            hideKeyboard();
                                        }
                                    }
                                }
                            });

                        }
                    }
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

    }

    public void getListCodigoAsociado() {
        ArrayList<String> listCodigoAsociado = new ArrayList<>();
        listCodigoAsociado.add("Seleccionar");
        listCodigoAsociado.add("Nro. Telefónico");
        listCodigoAsociado.add("Código de reclamo");
        listCodigoAsociado.add("Código de cliente");

        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(getContext(),
                android.R.layout.simple_spinner_item, listCodigoAsociado);
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinnerCodigoAsociado.setAdapter(dataAdapter);

        spinnerCodigoAsociado.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if(spinnerCodigoAsociado.getItemAtPosition(position).toString().equals("Nro. Telefónico")){
                    typeCodAsociado = 1;
                    spinnerCodigoAsociado.setBackground(getResources().getDrawable(R.drawable.border_edittext));

                }
                if(spinnerCodigoAsociado.getItemAtPosition(position).toString().equals("Código de reclamo")){
                    typeCodAsociado = 2;
                    spinnerCodigoAsociado.setBackground(getResources().getDrawable(R.drawable.border_edittext));

                }
                if(spinnerCodigoAsociado.getItemAtPosition(position).toString().equals("Código de cliente")){
                    typeCodAsociado = 3;
                    spinnerCodigoAsociado.setBackground(getResources().getDrawable(R.drawable.border_edittext));
                }

                Log.e("CODIGO ASOCIADO: " , String.valueOf(typeCodAsociado));

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

    }


    public void getTypeSereviceTrasu(ServiceEntity serviceEntity) {
        if (serviceEntity != null) {
            mService = serviceEntity;
            tvTipoServicio.setText(serviceEntity.getDescripcion());
            codService = serviceEntity.getIdCodigo();
        } else {
            mSessionManager.setServiceTrasu(mService);
            if (tvTipoServicio.getText().toString().isEmpty()) {
                mSessionManager.setServiceTrasuNull();
                mService = null;
                Toast.makeText(getContext(), "Deberá seleccionar un tipo de problema para continuar", Toast.LENGTH_SHORT).show();
            }
        }
    }

    public void setEnabledNames(boolean isEnabled) {

        if (isEnabled) {
            lyNombres.setVisibility(View.VISIBLE);
            lyApPaterno.setVisibility(View.VISIBLE);
            lyApMaterno.setVisibility(View.VISIBLE);
            etApPaterno.setVisibility(View.VISIBLE);
            etApMaterno.setVisibility(View.VISIBLE);
            etNombres.setVisibility(View.VISIBLE);
            lyRazonSocial.setVisibility(View.GONE);
            etRazonSocial.setVisibility(View.GONE);

        } else {
            lyNombres.setVisibility(View.GONE);
            lyApPaterno.setVisibility(View.GONE);
            lyApMaterno.setVisibility(View.GONE);
            etApPaterno.setVisibility(View.GONE);
            etApMaterno.setVisibility(View.GONE);
            etNombres.setVisibility(View.GONE);
            lyRazonSocial.setVisibility(View.VISIBLE);
            etRazonSocial.setVisibility(View.VISIBLE);
        }

    }

    @Override
    public boolean isActive() {
        return isAdded();
    }

    @Override
    public void setPresenter(RegistroTrasuContract.Presenter presenter) {
        this.mPresenter = presenter;
    }

    @Override
    public void setLoadingIndicator(boolean active) {
        if (active) {
            mProgressDialogCustom.show();
        } else {
            if (mProgressDialogCustom.isShowing()) {
                mProgressDialogCustom.dismiss();
            }
        }
    }

    @Override
    public void showMessage(String message) {
        DialogConfirm dialogConfirm = new DialogConfirm(getContext(), message, false, false);
        dialogConfirm.show();
        dialogConfirm.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialog) {
                remove();
            }
        });
        dialogConfirm.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
    }

    @Override
    public void showErrorMessage(String message) {
        DialogConfirm dialogConfirm = new DialogConfirm(getContext(), message, true, false);
        dialogConfirm.show();
        dialogConfirm.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
    }

    @OnClick({R.id.button_registro, R.id.tv_tipo_servicio})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.button_registro:
                sendLog(AppConstants.ACTION_REGISTER_TRASU_PROCEEDINGS);
                updateUI();
                if (!validarEmail(etCorreoElectronico.getText().toString())) {
                    etCorreoElectronico.setBackground(getResources().getDrawable(R.drawable.border_error_edittext));
                }

                if (!validarEmail(etConfirmCorreo.getText().toString())) {
                    etConfirmCorreo.setBackground(getResources().getDrawable(R.drawable.border_error_edittext));
                }

                validator.validate();
                break;
            case R.id.tv_tipo_servicio:
                if (listService == null) {
                    mPresenter.getServicios();
                } else {
                    if (mService != null) {
                        mSessionManager.setServiceTrasu(mService);
                    }
                    Bundle bundle = new Bundle();
                    bundle.putSerializable("listService", listService);
                    ServiciosTrasuListFragment serviciosTrasuListFragment = ServiciosTrasuListFragment.newInstance(bundle);
                    ActivityUtils.addFragment(getActivity().getSupportFragmentManager(),
                            serviciosTrasuListFragment, R.id.body_consultar, "Servicio Trasu List Fragment");
                }
                break;
        }
    }

    public void sendLog(String action){
        bodyEntityLog.setAccion(action);
        bodyEntityLog.setMac(mSessionManager.getMac());
        bodyEntityLog.setMarcaCelular(Build.BRAND);
        bodyEntityLog.setModeloCelular(Build.MODEL);
        if(mSessionManager.getUserEntity()!= null){
            bodyEntityLog.setNombreUsuario(mSessionManager.getUserEntity().getIdUsuario()+"");
            bodyEntityLog.setNumeroCelular(mSessionManager.getUserEntity().getNroTelefono());
        }else{
            bodyEntityLog.setNombreUsuario(" ");
            bodyEntityLog.setNumeroCelular(" ");
        }

        bodyEntityLog.setSistemaOperativo("Android");

        mPresenterLog.sendLog(bodyEntityLog);
    }

    private void validaDocumento() {
        if (type.equals("DNI")) {
            if (etNumeroDocumento.length() != 8) {
                tvErrorDocumento.setVisibility(View.VISIBLE);
                tvErrorDocumento.setText(getResources().getString(R.string.error_dni));
                etNumeroDocumento.setBackground(getResources().getDrawable(R.drawable.border_error_edittext));
                isNumDocumentCorrect = false;
            } else {
                isNumDocumentCorrect = true;
            }
        } else {
            if (type.equals("RUC")) {
                if (etNumeroDocumento.length() != 11) {
                    tvErrorDocumento.setVisibility(View.VISIBLE);
                    tvErrorDocumento.setText(getResources().getString(R.string.error_ruc));
                    etNumeroDocumento.setBackground(getResources().getDrawable(R.drawable.border_error_edittext));
                    isNumDocumentCorrect = false;
                } else {
                    isNumDocumentCorrect = true;
                }
            }
        }
    }

    private boolean validaCorreo() {

        if (!validarEmail(etCorreoElectronico.getText().toString())) {
            etCorreoElectronico.setBackground(getResources().getDrawable(R.drawable.border_error_edittext));
            return false;
        }

        if (!validarEmail(etConfirmCorreo.getText().toString())) {
            etConfirmCorreo.setBackground(getResources().getDrawable(R.drawable.border_error_edittext));
            return false;
        }

        return true;
    }

    private boolean correosIguales() {
        if (etCorreoElectronico.getText().toString().equals(etConfirmCorreo.getText().toString())) {
            return true;
        } else {
            etCorreoElectronico.setBackground(getResources().getDrawable(R.drawable.border_error_edittext));
            etConfirmCorreo.setBackground(getResources().getDrawable(R.drawable.border_error_edittext));
            tvErrorCorreo.setVisibility(View.VISIBLE);
            return false;
        }
    }

    private boolean validarEmail(String email) {
        Pattern pattern = Patterns.EMAIL_ADDRESS;
        return pattern.matcher(email).matches();
    }

    private boolean validaContrasenia() {
        if (etContrasenia.getText().toString().equals(etConfirmContrasenia.getText().toString())) {
            return true;
        } else {
            etContrasenia.setBackground(getResources().getDrawable(R.drawable.border_error_edittext));
            etConfirmContrasenia.setBackground(getResources().getDrawable(R.drawable.border_error_edittext));
            tvErrorContrasenia.setVisibility(View.VISIBLE);
        }
        return false;
    }

    private void updateUI() {
        etNumeroDocumento.setBackground(getResources().getDrawable(R.drawable.border_edittext));
        etNombres.setBackground(getResources().getDrawable(R.drawable.border_edittext));
        etApPaterno.setBackground(getResources().getDrawable(R.drawable.border_edittext));
        etCorreoElectronico.setBackground(getResources().getDrawable(R.drawable.border_edittext));
        spinnerTipoDocumento.setBackground(getResources().getDrawable(R.drawable.border_edittext));
        etCodigoAsociado.setBackground(getResources().getDrawable(R.drawable.border_edittext));
        etConfirmCorreo.setBackground(getResources().getDrawable(R.drawable.border_edittext));
        etContrasenia.setBackground(getResources().getDrawable(R.drawable.border_edittext));
        etConfirmContrasenia.setBackground(getResources().getDrawable(R.drawable.border_edittext));
        tvErrorDocumento.setVisibility(View.GONE);
        tvErrorContrasenia.setVisibility(View.GONE);
        tvErrorCorreo.setVisibility(View.GONE);
        tvTipoServicio.setBackground(getResources().getDrawable(R.drawable.border_edittext));
    }

    @Override
    public void onValidationSucceeded() {
        //  Toast.makeText(getContext(), "Yay! we got it right!", Toast.LENGTH_SHORT).show();
        validaDocumento();
        if (isNumDocumentCorrect) {
            if (validaCorreo() && validaContrasenia() && correosIguales()) {

                if (typeService != "Seleccionar") {
                    BodyRegistrarTrasu bodyRegistrarTrasu = new BodyRegistrarTrasu();

                    if (type.equals("DNI")) {
                        bodyRegistrarTrasu.setTipoDocumento("D");
                        bodyRegistrarTrasu.setNombres(etNombres.getText().toString());
                        bodyRegistrarTrasu.setApellidoPaterno(etNombres.getText().toString());
                        bodyRegistrarTrasu.setApellidoMaterno(etApMaterno.getText().toString());
                    } else {
                        if (type.equals("RUC")) {
                            bodyRegistrarTrasu.setTipoDocumento("R");
                            bodyRegistrarTrasu.setNombres(etRazonSocial.getText().toString());
                            bodyRegistrarTrasu.setApellidoPaterno(etRazonSocial.getText().toString());
                            bodyRegistrarTrasu.setApellidoMaterno(etRazonSocial.getText().toString());
                        }
                    }
                    bodyRegistrarTrasu.setNumeroDocumento(etNumeroDocumento.getText().toString());
                    bodyRegistrarTrasu.setCorreoElectronico(etCorreoElectronico.getText().toString());
                    bodyRegistrarTrasu.setConfirmarCorreoElectronico(etConfirmCorreo.getText().toString());
                    bodyRegistrarTrasu.setIdCodigoAsociado(typeCodAsociado);
                    bodyRegistrarTrasu.setCodigoReclamo(etCodigoAsociado.getText().toString());
                    bodyRegistrarTrasu.setContrasenia(etContrasenia.getText().toString());
                    bodyRegistrarTrasu.setConfirmarContrasenia(etConfirmContrasenia.getText().toString());
                    bodyRegistrarTrasu.setIdServicioReclamado(Integer.valueOf(codService));
                    bodyRegistrarTrasu.setSistemaOperativo("Android");
                    bodyRegistrarTrasu.setMarcaCelular(Build.BRAND);
                    bodyRegistrarTrasu.setModeloCelular(Build.MODEL);
                    bodyRegistrarTrasu.setMac(mSessionManager.getMac());
                    if (mSessionManager.getUserEntity() != null) {
                        bodyRegistrarTrasu.setNombreUsuario(mSessionManager.getUserEntity().getIdUsuario()+"");
                        bodyRegistrarTrasu.setNúmeroCelular(mSessionManager.getUserEntity().getNroTelefono());
                    } else {
                        bodyRegistrarTrasu.setNombreUsuario(" ");
                        bodyRegistrarTrasu.setNúmeroCelular(" ");
                    }
                    mPresenter.sendRegistrotrasu(bodyRegistrarTrasu);
                    //Toast.makeText(getContext(), "Yay! we got it right!", Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(getContext(), "Por favor ingrese sus datos correctamente", Toast.LENGTH_SHORT).show();

                }

            } else {
                Toast.makeText(getContext(), "Por favor ingrese sus datos correctamente", Toast.LENGTH_SHORT).show();
            }
        }


    }

    @Override
    public void onValidationFailed(List<ValidationError> errors) {
        String msg = "Por favor ingrese sus datos correctamente";
        Toast.makeText(getContext(), msg, Toast.LENGTH_LONG).show();

        for (ValidationError error : errors) {
            View view = error.getView();
            String message = error.getCollatedErrorMessage(getContext());

            // Display error messages ;)
            if (view instanceof EditText) {
                ((EditText) view).setBackground(getResources().getDrawable(R.drawable.border_error_edittext));

            } else {
                if (view instanceof Spinner) {
                    if (view.getId() == R.id.spinner_tipo_documento) {
                        if (spinnerTipoDocumento.getItemAtPosition(itemPositionTipoDocumento).toString().equals("Seleccionar")) {
                            spinnerTipoDocumento.setBackground(getResources().getDrawable(R.drawable.border_error_edittext));
                        }
                    }
                    if (view.getId() == R.id.spinner_codigo_asociado) {
                        if (spinnerTipoDocumento.getItemAtPosition(itemPositionTipoDocumento).toString().equals("Seleccionar")) {
                            spinnerTipoDocumento.setBackground(getResources().getDrawable(R.drawable.border_error_edittext));
                        }
                    }
                } else {
                    if (view instanceof TextView) {
                        if (view.getId() == R.id.tv_tipo_servicio) {
                            if (codService == null) {
                                tvTipoServicio.setBackground(getResources().getDrawable(R.drawable.border_error_edittext));
                            }
                        }
                    }       //Toast.makeText(getContext(), message, Toast.LENGTH_LONG).show();
                }
            }
        }
    }
}
