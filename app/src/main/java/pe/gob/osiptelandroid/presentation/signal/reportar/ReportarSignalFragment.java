package pe.gob.osiptelandroid.presentation.signal.reportar;

import android.app.Activity;
import android.content.DialogInterface;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.constraint.ConstraintLayout;
import android.support.v4.app.Fragment;
import android.support.v7.widget.Toolbar;
import android.util.Patterns;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.github.florent37.singledateandtimepicker.SingleDateAndTimePicker;
import com.github.florent37.singledateandtimepicker.dialog.SingleDateAndTimePickerDialog;
import com.mobsandgeeks.saripaar.ValidationError;
import com.mobsandgeeks.saripaar.Validator;
import com.mobsandgeeks.saripaar.annotation.Email;
import com.mobsandgeeks.saripaar.annotation.Length;
import com.mobsandgeeks.saripaar.annotation.NotEmpty;
import com.mobsandgeeks.saripaar.annotation.Select;
import pe.gob.osiptelandroid.R;
import pe.gob.osiptelandroid.core.BaseFragment;
import pe.gob.osiptelandroid.data.entities.DistritoEntity;
import pe.gob.osiptelandroid.data.entities.LocalidadEntity;
import pe.gob.osiptelandroid.data.entities.OperadoraEntity;
import pe.gob.osiptelandroid.data.entities.ProblemaCoberturaEntity;
import pe.gob.osiptelandroid.data.entities.ProvinciaEntity;
import pe.gob.osiptelandroid.data.entities.ReportarCoberturaResponse;
import pe.gob.osiptelandroid.data.entities.ServicioOperadorEntity;
import pe.gob.osiptelandroid.data.entities.UbicationLatLongEntity;
import pe.gob.osiptelandroid.data.entities.body.BodyEntityLog;
import pe.gob.osiptelandroid.data.entities.body.BodyReporteCobertura;
import pe.gob.osiptelandroid.data.local.SessionManager;
import pe.gob.osiptelandroid.presentation.register.dialogs.DialogConfirm;
import pe.gob.osiptelandroid.presentation.signal.dialog.TipoProblemaDialog;
import pe.gob.osiptelandroid.utils.ActivityUtils;
import pe.gob.osiptelandroid.utils.AppConstants;
import pe.gob.osiptelandroid.utils.LogContract;
import pe.gob.osiptelandroid.utils.LogPresenter;
import pe.gob.osiptelandroid.utils.ProgressDialogCustom;
import pe.gob.osiptelandroid.utils.ValidateUtils;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.regex.Pattern;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;

public class ReportarSignalFragment extends BaseFragment implements ReportarSignalContract.View, Validator.ValidationListener {

    private static final String TAG = ReportarSignalFragment.class.getSimpleName();
    @NotEmpty
    @BindView(R.id.et_nombres)
    EditText etNombres;
    @NotEmpty
    @BindView(R.id.et_ap_paterno)
    EditText etApPaterno;
    @NotEmpty
    @BindView(R.id.et_ap_materno)
    EditText etApMaterno;
    @NotEmpty
    @Length(min = 7)
    @BindView(R.id.et_numero_telefono)
    EditText etNumeroTelefono;
    @Select
    @BindView(R.id.spinner_empresa_operadora)
    Spinner spinnerEmpresaOperadora;
    @NotEmpty
    @BindView(R.id.tv_fecha_reporte)
    TextView tvFechaReporte;
    @NotEmpty
    @Email
    @BindView(R.id.et_correo_electronico)
    EditText etCorreoElectronico;
    @Select
    @BindView(R.id.spinner_tipo_problema)
    Spinner spinnerTipoProblema;
    @BindView(R.id.button_next)
    RelativeLayout buttonNext;
    Unbinder unbinder;
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.im_info)
    ImageView imInfo;
    @BindView(R.id.spinner_departamento)
    Spinner spinnerDepartamento;
    @BindView(R.id.spinner_provincia)
    Spinner spinnerProvincia;
    @BindView(R.id.spinner_distrito)
    Spinner spinnerDistrito;
    @BindView(R.id.spinner_localidad)
    Spinner spinnerLocalidad;
    @BindView(R.id.ly_tipo_servicio_operador)
    LinearLayout lyTipoServicioOperador;
    @BindView(R.id.et_tipo_servicio_operador)
    TextView etTipoServicioOperador;
    @BindView(R.id.layout_container)
    ConstraintLayout layoutContainer;
    @BindView(R.id.ly_hora)
    LinearLayout lyHora;
    @BindView(R.id.tv_hora_interrupcion)
    TextView tvHoraInterrupcion;
    @BindView(R.id.rl_hora)
    RelativeLayout rlHora;

    private ReportarSignalContract.Presenter mPresenter;
    private SessionManager mSessionManager;
    private ProgressDialogCustom mProgressDialogCustom;
    private int idProblema, idEmpresa;
    private String empresa;
    private String problema = " ";
    Validator validator;
    SingleDateAndTimePickerDialog.Builder singleBuilder;
    private String fechaReporte, horaInterrupcion;
    private String departamento, provincia, distrito, localidad;
    private String codDept = "Seleccionar";
    private String codProv = "Seleccionar";
    private String codDistr = "Seleccionar";
    private String codLocl = "Seleccionar";
    private String ubicacion;
    private String idServicios = "";
    private String latitudConsulta, longitudConsulta;
    private ValidateUtils validateUtils;
    private Boolean isConsultar = false;
    private boolean isChange = false;
    private BodyEntityLog bodyEntityLog;
    private LogContract.Presenter mPresenterLog;

    public ReportarSignalFragment() {
        // Requires empty public constructor
    }

    public void removeViewReport(Boolean isRemove, Fragment fragment) {
        if (isRemove) {
            ActivityUtils.removeFragment(getActivity().getSupportFragmentManager(),
                    fragment);
            remove();
        }
    }

    public static ReportarSignalFragment newInstance(Bundle bundle) {
        ReportarSignalFragment fragment = new ReportarSignalFragment();
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mPresenter = new ReportarSignalPresenter(this, getContext());
        mProgressDialogCustom = new ProgressDialogCustom(getContext(), "Espere...");
        mSessionManager = new SessionManager(getContext());
        validator = new Validator(this);
        validateUtils = new ValidateUtils(getContext(), getActivity());
        if (getArguments().getString("departamento") != null) {
            departamento = getArguments().getString("departamento");
            provincia = getArguments().getString("provincia");
            distrito = getArguments().getString("distrito");
            localidad = getArguments().getString("localidad");
            longitudConsulta = getArguments().getString("longitud");
            latitudConsulta = getArguments().getString("latitud");
            isConsultar = true;
        }
        mPresenterLog = new LogPresenter( getContext());
        bodyEntityLog = new BodyEntityLog();
    }

    @Override
    public void onResume() {
        super.onResume();
    }


    @Override
    public void onStart() {
        super.onStart();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.reportar_cobertura_fragment, container, false);
        unbinder = ButterKnife.bind(this, root);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                remove();
            }
        });
        layoutContainer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                hideKeyboardView(layoutContainer);
            }
        });
        return root;
    }


    private void remove() {
       /* if(singleBuilder!=null){
            singleBuilder.dismiss();
        }**/
        ActivityUtils.removeFragment(getActivity().getSupportFragmentManager(),
                this);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        validator.setValidationListener(this);
        mPresenter.getProblemasCobertura();
        spinnerProvincia.setEnabled(false);
        spinnerDistrito.setEnabled(false);
        spinnerLocalidad.setEnabled(false);
        if (mSessionManager.getArrayListOperadores(SessionManager.ARRAY_OPERADORES) == null) {
            mPresenter.getOperadores();
        } else {
            sendListToSpinner(mSessionManager.getArrayListOperadores(SessionManager.ARRAY_OPERADORES));
        }
        if (mSessionManager.getUserEntity() != null) {
            updateUi();
        }
        getListDepartamentos();
        getActualDate();
        cleanUi();
    }

    private void getActualDate() {
        final Calendar calendar = Calendar.getInstance();
        Date actualDate = calendar.getTime();
        Locale spanish = new Locale("es", "ES");
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("EEEE dd MMMM, yyyy", spanish);
        final SimpleDateFormat simpleDateFormatTextBD = new SimpleDateFormat("dd/MM/yyyy", spanish);
        tvFechaReporte.setText(simpleDateFormat.format(actualDate));
        fechaReporte = simpleDateFormatTextBD.format(actualDate);
    }

    private void getActualHour() {
        final Calendar calendar = Calendar.getInstance();
        Date actualDate = calendar.getTime();
        Locale spanish = new Locale("es", "ES");
        SimpleDateFormat simpleDateFormatText = new SimpleDateFormat("EEE dd MMM hh:mm", spanish);
        final SimpleDateFormat simpleDateFormatTextBD = new SimpleDateFormat("dd/MM/yyyy hh:mm", spanish);

        tvFechaReporte.setText(simpleDateFormatText.format(actualDate));
        fechaReporte = simpleDateFormatTextBD.format(actualDate);
    }

    public void hideKeyboardView(View view) {
        InputMethodManager inputMethodManager = (InputMethodManager) getActivity().getSystemService(Activity.INPUT_METHOD_SERVICE);
        inputMethodManager.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }

    private void cleanUi() {
        validateUtils.cleanUI(etNombres, null, false, 0);
        validateUtils.cleanUI(etApPaterno, null, false, 0);
        validateUtils.cleanUI(etApMaterno, null, false, 0);
        validateUtils.cleanUI(etNumeroTelefono, null, true, 9);
        validateUtils.cleanUI(etCorreoElectronico, null, false, 0);
    }

    private void updateUi() {
        etNombres.setText(mSessionManager.getUserEntity().getNombre());
        etApPaterno.setText(mSessionManager.getUserEntity().getApellidoPaterno());
        etApMaterno.setText(mSessionManager.getUserEntity().getApellidoMaterno());
        etNumeroTelefono.setText(mSessionManager.getUserEntity().getNroTelefono());
        etCorreoElectronico.setText(mSessionManager.getUserEntity().getCorreoElectronico());
    }

    private void sendListToSpinner(final ArrayList<OperadoraEntity> list) {
        ArrayList<String> listOperadores = new ArrayList<>();
        listOperadores.add("Seleccionar");
        for (int i = 0; i < list.size(); i++) {
            listOperadores.add(list.get(i).getNombreComercial());
        }

        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(getContext(),
                android.R.layout.simple_spinner_item, listOperadores);
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinnerEmpresaOperadora.setAdapter(dataAdapter);

        spinnerEmpresaOperadora.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                empresa = spinnerEmpresaOperadora.getItemAtPosition(position).toString();
                mSessionManager.saveArrayListaServiciosOperadores(null);
                if (!empresa.equals("Seleccionar")) {
                    spinnerEmpresaOperadora.setBackground(getResources().getDrawable(R.drawable.border_edittext));
                    for (int i = 0; i < list.size(); i++) {
                        if (list.get(i).getNombreComercial().equals(spinnerEmpresaOperadora.getItemAtPosition(position).toString())) {
                            idEmpresa = Integer.valueOf(list.get(i).getCodigoOperador());
                        }
                    }
                }

                if (idProblema == 2 && idEmpresa != 1000) {
                    mPresenter.getListServiciosSinonimo(idEmpresa);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
        if(singleBuilder!=null){
            singleBuilder.dismiss();
        }
    }

    @Override
    public void getListOperadores(ArrayList<OperadoraEntity> list) {
        mSessionManager.saveArrayListOperadores(list);
        sendListToSpinner(list);
    }

    @Override
    public void getListProblemasCobertura(final ArrayList<ProblemaCoberturaEntity> list) {
        ArrayList<String> listProblemas = new ArrayList<>();
        listProblemas.add("Seleccionar");
        for (int i = 0; i < list.size(); i++) {
            listProblemas.add(list.get(i).getTipoProblema());
        }

        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(getContext(),
                android.R.layout.simple_spinner_item, listProblemas);
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinnerTipoProblema.setAdapter(dataAdapter);

        spinnerTipoProblema.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                problema = spinnerTipoProblema.getItemAtPosition(position).toString();
                spinnerEmpresaOperadora.setSelection(0);
                idEmpresa = 1000;
                if (!spinnerTipoProblema.getItemAtPosition(position).toString().equals("Seleccionar")) {
                    spinnerTipoProblema.setBackground(getResources().getDrawable(R.drawable.border_edittext));
                    for (int i = 0; i < list.size(); i++) {
                        if (list.get(i).getTipoProblema().equals(spinnerTipoProblema.getItemAtPosition(position).toString())) {
                            idProblema = list.get(i).getIdTipoProblema();
                        }
                    }
                } else {
                    idProblema = 1000;
                }

                if (idProblema == 1) {
                    lyTipoServicioOperador.setVisibility(View.GONE);
                    etTipoServicioOperador.setVisibility(View.GONE);
                    mSessionManager.saveArrayListaServiciosOperadores(null);
                    idServicios = "";
                    getActualDate();
                }else if(idProblema==2){
                    getActualHour();
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }

    @Override
    public void reporteResponse(ReportarCoberturaResponse reportarCoberturaResponse) {

    }

    private void getListDepartamentos() {
        ArrayList<String> listDepartamentos = new ArrayList<>();
        listDepartamentos.add("Seleccionar");

        int size = 0;
        if(mSessionManager.getArrayListDepartamentos(SessionManager.ARRAY_DEPARTAMENTOS) != null){
            size = mSessionManager.getArrayListDepartamentos(SessionManager.ARRAY_DEPARTAMENTOS).size();
        }

        for (int i = 0; i < size; i++) {
            listDepartamentos.add(mSessionManager.getArrayListDepartamentos(SessionManager.ARRAY_DEPARTAMENTOS).get(i).getDepartamento());
        }
        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(getContext(),
                android.R.layout.simple_spinner_item, listDepartamentos);
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinnerDepartamento.setAdapter(dataAdapter);


        spinnerDepartamento.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                //   itemPositionEmpresa = position;
                codDept = spinnerDepartamento.getItemAtPosition(position).toString();
                if (!codDept.equals("Seleccionar")) {
                    mPresenter.getProvincia(codDept);
                    spinnerProvincia.setEnabled(true);
                    spinnerDepartamento.setBackground(getResources().getDrawable(R.drawable.border_edittext));
                    spinnerDistrito.setEnabled(false);
                    spinnerLocalidad.setEnabled(false);

                    if (isOpenKeyboard()) {
                        hideKeyboard();
                    }
                }
                isChange = true;
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });

        if (departamento != null) {
            for (int i = 0; i < mSessionManager.getArrayListDepartamentos(SessionManager.ARRAY_DEPARTAMENTOS).size(); i++) {
                if (departamento.equals(mSessionManager.getArrayListDepartamentos(SessionManager.ARRAY_DEPARTAMENTOS).get(i).getDepartamento())) {
                    spinnerDepartamento.setSelection(i + 1);
                }
            }
        }
    }

    @Override
    public void getListProvincia(ArrayList<ProvinciaEntity> list) {
        ArrayList<String> listProvincia = new ArrayList<>();
        listProvincia.add("Seleccionar");
        for (int i = 0; i < list.size(); i++) {
            listProvincia.add(list.get(i).getProvincia());
        }
        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(getContext(),
                android.R.layout.simple_spinner_item, listProvincia);
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinnerProvincia.setAdapter(dataAdapter);

        spinnerProvincia.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                //   itemPositionEmpresa = position;
                codProv = spinnerProvincia.getItemAtPosition(position).toString();
                if (!codProv.equals("Seleccionar")) {
                    mPresenter.getDistrito(codDept, codProv);
                    spinnerDistrito.setEnabled(true);
                    spinnerProvincia.setBackground(getResources().getDrawable(R.drawable.border_edittext));
                    spinnerLocalidad.setEnabled(false);
                }

                isChange = true;
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        if (provincia != null) {
            for (int i = 0; i < list.size(); i++) {
                if (provincia.equals(list.get(i).getProvincia())) {
                    spinnerProvincia.setSelection(i + 1);
                }
            }
        }
    }

    @Override
    public void getListDistrito(ArrayList<DistritoEntity> list) {
        ArrayList<String> listDistrito = new ArrayList<>();
        listDistrito.add("Seleccionar");
        for (int i = 0; i < list.size(); i++) {
            listDistrito.add(list.get(i).getDistrito());
        }
        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(getContext(),
                android.R.layout.simple_spinner_item, listDistrito);
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinnerDistrito.setAdapter(dataAdapter);

        spinnerDistrito.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                //   itemPositionEmpresa = position;
                codDistr = spinnerDistrito.getItemAtPosition(position).toString();
                if (!codDistr.equals("Seleccionar")) {
                    mPresenter.getLocalidades(codDept, codProv, codDistr);
                    spinnerLocalidad.setEnabled(true);
                    spinnerDistrito.setBackground(getResources().getDrawable(R.drawable.border_edittext));
                }
                isChange = true;

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        if (distrito != null) {
            for (int i = 0; i < list.size(); i++) {
                if (distrito.equals(list.get(i).getDistrito())) {
                    spinnerDistrito.setSelection(i + 1);
                }
            }
        }
    }

    @Override
    public void getListLocalidades(ArrayList<LocalidadEntity> list) {
        ArrayList<String> listLocalidad = new ArrayList<>();
        listLocalidad.add("Seleccionar");
        for (int i = 0; i < list.size(); i++) {
            listLocalidad.add(list.get(i).getLocalidad());
        }
        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(getContext(),
                android.R.layout.simple_spinner_item, listLocalidad);
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinnerLocalidad.setAdapter(dataAdapter);

        spinnerLocalidad.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                codLocl = spinnerLocalidad.getItemAtPosition(position).toString();
                if (!codLocl.equals("Seleccionar")) {
                    spinnerLocalidad.setBackground(getResources().getDrawable(R.drawable.border_edittext));
                }

                if(latitudConsulta==null){
                    mPresenter.getLatLong(codDept, codProv, codDistr, codLocl);
                }else{
                    if(isChange){
                        mPresenter.getLatLong(codDept, codProv, codDistr, codLocl);
                    }
                }

                isChange = true;

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        if (localidad != null) {
            for (int i = 0; i < list.size(); i++) {
                if (localidad.equals(list.get(i).getLocalidad())) {
                    spinnerLocalidad.setSelection(i + 1);
                }
            }
        }
    }

    @Override
    public void getListServiciosOperador(ArrayList<ServicioOperadorEntity> list) {
        if(list.size() == 0){
            showErrorMessage("Esta operadora aún no se encuentra vigente, seleccione otra por favor.");
            spinnerTipoProblema.setSelection(0);
            spinnerEmpresaOperadora.setSelection(0);
            idServicios="";
            lyTipoServicioOperador.setVisibility(View.GONE);
            etTipoServicioOperador.setVisibility(View.GONE);
        }else{
            Bundle bundle = new Bundle();
            bundle.putSerializable("servicioOperadorList", list);
            ServiciosListFragment serviciosListFragment = ServiciosListFragment.newInstance(bundle);
            ActivityUtils.addFragment(getActivity().getSupportFragmentManager(),
                    serviciosListFragment, R.id.body_signal, "Servicios List Fragment");
        }

    }

    @Override
    public void getLangLong(ArrayList<UbicationLatLongEntity> list) {
        if(list!=null){
            longitudConsulta = list.get(0).getLongitud();
            latitudConsulta = list.get(0).getLatitud();
        }
    }

    @Override
    public boolean isActive() {
        return isAdded();
    }

    @Override
    public void setPresenter(ReportarSignalContract.Presenter presenter) {
        this.mPresenter = presenter;
    }

    @Override
    public void setLoadingIndicator(boolean active) {
        if (active) {
            mProgressDialogCustom.show();
        } else {
            if (mProgressDialogCustom.isShowing()) {
                mProgressDialogCustom.dismiss();
            }
        }
    }

    @Override
    public void showMessage(String message) {
        DialogConfirm dialogConfirm = new DialogConfirm(getContext(), message, false, false);
        dialogConfirm.show();
        dialogConfirm.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialog) {
                remove();
            }
        });
        dialogConfirm.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
    }

    @Override
    public void showErrorMessage(String message) {
        DialogConfirm dialogConfirm = new DialogConfirm(getContext(), message, true, false);
        dialogConfirm.show();
        dialogConfirm.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
    }

    @OnClick({R.id.rl_fecha, R.id.button_next, R.id.im_info, R.id.ly_tipo_servicio_operador, R.id.et_tipo_servicio_operador, R.id.rl_hora})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.rl_fecha:
                if(idProblema ==1){
                    getDateTime();

                }else{
                    if(idProblema ==2){
                        getDateHour();
                    }
                }
                break;
            case R.id.rl_hora:
                break;
            case R.id.button_next:
                if (!validarEmail(etCorreoElectronico.getText().toString())) {
                    etCorreoElectronico.setBackground(getResources().getDrawable(R.drawable.border_error_edittext));
                }
                sendLog(AppConstants.ACTION_SEND_REPORT_OSIPTEL_SIGNAL);
                validator.validate();
                getErrorSpinner();
                break;
            case R.id.im_info:
                TipoProblemaDialog tipoProblemaDialog = new TipoProblemaDialog(getContext());
                tipoProblemaDialog.show();
                tipoProblemaDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                break;
            case R.id.ly_tipo_servicio_operador:
                //mPresenter.getListServicios(idEmpresa);
                mPresenter.getListServiciosSinonimo(idEmpresa);
                break;
            case R.id.et_tipo_servicio_operador:
                //mPresenter.getListServicios(idEmpresa);
                mPresenter.getListServiciosSinonimo(idEmpresa);
                break;
        }
    }

    public void sendLog(String action){
        bodyEntityLog.setAccion(action);
        bodyEntityLog.setMac(mSessionManager.getMac());
        bodyEntityLog.setMarcaCelular(Build.BRAND);
        bodyEntityLog.setModeloCelular(Build.MODEL);
        if(mSessionManager.getUserEntity()!= null){
            bodyEntityLog.setNombreUsuario(mSessionManager.getUserEntity().getIdUsuario()+"");
            bodyEntityLog.setNumeroCelular(mSessionManager.getUserEntity().getNroTelefono());
        }else{
            bodyEntityLog.setNombreUsuario(" ");
            bodyEntityLog.setNumeroCelular(" ");
        }

        bodyEntityLog.setSistemaOperativo("Android");

        mPresenterLog.sendLog(bodyEntityLog);
    }

    private void getDateTime() {
        final Calendar calendar = Calendar.getInstance();
        final SimpleDateFormat simpleDateFormat;
        final SimpleDateFormat simpleDateFormatText;

        Locale spanish = new Locale("es", "ES");
        simpleDateFormat = new SimpleDateFormat("EEEE dd MMMM, yyyy", spanish);
        final SimpleDateFormat simpleDateFormatTextBD = new SimpleDateFormat("dd/MM/yyyy", spanish);

        singleBuilder = new SingleDateAndTimePickerDialog.Builder(getContext())
                .bottomSheet()
                .mainColor(getResources().getColor(R.color.colorPrimary))
                .displayHours(false)
                .displayMinutes(false)
                .displayDays(true)
                .setDayFormatter(simpleDateFormat)
                .maxDateRange(calendar.getTime())
                .displayListener(new SingleDateAndTimePickerDialog.DisplayListener() {
                    @Override
                    public void onDisplayed(SingleDateAndTimePicker picker) {

                    }
                })
                .listener(new SingleDateAndTimePickerDialog.Listener() {
                    @Override
                    public void onDateSelected(Date date) {

                        if(date.compareTo(calendar.getTime())>0){
                            getActualDate();
                        }else{
                            tvFechaReporte.setText(simpleDateFormat.format(date));
                            fechaReporte = simpleDateFormatTextBD.format(date);
                        }

                        tvFechaReporte.setBackground(getResources().getDrawable(R.drawable.border_edittext));
                    }
                });
        singleBuilder.display();
    }
    private void getDateHour() {
        final Calendar calendar = Calendar.getInstance();
        final SimpleDateFormat simpleDateFormat;
        final SimpleDateFormat simpleDateFormatText;

        Locale spanish = new Locale("es", "ES");
        simpleDateFormat = new SimpleDateFormat("EEE d MMM", spanish);
        simpleDateFormatText = new SimpleDateFormat("EEE dd MMM hh:mm", spanish);
        final SimpleDateFormat simpleDateFormatTextBD = new SimpleDateFormat("dd/MM/yyyy hh:mm", spanish);

        singleBuilder = new SingleDateAndTimePickerDialog.Builder(getContext())
                .bottomSheet()
                .curved()
                .mainColor(getResources().getColor(R.color.colorPrimary))
                .displayHours(true)
                .displayMinutes(true)
                .displayDays(true)
                .maxDateRange(calendar.getTime())
                .setDayFormatter(simpleDateFormat)
                .displayListener(new SingleDateAndTimePickerDialog.DisplayListener() {
                    @Override
                    public void onDisplayed(SingleDateAndTimePicker picker) {

                    }
                })
                .listener(new SingleDateAndTimePickerDialog.Listener() {
                    @Override
                    public void onDateSelected(Date date) {
                        if(date.compareTo(calendar.getTime())>0){
                            getActualHour();
                        }else{
                            tvFechaReporte.setText(simpleDateFormatText.format(date));
                            fechaReporte = simpleDateFormatTextBD.format(date);
                        }

                        tvFechaReporte.setBackground(getResources().getDrawable(R.drawable.border_edittext));
                    }
                });
        singleBuilder.display();
    }


    @Override
    public void onValidationSucceeded() {
        if (!validarEmail(etCorreoElectronico.getText().toString())) {
            etCorreoElectronico.setBackground(getResources().getDrawable(R.drawable.border_error_edittext));
        } else {
            if (codDept.equals("Seleccionar") ||
                    codProv.equals("Seleccionar") ||
                    codDistr.equals("Seleccionar") ||
                    codLocl.equals("Seleccionar")) {
                getErrorSpinner();
                Toast.makeText(getContext(), "Por favor seleccione correctamente la ubicación a reportar", Toast.LENGTH_SHORT).show();
            } else {
                Bundle bundle = new Bundle();
                BodyReporteCobertura bodyReporteCobertura = new BodyReporteCobertura();
                bodyReporteCobertura.setNombre(etNombres.getText().toString());
                bodyReporteCobertura.setApellidoPaterno(etApPaterno.getText().toString());
                bodyReporteCobertura.setApellidoMaterno(etApMaterno.getText().toString());
                bodyReporteCobertura.setNumeroTelefono(etNumeroTelefono.getText().toString());
                bodyReporteCobertura.setIdEmpresa(idEmpresa);
                bodyReporteCobertura.setFechaReporte(fechaReporte);
                bodyReporteCobertura.setCorreoElectronico(etCorreoElectronico.getText().toString());
                bodyReporteCobertura.setIdProblema(idProblema);
                bodyReporteCobertura.setDepartamento(codDept);
                bodyReporteCobertura.setProvincia(codProv);
                bodyReporteCobertura.setDistrito(codDistr);
                bodyReporteCobertura.setLocalidad(codLocl);
                bodyReporteCobertura.setIdServicio(idServicios);
                bodyReporteCobertura.setLongitudReporte(longitudConsulta);
                bodyReporteCobertura.setLatitudReporte(latitudConsulta);
                bundle.putSerializable("bodyReporteCobertura", bodyReporteCobertura);
                bundle.putBoolean("isConsultar", isConsultar);
                MapaReportarSignalFragment mapaReportarSignalFragment = MapaReportarSignalFragment.newInstance(bundle);
                ActivityUtils.addFragmentToFragment(getActivity().getSupportFragmentManager(),
                        mapaReportarSignalFragment, R.id.body_signal, "Mapa Signal");
            }

        }

        // ActivityUtils.removeFragment(getActivity().getSupportFragmentManager(), this);
    }

    private boolean validarEmail(String email) {
        Pattern pattern = Patterns.EMAIL_ADDRESS;
        return pattern.matcher(email).matches();
    }

    private void getErrorSpinner() {
        if (codDept.equals("Seleccionar")) {
            spinnerDepartamento.setBackground(getResources().getDrawable(R.drawable.border_error_edittext));
        }

        if (codProv.equals("Seleccionar")) {
            spinnerProvincia.setBackground(getResources().getDrawable(R.drawable.border_error_edittext));
        }

        if (codDistr.equals("Seleccionar")) {
            spinnerDistrito.setBackground(getResources().getDrawable(R.drawable.border_error_edittext));
        }

        if (codLocl.equals("Seleccionar")) {
            spinnerLocalidad.setBackground(getResources().getDrawable(R.drawable.border_error_edittext));
        }
    }

    public void getDataListServicios(ArrayList<ServicioOperadorEntity> list) {
        if (list != null) {
            idServicios = "";
            lyTipoServicioOperador.setVisibility(View.VISIBLE);
            etTipoServicioOperador.setVisibility(View.VISIBLE);
            String text = "";
            for (int i = 0; i < list.size(); i++) {
                if (i == 0) {
                    text = list.get(0).getDescripcion();
                } else {
                    text = text + ", " + list.get(i).getDescripcion();
                }
                idServicios = list.get(i).getIdServicio() + "," + idServicios;
            }
            etTipoServicioOperador.setText(text);
        } else {
            lyTipoServicioOperador.setVisibility(View.GONE);
            etTipoServicioOperador.setVisibility(View.GONE);
            spinnerTipoProblema.setSelection(0);
            spinnerEmpresaOperadora.setSelection(0);
        }
    }


    @Override
    public void onValidationFailed(List<ValidationError> errors) {
        String msg = "Por favor ingrese sus datos correctamente";
        Toast.makeText(getContext(), msg, Toast.LENGTH_LONG).show();

        for (ValidationError error : errors) {
            View view = error.getView();
            String message = error.getCollatedErrorMessage(getContext());

            // Display error messages ;)
            if (view instanceof EditText) {
                ((EditText) view).setBackground(getResources().getDrawable(R.drawable.border_error_edittext));

            } else {
                if (view instanceof Spinner) {
                    if (view.getId() == R.id.spinner_empresa_operadora) {
                        if (empresa.equals("Seleccionar")) {
                            spinnerEmpresaOperadora.setBackground(getResources().getDrawable(R.drawable.border_error_edittext));
                        }
                    }
                    if (view.getId() == R.id.spinner_tipo_problema) {
                        if (problema.equals("Seleccionar")) {
                            spinnerTipoProblema.setBackground(getResources().getDrawable(R.drawable.border_error_edittext));
                        }
                    }
                } else {
                    if (view instanceof TextView) {
                        ((TextView) view).setBackground(getResources().getDrawable(R.drawable.border_error_edittext));
                    } else {
                        Toast.makeText(getContext(), message, Toast.LENGTH_LONG).show();

                    }
                }
            }
        }
    }


}
