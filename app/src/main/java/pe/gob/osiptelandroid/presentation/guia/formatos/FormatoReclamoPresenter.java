package pe.gob.osiptelandroid.presentation.guia.formatos;

import android.content.Context;

import pe.gob.osiptelandroid.data.entities.APIError;
import pe.gob.osiptelandroid.data.entities.AccessTokenEntity;
import pe.gob.osiptelandroid.data.entities.FormatoReclamoEntity;
import pe.gob.osiptelandroid.data.local.SessionManager;
import pe.gob.osiptelandroid.data.remote.ServiceFactory;
import pe.gob.osiptelandroid.data.remote.request.ListRequest;
import pe.gob.osiptelandroid.data.remote.request.LoginRequest;
import pe.gob.osiptelandroid.utils.ErrorUtils;

import java.util.ArrayList;

import okhttp3.MediaType;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by katherine on 28/06/17.
 */

public class FormatoReclamoPresenter implements FormatoReclamoContract.Presenter {

    private FormatoReclamoContract.View mView;
    private Context context;
    private SessionManager mSessionManager;

    public FormatoReclamoPresenter(FormatoReclamoContract.View mView, Context context) {
        this.context = context;
        this.mView = mView;
        this.mView.setPresenter(this);
        this.mSessionManager = new SessionManager(context);
    }


    @Override
    public void start() {

    }

    @Override
    public void getFormatos() {
        mView.setLoadingIndicator(true);
        ListRequest listRequest = ServiceFactory.createService(ListRequest.class);
        Call<ArrayList<FormatoReclamoEntity>> orders = listRequest.getFormatoReclamo("Bearer "+mSessionManager.getUserToken());
        orders.enqueue(new Callback<ArrayList<FormatoReclamoEntity>>() {
            @Override
            public void onResponse(Call<ArrayList<FormatoReclamoEntity>> call, Response<ArrayList<FormatoReclamoEntity>> response) {

                if (!mView.isActive()) {
                    return;
                }
                mView.setLoadingIndicator(false);

                if (response.isSuccessful()) {
                    if(response.body().size() != 0){
                        mView.getListFormatos(response.body());
                    }

                } else {
                    APIError apiError = ErrorUtils.firstParserError(response);
                    switch (response.code()){
                        case 400:
                            mView.showErrorMessage(apiError.getMessages().get(0));
                            break;
                        case 401:
                            getRefreshToken();
                            break;
                        case 500:
                            mView.showErrorMessage("Error al obtener los formatos de reclamo");
                            break;
                        default:
                            mView.showErrorMessage("Error desconocido");
                            break;
                    }
                }
            }

            @Override
            public void onFailure(Call<ArrayList<FormatoReclamoEntity>> call, Throwable t) {
                if (!mView.isActive()) {
                    return;
                }
                mView.setLoadingIndicator(false);
                mView.showErrorMessage("Error al conectar con el servidor");
            }
        });
    }

    @Override
    public void getRefreshToken() {
        String text = "client_id=OsitelApp&client_secret=d60d6b88-bf36-463f-ae90-b423188bf1bf&grant_type=client_credentials";
        RequestBody body =
                RequestBody.create(MediaType.parse("application/x-www-form-urlencoded"), text);
        LoginRequest loginRequest = ServiceFactory.createTokenService(LoginRequest.class);
        Call<AccessTokenEntity> orders = loginRequest.login(body);
        orders.enqueue(new Callback<AccessTokenEntity>() {
            @Override
            public void onResponse(Call<AccessTokenEntity> call, Response<AccessTokenEntity> response) {
                if (!mView.isActive()) {
                    return;
                }

                if (response.isSuccessful()) {
                    mSessionManager.setUserToken(response.body().getAccess_token());
                    getFormatos();
                }else{
                    mView.showErrorMessage("Error al auntenticar su sesión, intente nuevamente por favor");
                }
            }

            @Override
            public void onFailure(Call<AccessTokenEntity> call, Throwable t) {
                if (!mView.isActive()) {
                    return;
                }
                mView.showErrorMessage("Error al conectar con el servidor");
            }
        });
    }
}

