package pe.gob.osiptelandroid.presentation.signal.reportar;
import pe.gob.osiptelandroid.core.BasePresenter;
import pe.gob.osiptelandroid.core.BaseView;
import pe.gob.osiptelandroid.data.entities.DistritoEntity;
import pe.gob.osiptelandroid.data.entities.LocalidadEntity;
import pe.gob.osiptelandroid.data.entities.OperadoraEntity;
import pe.gob.osiptelandroid.data.entities.ProblemaCoberturaEntity;
import pe.gob.osiptelandroid.data.entities.ProvinciaEntity;
import pe.gob.osiptelandroid.data.entities.ReportarCoberturaResponse;
import pe.gob.osiptelandroid.data.entities.ServicioOperadorEntity;
import pe.gob.osiptelandroid.data.entities.UbicationLatLongEntity;
import pe.gob.osiptelandroid.data.entities.body.BodyReporteCobertura;

import java.util.ArrayList;

/**
 * Created by katherine on 12/05/17.
 */

public interface ReportarSignalContract {
    interface View extends BaseView<Presenter> {
        void getListOperadores(ArrayList<OperadoraEntity> list);
        void getListProblemasCobertura(ArrayList<ProblemaCoberturaEntity> list);
        void reporteResponse(ReportarCoberturaResponse reportarCoberturaResponse);

        void getListProvincia(ArrayList<ProvinciaEntity> list);
        void getListDistrito(ArrayList<DistritoEntity> list);
        void getListLocalidades(ArrayList<LocalidadEntity> list);

        void getListServiciosOperador(ArrayList<ServicioOperadorEntity> list);
        void getLangLong(ArrayList<UbicationLatLongEntity> list);
        boolean isActive();
    }

    interface Presenter extends BasePresenter {
        void getOperadores();
        void getProblemasCobertura();
        void sendReport(BodyReporteCobertura bodyReporteCobertura);

        void getProvincia(String codDepartamento);

        void getDistrito(String codDepartamento, String codProvincia);

        void getLocalidades(String codDepartamento, String codProvincia, String codDistrito);

        void getListServicios(int idOperador);

        void getListServiciosSinonimo(int idOperador);

        void getLatLong(String codDepartamento, String codProvincia, String codDistrito, String codLocl);

        void getRefreshToken();

    }
}
