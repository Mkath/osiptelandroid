package pe.gob.osiptelandroid.presentation.encuestas;

import android.app.Activity;
import android.content.DialogInterface;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Toast;

import pe.gob.osiptelandroid.R;
import pe.gob.osiptelandroid.core.BaseFragment;
import pe.gob.osiptelandroid.data.entities.body.BodyEncuestaEntity;
import pe.gob.osiptelandroid.data.entities.RespuestaEntity;
import pe.gob.osiptelandroid.data.entities.PreguntaEntity;
import pe.gob.osiptelandroid.data.local.SessionManager;
import pe.gob.osiptelandroid.presentation.register.dialogs.DialogConfirm;
import pe.gob.osiptelandroid.utils.ActivityUtils;
import pe.gob.osiptelandroid.utils.ProgressDialogCustom;

import java.util.ArrayList;
import java.util.Timer;
import java.util.TimerTask;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;

/**
 * Created by junior on 27/08/16.
 */
public class EncuestaFragment extends BaseFragment implements EncuestaContract.View, EncuestaItem {

    private static final String TAG = EncuestaFragment.class.getSimpleName();
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.rv_list)
    RecyclerView rvList;
    @BindView(R.id.button_send_encuesta)
    RelativeLayout buttonSendEncuesta;
    Unbinder unbinder;


    private EncuestaAdapter mAdapter;
    private LinearLayoutManager mlinearLayoutManager;
    private EncuestaContract.Presenter mPresenter;
    private ProgressDialogCustom mProgressDialogCustom;
    private SessionManager mSessionManager;
    private activateMenu mCallback;
    private int numPreguntas;
    public EncuestaFragment() {
        // Requires empty public constructor
    }

    public static EncuestaFragment newInstance() {
        return new EncuestaFragment();
    }

    public interface activateMenu {
        void sendActivateMenu(Boolean isActive);
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);

        // This makes sure that the container activity has implemented
        // the callback interface. If not, it throws an exception
        try {
            mCallback = (EncuestaFragment.activateMenu) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement TextClicked");
        }
    }

    @Override
    public void onDetach() {
        mCallback = null; // => avoid leaking, thanks @Deepscorn
        super.onDetach();
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mPresenter = new EncuestaPresenter(this, getContext());
        mSessionManager = new SessionManager(getContext());
    }

    @Override
    public void onResume() {
        super.onResume();

    }


    @Override
    public void onStart() {
        super.onStart();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.encuesta_fragment, container, false);
        unbinder = ButterKnife.bind(this, root);
        mProgressDialogCustom = new ProgressDialogCustom(getContext(), "Cargando...");
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                remove();
            }
        });
        buttonSendEncuesta.setVisibility(View.GONE);
        mPresenter.getPreguntas();
        return root;
    }

    private void remove() {
        mSessionManager.saveArrayList(null);
        mCallback.sendActivateMenu(true);
        ActivityUtils.removeFragment(getActivity().getSupportFragmentManager(),
                this);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mAdapter = new EncuestaAdapter(new ArrayList<PreguntaEntity>(), getContext(), this);
        mlinearLayoutManager = new LinearLayoutManager(getContext());
        mlinearLayoutManager.setOrientation(LinearLayout.VERTICAL);
        rvList.setAdapter(mAdapter);
        rvList.setLayoutManager(mlinearLayoutManager);
    }


    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @Override
    public void getListPreguntas(ArrayList<PreguntaEntity> list) {
        if (list.size() != 0) {
            numPreguntas = list.size() - 2;
            buttonSendEncuesta.setVisibility(View.VISIBLE);
            mAdapter.setItems(list);
        }
    }

    @Override
    public boolean isActive() {
        return isAdded();
    }

    @Override
    public void setPresenter(EncuestaContract.Presenter presenter) {
        this.mPresenter = presenter;
    }

    @Override
    public void setLoadingIndicator(boolean active) {
        if (active) {
            mProgressDialogCustom.show();
        } else {
            if (mProgressDialogCustom.isShowing()) {
                mProgressDialogCustom.dismiss();
            }
        }
    }

    @Override
    public void showMessage(String message) {
        final DialogConfirm dialogConfirm = new DialogConfirm(getContext(), message, false, true);
        dialogConfirm.show();
        dialogConfirm.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialog) {
                mSessionManager.saveArrayList(null);
                mSessionManager.setComentario(null);
                mSessionManager.setEncuestaActiva(false);
                remove();
            }
        });
        dialogConfirm.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        TimerTask task = new TimerTask() {
            @Override
            public void run() {
                dialogConfirm.dismiss();
               /* mSessionManager.setEncuestaActiva(true);
                Intent intent = new Intent(getContext(), MainActivity.class);
                startActivity(intent);
                getActivity().finish();*/
            }
        };

        // Simulate a long loading process on application startup.
        Timer timer = new Timer();
        timer.schedule(task, 3000);
    }

    @Override
    public void showErrorMessage(String message) {
        mSessionManager.saveArrayList(null);
        DialogConfirm dialogConfirm = new DialogConfirm(getContext(), message, true, false);
        dialogConfirm.show();
        dialogConfirm.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialog) {
                mSessionManager.setEncuestaActiva(true);
            }
        });
        dialogConfirm.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
    }

    @Override
    public void clickItem(PreguntaEntity preguntaEntity) {

    }

    @OnClick(R.id.button_send_encuesta)
    public void onViewClicked() {
        ArrayList<RespuestaEntity> list = new ArrayList<>();

        if (mSessionManager.getArrayList(SessionManager.ARRAY_ENCUESTA) == null
                || mSessionManager.getComentario() == null ||  mSessionManager.getComentario().equals("")
                || mSessionManager.getRating().equals("0")) {
           Toast.makeText(getContext(), "Por favor ingrese una respuesta válida", Toast.LENGTH_SHORT).show();
        } else {
            list = mSessionManager.getArrayList(SessionManager.ARRAY_ENCUESTA);

            Log.e("Num Preguntas", String.valueOf(numPreguntas));
            Log.e("LISTA SIZE", String.valueOf(list.size()));

            if (list.size() != numPreguntas) {
                Toast.makeText(getContext(), "Por favor responda todas las preguntas", Toast.LENGTH_SHORT).show();
            } else {
                list.add(new RespuestaEntity(mSessionManager.getIdComentario(), mSessionManager.getComentario()));
                list.add(new RespuestaEntity(mSessionManager.getIdCalificanos(), mSessionManager.getRating()));

                BodyEncuestaEntity encuestaEntity = new BodyEncuestaEntity();
                encuestaEntity.setIdUsuario(mSessionManager.getUserEntity().getIdUsuario());
                encuestaEntity.setUsuarioRegistro(mSessionManager.getUserEntity().getIdUsuario()+"");
                encuestaEntity.setUsuarioPregunta(list);
                encuestaEntity.setNombreUsuario(mSessionManager.getUserEntity().getIdUsuario()+"");
                encuestaEntity.setSistemaOperativo("Android");
                encuestaEntity.setMarcaCelular(Build.BRAND);
                encuestaEntity.setModeloCelular(Build.MODEL);
                encuestaEntity.setNumeroCelular(mSessionManager.getUserEntity().getNroTelefono());
                encuestaEntity.setMac(mSessionManager.getMac());
                mPresenter.setEncuesta(encuestaEntity);
                if(isOpenKeyboard()){
                    hideKeyboard();
                }
            }
        }


    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mSessionManager.saveArrayList(null);
    }
}
