package pe.gob.osiptelandroid.presentation.imei.consultar;

import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import pe.gob.osiptelandroid.R;
import pe.gob.osiptelandroid.core.BaseFragment;
import pe.gob.osiptelandroid.data.entities.ConsultaImeiEntity;
import pe.gob.osiptelandroid.data.local.SessionManager;
import pe.gob.osiptelandroid.presentation.imei.dialogs.DetalleImei;
import pe.gob.osiptelandroid.presentation.imei.dialogs.DetalleNoEncontradoImei;
import pe.gob.osiptelandroid.presentation.imei.dialogs.ObtenerImeiDialog;
import pe.gob.osiptelandroid.presentation.imei.reclamo.ReclamoImeiFragment;
import pe.gob.osiptelandroid.utils.ActivityUtils;
import pe.gob.osiptelandroid.utils.ProgressDialogCustom;
import pe.gob.osiptelandroid.utils.ValidateUtils;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;

/**
 * Created by junior on 27/08/16.
 */
public class ImeiFragment extends BaseFragment implements ImeiContract.View {

    private static final String TAG = ImeiFragment.class.getSimpleName();
    public static final String LISTA_IMEI = "lista_imei";

    @BindView(R.id.im_information)
    ImageView imInformation;
    Unbinder unbinder;
    @BindView(R.id.et_imei)
    EditText etImei;
    @BindView(R.id.tv_error_imei)
    TextView tvErrorImei;
    @BindView(R.id.button_ingresar)
    RelativeLayout buttonConsult;
    @BindView(R.id.button_registrar)
    RelativeLayout buttonReport;
    @BindView(R.id.toolbar)
    Toolbar toolbar;

    private String imei;
    private boolean isConsulta= false;
    private ImeiContract.Presenter mPresenter;
    private ProgressDialogCustom mProgressDialogCustom;
    private SessionManager mSessionManager;
    private ValidateUtils validateUtils;


    public ImeiFragment() {
        // Requires empty public constructor
    }

    public static ImeiFragment newInstance() {
        return new ImeiFragment();
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mPresenter = new ImeiPresenter(this, getContext());
        mSessionManager = new SessionManager(getContext());
        validateUtils = new ValidateUtils(getContext(), getActivity());
    }

    @Override
    public void onResume() {
        super.onResume();
        activateButtons(true);
    }


    @Override
    public void onStart() {
        super.onStart();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.imei_fragment, container, false);
        unbinder = ButterKnife.bind(this, root);
        mProgressDialogCustom = new ProgressDialogCustom(getContext(), "Cargando...");
        validateUtils.cleanUI(etImei, tvErrorImei, true, 15);
        return root;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }


    @OnClick({R.id.button_ingresar, R.id.button_registrar, R.id.im_information})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.button_ingresar:
                if (isOpenKeyboard()) {
                    hideKeyboard();
                }
                imei = etImei.getText().toString();
                validateImei();
                break;
            case R.id.button_registrar:
                if (isOpenKeyboard()) {
                    hideKeyboard();
                }
                isConsulta = false;
                Bundle bundle = new Bundle();
                bundle.putString("imei", etImei.getText().toString());
                ReclamoImeiFragment reportarImeifragment = ReclamoImeiFragment.newInstance(bundle);
                ActivityUtils.addFragmentToFragment(getActivity().getSupportFragmentManager(),
                        reportarImeifragment, R.id.new_body, "ActivateImei");
                break;
            case R.id.im_information:
                ObtenerImeiDialog obtenerImeiDialog = new ObtenerImeiDialog(getContext());
                obtenerImeiDialog.show();
                obtenerImeiDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                break;
        }
    }

    private void validateImei() {
        if (etImei.length() == 15) {
            if (mSessionManager.getUserEntity() != null) {
                mPresenter.sendImei(etImei.getText().toString(),
                        mSessionManager.getUserEntity().getIdUsuario()+"",
                        mSessionManager.getUserEntity().getNroTelefono());
            } else {
                mPresenter.sendImei(etImei.getText().toString(),
                        " ",
                        " ");
            }
            etImei.setBackground(getResources().getDrawable(R.drawable.border_edittext));
            tvErrorImei.setVisibility(View.GONE);
        } else {
            etImei.setBackground(getResources().getDrawable(R.drawable.border_error_edittext));
            if (!etImei.getText().toString().isEmpty()) {
                tvErrorImei.setVisibility(View.VISIBLE);
            }
        }
    }

    @Override
    public void imeiSuccess(ArrayList<ConsultaImeiEntity> consultaImeiEntity) {
        if (consultaImeiEntity.size() == 1) {
            DetalleImei detalleImei = new DetalleImei(getContext(), consultaImeiEntity.get(0).getReporte(),
                    consultaImeiEntity.get(0).getNe(), consultaImeiEntity.get(0).getOperador(), consultaImeiEntity.get(0).getFechaHora());
            detalleImei.show();
            detalleImei.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        } else {
            activateButtons(false);
            isConsulta = true;
            Bundle bundle = new Bundle();
            bundle.putSerializable(LISTA_IMEI, consultaImeiEntity);
            ImeiListFragment imeiListFragment = ImeiListFragment.newInstance(bundle);
            ActivityUtils.addFragmentToFragment(getActivity().getSupportFragmentManager(),
                    imeiListFragment, R.id.new_body, "ActivateImei");

        }

    }

    @Override
    public void imeiFailure(String message) {
        DetalleNoEncontradoImei detalleFallidoImei = new DetalleNoEncontradoImei(getContext(), etImei.getText().toString(), message);
        detalleFallidoImei.show();
        detalleFallidoImei.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
    }

    @Override
    public boolean isActive() {
        return isAdded();
    }

    @Override
    public void setPresenter(ImeiContract.Presenter presenter) {
        this.mPresenter = presenter;
    }

    @Override
    public void setLoadingIndicator(boolean active) {
        if (active) {
            mProgressDialogCustom.show();
        } else {
            if (mProgressDialogCustom.isShowing()) {
                mProgressDialogCustom.dismiss();
            }
        }
    }

    @Override
    public void showMessage(String message) {

    }

    @Override
    public void showErrorMessage(String message) {

    }

    public void activateButtons(boolean isActive) {
        buttonConsult.setEnabled(isActive);
        buttonReport.setEnabled(isActive);
        etImei.setEnabled(isActive);
        imInformation.setEnabled(isActive);
        if(isConsulta){
            etImei.setText(imei);
        }else{
            etImei.setText("");
        }
        etImei.setBackground(getResources().getDrawable(R.drawable.border_edittext));

    }

    public void deleteImei() {
        etImei.setText("");
    }

}
