package pe.gob.osiptelandroid.presentation.signal.verreportes;

import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.support.constraint.ConstraintLayout;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import pe.gob.osiptelandroid.R;
import pe.gob.osiptelandroid.core.LoaderAdapter;
import pe.gob.osiptelandroid.data.entities.VerReporteEntity;
import pe.gob.osiptelandroid.data.local.SessionManager;
import pe.gob.osiptelandroid.presentation.encuestas.EncuestaItem;
import pe.gob.osiptelandroid.presentation.signal.dialog.ComentarioDialog;
import pe.gob.osiptelandroid.utils.OnClickListListener;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;


/**
 * Created by katherine on 15/05/17.
 */

public class VerReportesAdapter extends LoaderAdapter<VerReporteEntity> implements OnClickListListener {



    private Context context;
    private EncuestaItem encuestaItem;
    private SessionManager mSessionManager;
    private String cadena;
    private boolean init = true;
    private int row_index;

    public VerReportesAdapter(ArrayList<VerReporteEntity> verReporteEntities, Context context) {
        super(context);
        setItems(verReporteEntities);
        this.context = context;
        mSessionManager = new SessionManager(context);
    }

    public ArrayList<VerReporteEntity> getItems() {
        return (ArrayList<VerReporteEntity>) getmItems();
    }

    @Override
    public long getYourItemId(int position) {
        return 0;
    }

    @Override
    public RecyclerView.ViewHolder getYourItemViewHolder(ViewGroup parent) {
        View root = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_list_ver_reportes, parent, false);
        return new ViewHolder(root, this);
    }

    @Override
    public void bindYourViewHolder(final RecyclerView.ViewHolder holder, final int position) {
        final VerReporteEntity verReporteEntity = getItems().get(position);
        ((ViewHolder) holder).tvTitle.setText("REPORTE N° " + String.valueOf(verReporteEntity.getNumEncuesta()));
        ((ViewHolder) holder).tvTitleToolbar.setText("REPORTE N° " + String.valueOf(verReporteEntity.getNumEncuesta()));
        ((ViewHolder) holder).tvUsuario.setText(String.valueOf(verReporteEntity.getNombre()));
        ((ViewHolder) holder).tvEmpresa.setText(verReporteEntity.getEmpresa());
        ((ViewHolder) holder).tvFechaVisita.setText(verReporteEntity.getFechaVisita());
        ((ViewHolder) holder).tvFechaRegistro.setText(verReporteEntity.getFechaRegistro());
        if(verReporteEntity.getComentario() != null){
            ((ViewHolder) holder).lyVerComentario.setVisibility(View.VISIBLE);
        }else{
            ((ViewHolder) holder).lyVerComentario.setVisibility(View.GONE);
        }

        ((ViewHolder) holder).rlToolbar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((ViewHolder) holder).rlToolbar.setVisibility(View.GONE);
                ((ViewHolder) holder).rlToolbarList.setVisibility(View.VISIBLE);
                ((ViewHolder) holder).content.setVisibility(View.VISIBLE);
                row_index = position;
                notifyDataSetChanged();

            }
        });
        ((ViewHolder) holder).rlToolbarList.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((ViewHolder) holder).rlToolbar.setVisibility(View.VISIBLE);
                ((ViewHolder) holder).rlToolbarList.setVisibility(View.GONE);
                ((ViewHolder) holder).content.setVisibility(View.GONE);
            }
        });

        if (row_index == position) {
            ((ViewHolder) holder).rlToolbar.setVisibility(View.GONE);
            ((ViewHolder) holder).rlToolbarList.setVisibility(View.VISIBLE);
            ((ViewHolder) holder).content.setVisibility(View.VISIBLE);

        } else {
            ((ViewHolder) holder).rlToolbar.setVisibility(View.VISIBLE);
            ((ViewHolder) holder).rlToolbarList.setVisibility(View.GONE);
            ((ViewHolder) holder).content.setVisibility(View.GONE);
        }


        ((ViewHolder) holder).lyVerComentario.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ComentarioDialog comentarioDialog = new ComentarioDialog(context, verReporteEntity.getComentario());
                comentarioDialog.show();
                comentarioDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
            }
        });


    }

    @Override
    public void onClick(int position) {
    }


    static class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        private OnClickListListener onClickListListener;

        @BindView(R.id.tv_title_toolbar)
        TextView tvTitleToolbar;
        @BindView(R.id.tv_title)
        TextView tvTitle;
        @BindView(R.id.rl_toolbar)
        RelativeLayout rlToolbar;
        @BindView(R.id.rl_toolbar_list)
        RelativeLayout rlToolbarList;
        @BindView(R.id.tv_usuario)
        TextView tvUsuario;
        @BindView(R.id.tv_empresa)
        TextView tvEmpresa;
        @BindView(R.id.tv_fecha_visita)
        TextView tvFechaVisita;
        @BindView(R.id.tv_fecha_registro)
        TextView tvFechaRegistro;
        @BindView(R.id.content)
        ConstraintLayout content;
        @BindView(R.id.ly_ver_comentario)
        LinearLayout lyVerComentario;

        ViewHolder(View itemView, OnClickListListener onClickListListener) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            this.onClickListListener = onClickListListener;
            this.itemView.setOnClickListener(this);
        }


        @Override
        public void onClick(View v) {
            onClickListListener.onClick(getAdapterPosition());
        }
    }
}
