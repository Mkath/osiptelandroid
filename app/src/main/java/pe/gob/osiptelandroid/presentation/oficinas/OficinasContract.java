package pe.gob.osiptelandroid.presentation.oficinas;
import pe.gob.osiptelandroid.core.BasePresenter;
import pe.gob.osiptelandroid.core.BaseView;
import pe.gob.osiptelandroid.data.entities.DepartamentoEntity;
import pe.gob.osiptelandroid.data.entities.OficinasEntity;

import java.util.ArrayList;

/**
 * Created by katherine on 12/05/17.
 */

public interface OficinasContract {
    interface View extends BaseView<Presenter> {
        void getListOficinas(ArrayList<OficinasEntity> list);
        void getListDepartamentos(ArrayList<DepartamentoEntity> list);
        boolean isActive();
    }

    interface Presenter extends BasePresenter {

        void getOficinas(int idDepartamento, String nombreDepartamento, String nombreUsuario, String numeroCel);
        void getDepartamentos();
        void getRefreshToken();

      /*  void loadOrdersFromPage(int page, int id);

        void loadfromNextPage(int id);

        void startLoad(int id);

        void getPromos(int page, int id);*/

    }
}
