package pe.gob.osiptelandroid.presentation.signal.consultar;
import pe.gob.osiptelandroid.core.BasePresenter;
import pe.gob.osiptelandroid.core.BaseView;
import pe.gob.osiptelandroid.data.entities.CoberturaByOperadoraEntity;
import pe.gob.osiptelandroid.data.entities.DepartamentoEntity;
import pe.gob.osiptelandroid.data.entities.DistritoEntity;
import pe.gob.osiptelandroid.data.entities.LocalidadEntity;
import pe.gob.osiptelandroid.data.entities.OperadoraEntity;
import pe.gob.osiptelandroid.data.entities.ProvinciaEntity;

import java.util.ArrayList;

/**
 * Created by katherine on 12/05/17.
 */

public interface ConsultarSignalContract {
    interface View extends BaseView<Presenter> {

        void getListUbicacion();
        void getListDepartamentos(ArrayList<DepartamentoEntity> list);
        void getListOperadores(ArrayList<OperadoraEntity> list);
        void getListProvincia(ArrayList<ProvinciaEntity> list);
        void getListDistrito(ArrayList<DistritoEntity> list);
        void getListLocalidades(ArrayList<LocalidadEntity> list);

        void getListCobertura(ArrayList<CoberturaByOperadoraEntity> list);

        boolean isActive();
    }

    interface Presenter extends BasePresenter {

        void getUbicacion();

        void getOperadores();

        void getDepartamentos();

        void getProvincia(String codDepartamento);

        void getDistrito(String codDepartamento, String codProvincia);

        void getLocalidades(String codDepartamento, String codProvincia, String codDistrito);

        void consultarCobertura(String departamento,
                                String provincia,
                                String distrito,
                                String localidad,
                                String idOperadora, String nombreUsuario, String numeroCel);

        void getRefreshToken();


      /*  void loadOrdersFromPage(int page, int id);

        void loadfromNextPage(int id);

        void startLoad(int id);

        void getPromos(int page, int id);*/

    }
}
