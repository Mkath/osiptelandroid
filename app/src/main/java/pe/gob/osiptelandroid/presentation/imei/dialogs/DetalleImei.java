package pe.gob.osiptelandroid.presentation.imei.dialogs;


import android.app.AlertDialog;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import pe.gob.osiptelandroid.R;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class DetalleImei extends AlertDialog {
    @BindView(R.id.im_close)
    ImageView imClose;
    @BindView(R.id.tv_estado_equipo)
    TextView tvEstadoEquipo;
    @BindView(R.id.tv_imei_serie)
    TextView tvImeiSerie;
    @BindView(R.id.tv_empresa_operadora)
    TextView tvEmpresaOperadora;
    @BindView(R.id.tv_fecha)
    TextView tvFecha;

    public DetalleImei(Context context, String estadoEquipo, String imeiSerie, String empresa, String fecha) {
        super(context);
        LayoutInflater inflater = LayoutInflater.from(getContext());
        final View view = inflater.inflate(R.layout.dialog_detalle_imei, null);
        setView(view);
        ButterKnife.bind(this, view);
        setUI(estadoEquipo,imeiSerie,empresa,fecha);
    }

    private void setUI(String estadoEquipo, String imeiSerie, String empresa, String fecha){
        tvEmpresaOperadora.setText(empresa);
        tvEstadoEquipo.setText(estadoEquipo);
        tvFecha.setText(fecha);
        tvImeiSerie.setText(imeiSerie);
    }

    @OnClick(R.id.im_close)
    public void onViewClicked() {
        dismiss();
    }
}
