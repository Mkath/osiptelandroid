package pe.gob.osiptelandroid.presentation.imei.dialogs;


import android.app.AlertDialog;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;

import pe.gob.osiptelandroid.R;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class ObtenerImeiDialog extends AlertDialog {
    @BindView(R.id.im_close)
    ImageView imClose;

    public ObtenerImeiDialog(Context context) {
        super(context);

        LayoutInflater inflater = LayoutInflater.from(getContext());
        final View view = inflater.inflate(R.layout.dialog_obtener_imei, null);
        setView(view);
        ButterKnife.bind(this, view);
    }

    @OnClick(R.id.im_close)
    public void onViewClicked() {
        dismiss();
    }
}
