package pe.gob.osiptelandroid.presentation.signal.reportar;

import android.app.Activity;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import pe.gob.osiptelandroid.R;
import pe.gob.osiptelandroid.core.BaseFragment;
import pe.gob.osiptelandroid.data.entities.ServicioOperadorEntity;
import pe.gob.osiptelandroid.data.local.SessionManager;
import pe.gob.osiptelandroid.utils.ActivityUtils;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;

/**
 * Created by junior on 27/08/16.
 */
public class ServiciosListFragment extends BaseFragment {

    private static final String TAG = ServiciosListFragment.class.getSimpleName();
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.rv_list)
    RecyclerView rvList;
    Unbinder unbinder;
    @BindView(R.id.noListIcon)
    ImageView noListIcon;
    @BindView(R.id.noListMain)
    TextView noListMain;
    @BindView(R.id.noList)
    LinearLayout noList;
    @BindView(R.id.button_next)
    RelativeLayout buttonNext;

    private ServiciosListAdapter mAdapter;
    private LinearLayoutManager mlinearLayoutManager;
    private SessionManager mSessionManager;
    private sendDataToReportInterface mCallback;
    private ArrayList<ServicioOperadorEntity> list;
    public ServiciosListFragment() {
        // Requires empty public constructor
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
    }

    @OnClick(R.id.button_next)
    public void onViewClicked() {
        if(mSessionManager.getArrayListaServiciosOperadores(SessionManager.ARRAY_LISTA_SERVICIOS)==null ||
                mSessionManager.getArrayListaServiciosOperadores(SessionManager.ARRAY_LISTA_SERVICIOS).size()== 0){
            Toast.makeText(getContext(), "Por favor seleccione uno o más tipos de servicios que desea reportar", Toast.LENGTH_SHORT).show();
        }else{
            mCallback.sendDataToReport(mSessionManager.getArrayListaServiciosOperadores(SessionManager.ARRAY_LISTA_SERVICIOS));
        }
    }


    public interface sendDataToReportInterface {
        void sendDataToReport(ArrayList<ServicioOperadorEntity> list);
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);

        // This makes sure that the container activity has implemented
        // the callback interface. If not, it throws an exception
        try {
            mCallback = (sendDataToReportInterface) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement TextClicked");
        }
    }

    @Override
    public void onDetach() {
        mCallback = null; // => avoid leaking, thanks @Deepscorn
        super.onDetach();
    }


    public static ServiciosListFragment newInstance(Bundle bundle) {
        ServiciosListFragment fragment = new ServiciosListFragment();
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mSessionManager = new SessionManager(getContext());
        list = new ArrayList<>();
        list = (ArrayList<ServicioOperadorEntity>) getArguments().getSerializable("servicioOperadorList");
    }

    @Override
    public void onResume() {
        super.onResume();

    }


    @Override
    public void onStart() {
        super.onStart();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.list_signal_fragment, container, false);
        unbinder = ButterKnife.bind(this, root);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mCallback.sendDataToReport(null);
            }
        });


        return root;
    }


    void remove() {
        ActivityUtils.removeFragment(getActivity().getSupportFragmentManager(),
                this);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        //mAdapter = new VerReportesAdapter(getList());
        if(mSessionManager.getArrayListaServiciosOperadores(SessionManager.ARRAY_LISTA_SERVICIOS)!=null){
            mAdapter = new ServiciosListAdapter(new ArrayList<ServicioOperadorEntity>(), getContext(), mSessionManager.getArrayListaServiciosOperadores(SessionManager.ARRAY_LISTA_SERVICIOS));
        }else{
            mAdapter = new ServiciosListAdapter(new ArrayList<ServicioOperadorEntity>(), getContext(), null);
        }
        mlinearLayoutManager = new LinearLayoutManager(getContext());
        mlinearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        rvList.setAdapter(mAdapter);
        rvList.setLayoutManager(mlinearLayoutManager);
        if(list!=null){
            mAdapter.setItems(list);
        }
    }

    private void getNewList() {
        ArrayList<ServicioOperadorEntity> list = new ArrayList<>();
        list.add(new ServicioOperadorEntity("Servicio 1"));
        list.add(new ServicioOperadorEntity("Servicio 2"));
        list.add(new ServicioOperadorEntity("Servicio 3"));
        list.add(new ServicioOperadorEntity("Servicio 4"));
        list.add(new ServicioOperadorEntity("Servicio 5"));
        mAdapter.setItems(list);
    }

}
