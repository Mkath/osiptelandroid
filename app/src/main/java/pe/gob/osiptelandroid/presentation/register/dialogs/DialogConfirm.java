package pe.gob.osiptelandroid.presentation.register.dialogs;


import android.app.AlertDialog;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import pe.gob.osiptelandroid.R;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class DialogConfirm extends AlertDialog {
    @BindView(R.id.im_close)
    ImageView imClose;
    @BindView(R.id.message)
    TextView message;
    @BindView(R.id.im_icon)
    ImageView imIcon;

    public DialogConfirm(Context context, String msg, Boolean isError, boolean hidenClose) {
        super(context);
        LayoutInflater inflater = LayoutInflater.from(getContext());
        final View view = inflater.inflate(R.layout.dialog_register_confirm, null);
        setView(view);
        ButterKnife.bind(this, view);
        message.setText(msg);
        if (isError) {
            imIcon.setImageDrawable(context.getResources().getDrawable(R.drawable.ic_error));
        }
        if(hidenClose){
            imClose.setVisibility(View.GONE);
        }
    }


    @OnClick(R.id.im_close)
    public void onViewClicked() {
        dismiss();
    }
}
