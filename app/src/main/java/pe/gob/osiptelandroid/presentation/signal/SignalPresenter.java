package pe.gob.osiptelandroid.presentation.signal;

import android.content.Context;
import android.util.Log;

import pe.gob.osiptelandroid.data.entities.APIError;
import pe.gob.osiptelandroid.data.entities.EnlaceAppMovilEntity;
import pe.gob.osiptelandroid.data.local.SessionManager;
import pe.gob.osiptelandroid.data.remote.ServiceFactory;
import pe.gob.osiptelandroid.data.remote.request.ListRequest;
import pe.gob.osiptelandroid.utils.ErrorUtils;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SignalPresenter implements SignalContract.Presenter{

    private SignalContract.View mView;
    private Context context;
    private SessionManager mSessionManager;

    public SignalPresenter(SignalContract.View mView, Context context) {
        this.context = context;
        this.mView = mView;
        this.mView.setPresenter(this);
        mSessionManager = new SessionManager(context);
    }

    @Override
    public void start() {

    }

    @Override
    public void getShareLink(String aplicacion,String global) {
        mView.setLoadingIndicator(true);
        ListRequest listRequest = ServiceFactory.createService(ListRequest.class);
        Call<EnlaceAppMovilEntity> orders = listRequest.getEnlacesAppmovil(
                "Bearer "+mSessionManager.getUserToken(), aplicacion, global);
        orders.enqueue(new Callback<EnlaceAppMovilEntity>() {
            @Override
            public void onResponse(Call<EnlaceAppMovilEntity> call, Response<EnlaceAppMovilEntity> response) {

                if (response.isSuccessful()) {
                    mView.setLoadingIndicator(false);
                    mView.setShareLink(response.body());

                } else {
                    mView.setLoadingIndicator(false);

                }
            }

            @Override
            public void onFailure(Call<EnlaceAppMovilEntity> call, Throwable t) {
                mView.setLoadingIndicator(false);
            }
        });
    }
}
