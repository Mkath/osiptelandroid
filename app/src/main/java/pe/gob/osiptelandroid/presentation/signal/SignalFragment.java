package pe.gob.osiptelandroid.presentation.signal;

import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Build;
import android.os.Bundle;
import android.os.Looper;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.BottomSheetBehavior;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import pe.gob.osiptelandroid.R;
import pe.gob.osiptelandroid.core.BaseFragment;
import pe.gob.osiptelandroid.data.entities.CoberturaByOperadoraEntity;
import pe.gob.osiptelandroid.data.entities.EnlaceAppMovilEntity;
import pe.gob.osiptelandroid.data.entities.TecnologiaEntity;
import pe.gob.osiptelandroid.data.local.SessionManager;
import pe.gob.osiptelandroid.presentation.signal.consultar.ConsultarSignalAdapter;
import pe.gob.osiptelandroid.presentation.signal.consultar.ConsultarSignalFragment;
import pe.gob.osiptelandroid.presentation.signal.consultar.ItemConsultaAdapter;
import pe.gob.osiptelandroid.presentation.signal.reportar.ReportarSignalFragment;
import pe.gob.osiptelandroid.presentation.signal.verreportes.VerReportesFragment;
import pe.gob.osiptelandroid.utils.ActivityUtils;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Target;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;

/**
 * Created by junior on 27/08/16.
 */
public class SignalFragment extends BaseFragment implements OnMapReadyCallback,
        GoogleMap.OnMarkerClickListener, LocationListener, SignalContract.View {

    private static final String TAG = SignalFragment.class.getSimpleName();

    GoogleMap mMap;
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.button_consultar_cobertura)
    RelativeLayout buttonConsultarCobertura;
    @BindView(R.id.button_reportar_cobertura)
    RelativeLayout buttonReportarCobertura;
    Unbinder unbinder;
    @BindView(R.id.bottom_sheet)
    RelativeLayout bottomSheet;
    @BindView(R.id.im_back)
    ImageView imBack;
    @BindView(R.id.im_share)
    ImageView imShare;
    @BindView(R.id.leyenda)
    RelativeLayout leyenda;

    private View mapView;
    SupportMapFragment mapFragment;
    private SessionManager mSessionManager;
    private LocationManager mlocManager;
    private boolean isUpdate = false;
    AlertDialog dialogogps;
    Location mLastLocation;
    LocationRequest mLocationRequest;
    private FusedLocationProviderClient mFusedLocationClient;
    LatLng center;
    Geocoder geocoder;
    private boolean firstGps = true;
    private bottomHiden mCallback;
    BottomSheetBehavior sheetBehavior;
    private boolean first = true;
    private double latitude, longitude;
    private String departamento, provincia, distrito, localidad;
    private String dptoConsulta, provConsulta, distritoConsulta, localConsulta;
    private String lat, longi;
    private ArrayList<CoberturaByOperadoraEntity> listCobertura;
    private ArrayList<TecnologiaEntity> listTecnologia;
    private Boolean isExpand;
    private EnlaceAppMovilEntity shareLinkEntity;
    private SignalContract.Presenter mPresenter;
    private ConsultarSignalAdapter mAdapter;
    private LinearLayoutManager mlinearLayoutManager;

    public SignalFragment() {
        // Requires empty public constructor
    }

    public static SignalFragment newInstance() {
        return new SignalFragment();
    }

    @Override
    public void setShareLink(EnlaceAppMovilEntity response) {
        imShare.setEnabled(true);
        shareLinkEntity = response;
    }

    @Override
    public void setPresenter(SignalContract.Presenter presenter) {

    }

    @Override
    public void setLoadingIndicator(boolean active) {

    }

    @Override
    public void showMessage(String message) {

    }

    @Override
    public void showErrorMessage(String message) {

    }


    public interface bottomHiden {
        void sendBottomHiden(Boolean isHiden);
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);

        // This makes sure that the container activity has implemented
        // the callback interface. If not, it throws an exception
        try {
            mCallback = (bottomHiden) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement TextClicked");
        }
    }

    @Override
    public void onDetach() {
        mCallback = null; // => avoid leaking, thanks @Deepscorn
        super.onDetach();
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mPresenter = new SignalPresenter(this,getContext());
        mSessionManager = new SessionManager(getContext());
        mFusedLocationClient = LocationServices.getFusedLocationProviderClient(getContext());
        listCobertura = new ArrayList<>();
        listTecnologia = new ArrayList<>();

        mPresenter.getShareLink(getResources().getString(R.string.name_application),getResources().getString(R.string.name_global_cobertura));

    }

    @Override
    public void onResume() {
        super.onResume();

    }

    public void getDataLocation(String departamento, String provincia, String distrito, String localidad) {
        dptoConsulta = departamento;
        provConsulta = provincia;
        distritoConsulta = distrito;
        localConsulta = localidad;
    }

    public void updateUiConsultaCobertura(final ArrayList<CoberturaByOperadoraEntity> list) {
        buttonConsultarCobertura.setVisibility(View.GONE);
        buttonReportarCobertura.setVisibility(View.GONE);
        leyenda.setVisibility(View.VISIBLE);
        imBack.setVisibility(View.VISIBLE);
        mCallback.sendBottomHiden(true);
        listCobertura = list;
        for (int i = 0; i < list.size(); i++) {
            final String id = list.get(i).getIdUbigeo();
            final LatLng puntos = new LatLng(Double.parseDouble(list.get(i).getLatitud()), Double.parseDouble(list.get(i).getLongitud()));
            if (list.get(i).isTieneCobertura()) {
                Picasso.get().load(R.drawable.marker_celeste).into(new Target() {
                    @Override
                    public void onBitmapLoaded(Bitmap bitmap, Picasso.LoadedFrom from) {
                        Bitmap newBitmap = Bitmap.createScaledBitmap(bitmap, (int) (bitmap.getWidth() * 0.7), (int) (bitmap.getHeight() * 0.7), true);
                        mMap.addMarker(new MarkerOptions().position(puntos).icon(BitmapDescriptorFactory.fromBitmap(newBitmap))).setTag(id);
                    }

                    @Override
                    public void onBitmapFailed(Exception e, Drawable errorDrawable) {

                    }

                    @Override
                    public void onPrepareLoad(Drawable placeHolderDrawable) {

                    }
                });
            } else {
                Picasso.get().load(R.drawable.marker_gris).into(new Target() {
                    @Override
                    public void onBitmapLoaded(Bitmap bitmap, Picasso.LoadedFrom from) {
                        Bitmap newBitmap = Bitmap.createScaledBitmap(bitmap, (int) (bitmap.getWidth() * 0.7), (int) (bitmap.getHeight() * 0.7), true);
                        mMap.addMarker(new MarkerOptions().position(puntos).icon(BitmapDescriptorFactory.fromBitmap(newBitmap))).setTag(id);
                    }

                    @Override
                    public void onBitmapFailed(Exception e, Drawable errorDrawable) {

                    }

                    @Override
                    public void onPrepareLoad(Drawable placeHolderDrawable) {

                    }
                });
            }

            // m.showInfoWindow();
        }

        LatLng cali = new LatLng(Double.valueOf(list.get(0).getLatitud()), Double.valueOf(list.get(0).getLongitud()));

        CameraPosition cameraPosition = CameraPosition.builder()
                .target(cali)
                .zoom(11)
                .build();

        mMap.moveCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));


    }

    @Override
    public void onStart() {
        super.onStart();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.signal_fragment, container, false);
        unbinder = ButterKnife.bind(this, root);
        bottomSheet.setVisibility(View.GONE);
        sheetBehavior = BottomSheetBehavior.from(bottomSheet);
        mlocManager = (LocationManager) getActivity().getSystemService(Context.LOCATION_SERVICE);
        return root;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mapFragment = (SupportMapFragment) getChildFragmentManager().findFragmentById(R.id.map);
        if (mapFragment == null) {
            FragmentManager fr = getFragmentManager();
            FragmentTransaction ft = fr.beginTransaction();
            mapFragment = SupportMapFragment.newInstance();
            ft.replace(R.id.map, mapFragment).commit();
        }
        mapFragment.getMapAsync(this);
        mapView = mapFragment.getView();
    }

    LocationCallback mLocationCallback = new LocationCallback() {
        @Override
        public void onLocationResult(LocationResult locationResult) {
            for (Location location : locationResult.getLocations()) {
                if (getContext() != null) {
                    mLastLocation = location;
                    latitude = location.getLatitude();
                    longitude = location.getLongitude();
                    if (first) {
                        LatLng latLng = new LatLng(latitude, longitude);
                        mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(latLng, 10));
                        first = false;
                    }
                    //getDireccion();
                }
            }
        }
    };
    @Override
    public boolean onMarkerClick(final Marker marker) {
        isExpand = true;
        getData(marker.getTag().toString());
        bottomSheet.setVisibility(View.VISIBLE);
        sheetBehavior.setState(BottomSheetBehavior.STATE_HIDDEN);
        int height = getActivity().getWindow().getDecorView().getHeight();
        sheetBehavior.setPeekHeight(height / 12);
        sheetBehavior.setState(BottomSheetBehavior.STATE_COLLAPSED);
        bottomSheet.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sheetBehavior.setState(BottomSheetBehavior.STATE_EXPANDED);
            }
        });
        sheetBehavior.setBottomSheetCallback(new BottomSheetBehavior.BottomSheetCallback() {
            @Override
            public void onStateChanged(@NonNull final View bottomSheet, int newState) {
                TextView tvDepartamentos = bottomSheet.findViewById(R.id.tv_departamento);
                TextView tvProvincia = bottomSheet.findViewById(R.id.tv_provincia);
                TextView tvDistrito = bottomSheet.findViewById(R.id.tv_distrito);
                TextView tvLocalidad = bottomSheet.findViewById(R.id.tv_localidad);
                TextView tvLatitud = bottomSheet.findViewById(R.id.tv_latitud);
                TextView tvLongitud = bottomSheet.findViewById(R.id.tv_longitud);
                RecyclerView rvList = bottomSheet.findViewById(R.id.rv_list);
                ImageView imUp = bottomSheet.findViewById(R.id.im_up);
                RelativeLayout btnReportar = bottomSheet.findViewById(R.id.button_reportar_cobertura);
                RelativeLayout btnVerReportes = bottomSheet.findViewById(R.id.button_ver_reporte);
                ItemConsultaAdapter itemAdapter = new ItemConsultaAdapter(new ArrayList<TecnologiaEntity>(), getContext(), listTecnologia.size());
                mlinearLayoutManager = new LinearLayoutManager(getContext());
                mlinearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
                if (rvList != null) {
                    rvList.setAdapter(itemAdapter);
                    rvList.setLayoutManager(mlinearLayoutManager);
                }
                imUp.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if(isExpand){
                            sheetBehavior.setState(BottomSheetBehavior.STATE_EXPANDED);
                            isExpand = false;
                        }else{
                            sheetBehavior.setState(BottomSheetBehavior.STATE_COLLAPSED);
                            isExpand = true;
                        }
                    }
                });
                btnReportar.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        bottomSheet.setVisibility(View.GONE);
                        sheetBehavior.setState(BottomSheetBehavior.STATE_HIDDEN);
                        Bundle bundle = new Bundle();
                        bundle.putString("departamento", dptoConsulta);
                        bundle.putString("provincia", provConsulta);
                        bundle.putString("distrito", distritoConsulta);
                        bundle.putString("localidad", localConsulta);
                        bundle.putString("latitud", lat);
                        bundle.putString("longitud", longi);
                        ReportarSignalFragment reportarSignalFragment = ReportarSignalFragment.newInstance(bundle);
                        ActivityUtils.addFragmentToFragment(getActivity().getSupportFragmentManager(),
                                reportarSignalFragment, R.id.body_signal, "Reportar Signal");
                    }
                });

                btnVerReportes.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        bottomSheet.setVisibility(View.GONE);
                        sheetBehavior.setState(BottomSheetBehavior.STATE_HIDDEN);
                        Bundle bundle = new Bundle();
                        bundle.putString("departamento", departamento);
                        bundle.putString("provincia", provincia);
                        bundle.putString("distrito", distrito);
                        bundle.putString("localidad", localidad);
                        VerReportesFragment verReportesFragment = VerReportesFragment.newInstance(bundle);
                        ActivityUtils.addFragmentToFragment(getActivity().getSupportFragmentManager(),
                                verReportesFragment, R.id.body_signal, "Return");
                    }
                });

                switch (newState) {
                    case BottomSheetBehavior.STATE_HIDDEN:
                        tvProvincia.setText(provincia);
                        tvDepartamentos.setText(departamento);
                        tvDistrito.setText(distrito);
                        tvLocalidad.setText(localidad);
                        tvLatitud.setText(lat);
                        tvLongitud.setText(longi);
                        itemAdapter.setItems(listTecnologia);
                        break;

                    case BottomSheetBehavior.STATE_EXPANDED: {
                        tvDepartamentos.setText(departamento);
                        tvProvincia.setText(provincia);
                        tvDistrito.setText(distrito);
                        tvLocalidad.setText(localidad);
                        tvLatitud.setText(lat);
                        tvLongitud.setText(longi);
                        imUp.setImageDrawable(getResources().getDrawable(R.drawable.ic_keyboard_arrow_down));
                        itemAdapter.setItems(listTecnologia);
                    }
                    break;

                    case BottomSheetBehavior.STATE_COLLAPSED: {
                        tvProvincia.setText(provincia);
                        tvDepartamentos.setText(departamento);
                        tvDistrito.setText(distrito);
                        tvLocalidad.setText(localidad);
                        tvLatitud.setText(lat);
                        tvLongitud.setText(longi);
                        imUp.setImageDrawable(getResources().getDrawable(R.drawable.ic_keyboard_arrow_up));
                        itemAdapter.setItems(listTecnologia);
                    }
                    break;

                    case BottomSheetBehavior.STATE_DRAGGING:
                        itemAdapter.setItems(listTecnologia);
                        tvProvincia.setText(provincia);
                        tvDepartamentos.setText(departamento);
                        tvDistrito.setText(distrito);
                        tvLocalidad.setText(localidad);
                        tvLatitud.setText(lat);
                        tvLongitud.setText(longi);
                        break;

                    case BottomSheetBehavior.STATE_SETTLING:
                        itemAdapter.setItems(listTecnologia);
                        tvProvincia.setText(provincia);
                        tvDepartamentos.setText(departamento);
                        tvDistrito.setText(distrito);
                        tvLocalidad.setText(localidad);
                        tvLatitud.setText(lat);
                        tvLongitud.setText(longi);
                        break;
                }
            }

            @Override
            public void onSlide(@NonNull View bottomSheet, float slideOffset) {
                ImageView imUp = bottomSheet.findViewById(R.id.im_up);
                imUp.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if(isExpand){
                            sheetBehavior.setState(BottomSheetBehavior.STATE_EXPANDED);
                            isExpand = false;
                        }else{
                            sheetBehavior.setState(BottomSheetBehavior.STATE_COLLAPSED);
                            isExpand = true;
                        }
                    }
                });
            }
        });
        return false;
    }

    private void getData(String tag) {
        for (int i = 0; i < listCobertura.size(); i++) {
            if (tag.equals(listCobertura.get(i).getIdUbigeo())) {
                departamento = listCobertura.get(i).getDepartamento();
                provincia = listCobertura.get(i).getProvincia();
                distrito = listCobertura.get(i).getDistrito();
                localidad = listCobertura.get(i).getLocalidad();
                lat = listCobertura.get(i).getLatitud();
                longi = listCobertura.get(i).getLongitud();
                listTecnologia = listCobertura.get(i).getTecnologisOperador();
            }
        }
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        mMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);
        mMap.getUiSettings().setMapToolbarEnabled(false);
        mMap.getUiSettings().setCompassEnabled(false);
        mMap.getUiSettings().setRotateGesturesEnabled(false);
        mMap.setOnMarkerClickListener(this);
    }


    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
        if(mlocManager!=null){
            mlocManager.removeUpdates(this);
        }
    }

    @OnClick({R.id.button_consultar_cobertura, R.id.button_reportar_cobertura, R.id.im_back, R.id.im_share})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.button_consultar_cobertura:
                ConsultarSignalFragment consultarSignalFragment = ConsultarSignalFragment.newInstance();
                ActivityUtils.addFragmentToFragment(getActivity().getSupportFragmentManager(),
                        consultarSignalFragment, R.id.body_signal, "Consultar Signal");
                break;

            case R.id.button_reportar_cobertura:
                Bundle bundle = new Bundle();
                bundle.putString("departamento", null);
                bundle.putString("provincia", null);
                bundle.putString("distrito", null);
                bundle.putString("localidad", null);
                ReportarSignalFragment reportarSignalFragment = ReportarSignalFragment.newInstance(bundle);
                ActivityUtils.addFragmentToFragment(getActivity().getSupportFragmentManager(),
                        reportarSignalFragment, R.id.body_signal, "Reportar Signal");
                break;

            case R.id.im_back:
                updateBack();
                break;

            case R.id.im_share:
                shareTextUrl();
                break;
        }
    }

    public void updateBack() {
        leyenda.setVisibility(View.GONE);
        imBack.setVisibility(View.GONE);
        buttonReportarCobertura.setVisibility(View.VISIBLE);
        buttonConsultarCobertura.setVisibility(View.VISIBLE);
        mMap.clear();
        mCallback.sendBottomHiden(false);
        bottomSheet.setVisibility(View.GONE);
        if (latitude != 0.0) {
            LatLng latLng = new LatLng(latitude, longitude);
            mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(latLng, 10));
        } else {
            setMapToCenter();
        }
    }

    private void shareTextUrl() {
        Intent share = new Intent(Intent.ACTION_SEND);
        share.setType("text/plain");
        share.addFlags(Intent.FLAG_ACTIVITY_CLEAR_WHEN_TASK_RESET);
        // Add data to the intent, the receiving app will decide
        // what to do with it.
        share.putExtra(Intent.EXTRA_SUBJECT, "Prueba Osiptel");
        share.putExtra(Intent.EXTRA_TEXT, shareLinkEntity.getValor());
        startActivity(Intent.createChooser(share, "Share link!"));
    }

    @Override
    public void onLocationChanged(Location location) {

    }

    @Override
    public void onStatusChanged(String s, int i, Bundle bundle) {

    }

    @Override
    public void onProviderEnabled(String s) {

    }

    @Override
    public void onProviderDisabled(String s) {

    }

    public void setMapToCenter() {
        latitude = -12.0469294;
        longitude = -77.0437909;
        LatLng latLng = new LatLng(latitude, longitude);
        mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(latLng, 10));
        first = false;
    }

    public void alertafalta() {
        AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
        builder.setTitle("Advertencia");
        builder.setMessage("El sistema GPS esta desactivado, para continuar presione el boton activar");
        builder.setCancelable(true);
        builder.setNegativeButton("Cancelar", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialogogps.dismiss();
            }
        });
        builder.setPositiveButton("Activar", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialogo1, int id) {
                startActivity(new Intent
                        (Settings.ACTION_LOCATION_SOURCE_SETTINGS));
            }
        });
        dialogogps = builder.create();
        dialogogps.show();
        dialogogps.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialog) {
                mSessionManager.setGPSstatus(false);
                setMapToCenter();
            }
        });
    }

    public void updateLocation() {
        if (!mlocManager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
            // Call your Alert message
            if (mSessionManager.getFirtsGPSstatus()) {
                startActivity(new Intent
                        (Settings.ACTION_LOCATION_SOURCE_SETTINGS));
                mSessionManager.FirtsGPSstatus(false);
                mSessionManager.setGPSstatus(false);
            } else {
                if (mSessionManager.getGPSstatus()) {
                    alertafalta();
                }
            }
        }
        mLocationRequest = LocationRequest.create();
        mLocationRequest.setInterval(1000);
        mLocationRequest.setFastestInterval(1000);
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (ContextCompat.checkSelfPermission(getContext(), Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
                //locationStart();
            } else {
                //checkLocationPermission();
            }
        }

        mFusedLocationClient.requestLocationUpdates(mLocationRequest, mLocationCallback, Looper.myLooper());

        mMap.setMyLocationEnabled(true);
    }


}
