package pe.gob.osiptelandroid.presentation.splash;

import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import pe.gob.osiptelandroid.R;
import pe.gob.osiptelandroid.core.BaseActivity;
import pe.gob.osiptelandroid.data.entities.body.BodyEntityLog;
import pe.gob.osiptelandroid.data.local.SessionManager;
import pe.gob.osiptelandroid.presentation.main.MainActivity;
import pe.gob.osiptelandroid.presentation.register.RegisterActivity;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import pe.gob.osiptelandroid.utils.AppConstants;
import pe.gob.osiptelandroid.utils.LogContract;
import pe.gob.osiptelandroid.utils.LogPresenter;

/**
 * Created by junior on 27/08/16.
 */
public class InitialActivity extends BaseActivity {

    private static final String TAG = InitialActivity.class.getSimpleName();
    @BindView(R.id.background)
    ImageView background;
    @BindView(R.id.button_ingresar)
    RelativeLayout buttonIngresar;
    @BindView(R.id.button_registro)
    RelativeLayout buttonRegistro;

    private BodyEntityLog bodyEntityLog;
    private LogContract.Presenter mPresenterLog;
    private SessionManager mSessionManager;

    public InitialActivity() {
        // Requires empty public constructor
    }

    public static InitialActivity newInstance() {
        return new InitialActivity();
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_initial);
        ButterKnife.bind(this);

        mPresenterLog = new LogPresenter( getApplicationContext());
        mSessionManager = new SessionManager(getApplicationContext());
        bodyEntityLog = new BodyEntityLog();
    }

    @Override
    protected void onResume() {
        super.onResume();
        buttonIngresar.setEnabled(true);
        buttonRegistro.setEnabled(true);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    @OnClick({R.id.button_ingresar, R.id.button_registro})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.button_ingresar:
                buttonIngresar.setEnabled(false);
                nextActivity(this,null, MainActivity.class, true);

                bodyEntityLog.setAccion(AppConstants.ACTION_GETIN_APP);
                bodyEntityLog.setMac(mSessionManager.getMac());
                bodyEntityLog.setMarcaCelular(Build.BRAND);
                bodyEntityLog.setModeloCelular(Build.MODEL);
                if(mSessionManager.getUserEntity()!= null){
                    bodyEntityLog.setNombreUsuario(mSessionManager.getUserEntity().getIdUsuario()+"");
                    bodyEntityLog.setNumeroCelular(mSessionManager.getUserEntity().getNroTelefono());
                }else{
                    bodyEntityLog.setNombreUsuario(" ");
                    bodyEntityLog.setNumeroCelular(" ");
                }

                bodyEntityLog.setSistemaOperativo("Android");

                mPresenterLog.sendLog(bodyEntityLog);

                break;
            case R.id.button_registro:
                buttonRegistro.setEnabled(false);
                nextActivity(this,null, RegisterActivity.class, true);

                bodyEntityLog.setAccion(AppConstants.ACTION_GETIN_REGISTER_APP);
                bodyEntityLog.setMac(mSessionManager.getMac());
                bodyEntityLog.setMarcaCelular(Build.BRAND);
                bodyEntityLog.setModeloCelular(Build.MODEL);
                if(mSessionManager.getUserEntity()!= null){
                    bodyEntityLog.setNombreUsuario(mSessionManager.getUserEntity().getIdUsuario()+"");
                    bodyEntityLog.setNumeroCelular(mSessionManager.getUserEntity().getNroTelefono());
                }else{
                    bodyEntityLog.setNombreUsuario(" ");
                    bodyEntityLog.setNumeroCelular(" ");
                }

                bodyEntityLog.setSistemaOperativo("Android");

                mPresenterLog.sendLog(bodyEntityLog);
                break;
        }
    }

}
