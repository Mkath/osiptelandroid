package pe.gob.osiptelandroid.presentation.imei.reclamo;
import pe.gob.osiptelandroid.core.BasePresenter;
import pe.gob.osiptelandroid.core.BaseView;
import pe.gob.osiptelandroid.data.entities.MarcaEntity;
import pe.gob.osiptelandroid.data.entities.ModeloEntity;
import pe.gob.osiptelandroid.data.entities.OperadoraEntity;
import pe.gob.osiptelandroid.data.entities.ProblemaEntity;
import pe.gob.osiptelandroid.data.entities.body.BodyReportarImei;

import java.util.ArrayList;

/**
 * Created by katherine on 12/05/17.
 */

public interface ReclamoImeiContract {
    interface View extends BaseView<Presenter> {
        void getListMarca(ArrayList<MarcaEntity> list);
        void getListModelo(ArrayList<ModeloEntity> list);
        void getListEmpresa(ArrayList<OperadoraEntity> list);
        void getListProblema(ArrayList<ProblemaEntity> list);
        boolean isActive();
    }

    interface Presenter extends BasePresenter {

        void sendReport(BodyReportarImei bodyReportarImei);

        void getMarca();
        void getModelo(int idMarca);
        void getEmpresas();
        void getProblemas();
        void getRefreshToken();
    }
}
