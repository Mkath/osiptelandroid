package pe.gob.osiptelandroid.presentation.signal.consultar;

import android.app.Activity;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.Toast;

import pe.gob.osiptelandroid.R;
import pe.gob.osiptelandroid.core.BaseFragment;
import pe.gob.osiptelandroid.data.entities.CoberturaByOperadoraEntity;
import pe.gob.osiptelandroid.data.entities.DepartamentoEntity;
import pe.gob.osiptelandroid.data.entities.DistritoEntity;
import pe.gob.osiptelandroid.data.entities.LocalidadEntity;
import pe.gob.osiptelandroid.data.entities.OperadoraEntity;
import pe.gob.osiptelandroid.data.entities.ProvinciaEntity;
import pe.gob.osiptelandroid.data.entities.body.BodyEntityLog;
import pe.gob.osiptelandroid.data.local.SessionManager;
import pe.gob.osiptelandroid.presentation.register.dialogs.DialogConfirm;
import pe.gob.osiptelandroid.presentation.verficarlinea.VerificarLineaItem;
import pe.gob.osiptelandroid.utils.ActivityUtils;
import pe.gob.osiptelandroid.utils.AppConstants;
import pe.gob.osiptelandroid.utils.LogContract;
import pe.gob.osiptelandroid.utils.LogPresenter;
import pe.gob.osiptelandroid.utils.ProgressDialogCustom;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;

/**
 * Created by junior on 27/08/16.
 */
public class ConsultarSignalFragment extends BaseFragment implements
        ConsultarSignalContract.View, VerificarLineaItem {

    private static final String TAG = ConsultarSignalFragment.class.getSimpleName();
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.spinner_departamento)
    Spinner spinnerDepartamento;
    @BindView(R.id.spinner_provincia)
    Spinner spinnerProvincia;
    @BindView(R.id.spinner_distrito)
    Spinner spinnerDistrito;
    @BindView(R.id.spinner_localidad)
    Spinner spinnerLocalidad;
    @BindView(R.id.rv_list)
    RecyclerView rvList;
    @BindView(R.id.button_filtrar_cobertura)
    RelativeLayout buttonFiltrarCobertura;
    Unbinder unbinder;
    private String text;
    private ConsultarSignalContract.Presenter mPresenter;
    private ConsultarSignalAdapter mAdapter;
    private LinearLayoutManager mlinearLayoutManager;
    private ProgressDialogCustom mProgressDialogCustom;
    private SessionManager mSessionManager;
    private ArrayList<DepartamentoEntity> mList;
    private String codDept = "Seleccionar";
    private String codProv = "Seleccionar";
    private String codDistr = "Seleccionar";
    private String codLocl = "Seleccionar";
    private String ubicacion;
    private BodyEntityLog bodyEntityLog;
    private LogContract.Presenter mPresenterLog;

    private sendConsultaCobertura mCallback;

    public ConsultarSignalFragment() {
        // Requires empty public constructor
    }

    public static ConsultarSignalFragment newInstance() {
        return new ConsultarSignalFragment();
    }

    public interface sendConsultaCobertura {
        void sendConsultaCoberturaUI(ArrayList<CoberturaByOperadoraEntity> list, String Dpto, String prov, String distr, String Local);
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);

        // This makes sure that the container activity has implemented
        // the callback interface. If not, it throws an exception
        try {
            mCallback = (ConsultarSignalFragment.sendConsultaCobertura) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement TextClicked");
        }
    }

    @Override
    public void onDetach() {
        mCallback = null; // => avoid leaking, thanks @Deepscorn
        super.onDetach();
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mPresenter = new ConsultarSignalPresenter(this, getContext());
        mSessionManager = new SessionManager(getContext());
        mPresenterLog = new LogPresenter( getContext());
        bodyEntityLog = new BodyEntityLog();
    }

    @Override
    public void onResume() {
        super.onResume();
    }


    @Override
    public void onStart() {
        super.onStart();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.consultar_cobertura_fragment, container, false);
        unbinder = ButterKnife.bind(this, root);
        mProgressDialogCustom = new ProgressDialogCustom(getContext(), "Cargando...");
        mList = new ArrayList<>();
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                remove();
            }
        });
        spinnerProvincia.setEnabled(false);
        spinnerDistrito.setEnabled(false);
        spinnerLocalidad.setEnabled(false);
        return root;
    }

    private void remove() {
        mSessionManager.saveArrayListSendOperadoresCobertura(null);
        ActivityUtils.removeFragment(getActivity().getSupportFragmentManager(),
                this);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mProgressDialogCustom = new ProgressDialogCustom(getContext(), "Cargando...");
        mAdapter = new ConsultarSignalAdapter(new ArrayList<OperadoraEntity>(), getContext(), (VerificarLineaItem) this);
        mlinearLayoutManager = new LinearLayoutManager(getContext());
        mlinearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        rvList.setAdapter(mAdapter);
        rvList.setLayoutManager(mlinearLayoutManager);

        if (mSessionManager.getArrayListOperadores(SessionManager.ARRAY_OPERADORES) == null) {
            mPresenter.getOperadores();
        } else {
            mAdapter.setItems(mSessionManager.getArrayListOperadores(SessionManager.ARRAY_OPERADORES));
        }
        if (mSessionManager.getArrayListDepartamentos(SessionManager.ARRAY_DEPARTAMENTOS) != null) {
            mList = mSessionManager.getArrayListDepartamentos(SessionManager.ARRAY_DEPARTAMENTOS);
            getListDepartamentos();
        } else {
        }
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
        mSessionManager.saveArrayListSendOperadoresCobertura(null);

    }

    private void getListDepartamentos() {
        ArrayList<String> listDepartamentos = new ArrayList<>();
        listDepartamentos.add("Seleccionar");
        for (int i = 0; i < mList.size(); i++) {
            listDepartamentos.add(mList.get(i).getDepartamento());
        }
        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(getContext(),
                android.R.layout.simple_spinner_item, listDepartamentos);
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinnerDepartamento.setAdapter(dataAdapter);

        spinnerDepartamento.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                //   itemPositionEmpresa = position;
                codDept = spinnerDepartamento.getItemAtPosition(position).toString();
                if (!codDept.equals("Seleccionar")) {
                    mPresenter.getProvincia(codDept);
                    spinnerProvincia.setEnabled(true);
                    spinnerDepartamento.setBackground(getResources().getDrawable(R.drawable.border_edittext));
                    spinnerDistrito.setEnabled(false);
                    spinnerLocalidad.setEnabled(false);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }


    @Override
    public void getListUbicacion() {
    }

    @Override
    public void getListDepartamentos(ArrayList<DepartamentoEntity> list) {
        ArrayList<String> listDepartamentos = new ArrayList<>();
        listDepartamentos.add("Seleccionar");
        for (int i = 0; i < list.size(); i++) {
            listDepartamentos.add(list.get(i).getDepartamento());
        }
        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(getContext(),
                android.R.layout.simple_spinner_item, listDepartamentos);
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinnerDepartamento.setAdapter(dataAdapter);
        spinnerDepartamento.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                //   itemPositionEmpresa = position;
                codDept = spinnerDepartamento.getItemAtPosition(position).toString();
                if (!codDept.equals("Seleccionar")) {
                    mPresenter.getProvincia(codDept);
                    spinnerProvincia.setEnabled(true);
                    spinnerDepartamento.setBackground(getResources().getDrawable(R.drawable.border_edittext));
                    spinnerDistrito.setEnabled(false);
                    spinnerLocalidad.setEnabled(false);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }

    @Override
    public void getListOperadores(ArrayList<OperadoraEntity> list) {
        if (list != null) {
            mSessionManager.saveArrayListOperadores(list);
            mAdapter.setItems(list);
        }
    }

    @Override
    public void getListProvincia(final ArrayList<ProvinciaEntity> list) {
        ArrayList<String> listProvincia = new ArrayList<>();
        listProvincia.add("Seleccionar");
        for (int i = 0; i < list.size(); i++) {
            listProvincia.add(list.get(i).getProvincia());
        }
        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(getContext(),
                android.R.layout.simple_spinner_item, listProvincia);
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinnerProvincia.setAdapter(dataAdapter);

        spinnerProvincia.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                //   itemPositionEmpresa = position;
                codProv = spinnerProvincia.getItemAtPosition(position).toString();
                if (!codProv.equals("Seleccionar")) {
                    mPresenter.getDistrito(codDept, codProv);
                    spinnerDistrito.setEnabled(true);
                    spinnerProvincia.setBackground(getResources().getDrawable(R.drawable.border_edittext));
                    spinnerLocalidad.setEnabled(false);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }

    @Override
    public void getListDistrito(final ArrayList<DistritoEntity> list) {
        ArrayList<String> listDistrito = new ArrayList<>();
        listDistrito.add("Seleccionar");
        for (int i = 0; i < list.size(); i++) {
            listDistrito.add(list.get(i).getDistrito());
        }
        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(getContext(),
                android.R.layout.simple_spinner_item, listDistrito);
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinnerDistrito.setAdapter(dataAdapter);

        spinnerDistrito.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                //   itemPositionEmpresa = position;
                codDistr = spinnerDistrito.getItemAtPosition(position).toString();
                if (!codDistr.equals("Seleccionar")) {
                    mPresenter.getLocalidades(codDept, codProv, codDistr);
                    spinnerLocalidad.setEnabled(true);
                    spinnerDistrito.setBackground(getResources().getDrawable(R.drawable.border_edittext));
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }


    @Override
    public void getListLocalidades(final ArrayList<LocalidadEntity> list) {
        ArrayList<String> listLocalidad = new ArrayList<>();
        listLocalidad.add("Seleccionar");
        listLocalidad.add("Todos");
        for (int i = 0; i < list.size(); i++) {
            listLocalidad.add(list.get(i).getLocalidad());
        }
        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(getContext(),
                android.R.layout.simple_spinner_item, listLocalidad);
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinnerLocalidad.setAdapter(dataAdapter);

        spinnerLocalidad.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                //   itemPositionEmpresa = position;
                // mPresenter.getLocalidades(codDept,codProv, codDistr);
                codLocl = spinnerLocalidad.getItemAtPosition(position).toString();
                if (codLocl.equals("Todos")) {
                    codLocl = "0";
                    spinnerLocalidad.setBackground(getResources().getDrawable(R.drawable.border_edittext));
                } else {
                    if (!codLocl.equals("Seleccionar")) {
                        spinnerLocalidad.setBackground(getResources().getDrawable(R.drawable.border_edittext));
                    }
                }

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }

    @Override
    public void getListCobertura(ArrayList<CoberturaByOperadoraEntity> list) {
        if (list.get(0).getTecnologisOperador() != null) {
            mCallback.sendConsultaCoberturaUI(list, codDept, codProv, codDistr, codLocl);
            remove();
        }else {
            showErrorMessage("No se encontraron resultados");
        }

    }

    @Override
    public boolean isActive() {
        return isAdded();
    }

    @Override
    public void setPresenter(ConsultarSignalContract.Presenter presenter) {
        this.mPresenter = presenter;
    }

    @Override
    public void setLoadingIndicator(boolean active) {
        if (active) {
            mProgressDialogCustom.show();
        } else {
            if (mProgressDialogCustom.isShowing()) {
                mProgressDialogCustom.dismiss();
            }
        }
    }

    @Override
    public void showMessage(String message) {

    }

    @Override
    public void showErrorMessage(String message) {
        DialogConfirm dialogConfirm = new DialogConfirm(getContext(), message, true, false);
        dialogConfirm.show();
        dialogConfirm.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
    }

    @OnClick(R.id.button_filtrar_cobertura)
    public void onViewClicked() {
        sendLog(AppConstants.ACTION_CONSULT_COVERAGE_OSIPTEL_SIGNAL);
        String k = null;
        if (mSessionManager.getArrayListSendOperadoresCobertura(SessionManager.ARRAY_SEND_OPERADORES) != null) {

            if (mSessionManager.getArrayListSendOperadoresCobertura(SessionManager.ARRAY_SEND_OPERADORES).size() != 0) {

                for (int i = 0; i < mSessionManager.getArrayListSendOperadoresCobertura(SessionManager.ARRAY_SEND_OPERADORES).size(); i++) {
                    if (i == 0) {
                        k = mSessionManager.getArrayListSendOperadoresCobertura(SessionManager.ARRAY_SEND_OPERADORES).get(i);
                    } else {
                        k = mSessionManager.getArrayListSendOperadoresCobertura(SessionManager.ARRAY_SEND_OPERADORES).get(i) + "," + k;

                    }
                }

                if (mSessionManager.getUserEntity() != null) {
                    if (codDept.equals("Seleccionar") ||
                            codProv.equals("Seleccionar") ||
                            codDistr.equals("Seleccionar") ||
                            codLocl.equals("Seleccionar")) {

                        getErrorSpinner();
                        Toast.makeText(getContext(), "Por favor seleccione correctamente la ubicación a consultar", Toast.LENGTH_SHORT).show();
                    } else {
                        mPresenter.consultarCobertura(codDept, codProv, codDistr, codLocl, k,
                                mSessionManager.getUserEntity().getIdUsuario()+"",
                                mSessionManager.getUserEntity().getNroTelefono());
                    }


                } else {
                    if (codDept.equals("Seleccionar") ||
                            codProv.equals("Seleccionar") ||
                            codDistr.equals("Seleccionar") ||
                            codLocl.equals("Seleccionar")) {

                        getErrorSpinner();
                        Toast.makeText(getContext(), "Por favor seleccione correctamente la ubicación a consultar", Toast.LENGTH_SHORT).show();
                    } else {
                        mPresenter.consultarCobertura(codDept, codProv, codDistr, codLocl, k,
                                " ", " ");
                    }
                }

            } else {
                getErrorSpinner();
                Toast.makeText(getContext(), "Por favor seleccione correctamente los datos de consulta", Toast.LENGTH_SHORT).show();
            }
        } else {
            getErrorSpinner();
            Toast.makeText(getContext(), "Por favor seleccione correctamente los datos de consulta", Toast.LENGTH_SHORT).show();

        }
    }

    public void sendLog(String action){
        bodyEntityLog.setAccion(action);
        bodyEntityLog.setMac(mSessionManager.getMac());
        bodyEntityLog.setMarcaCelular(Build.BRAND);
        bodyEntityLog.setModeloCelular(Build.MODEL);
        if(mSessionManager.getUserEntity()!= null){
            bodyEntityLog.setNombreUsuario(mSessionManager.getUserEntity().getIdUsuario()+"");
            bodyEntityLog.setNumeroCelular(mSessionManager.getUserEntity().getNroTelefono());
        }else{
            bodyEntityLog.setNombreUsuario(" ");
            bodyEntityLog.setNumeroCelular(" ");
        }

        bodyEntityLog.setSistemaOperativo("Android");

        mPresenterLog.sendLog(bodyEntityLog);
    }

    private void getErrorSpinner() {
        if (codDept.equals("Seleccionar")) {
            spinnerDepartamento.setBackground(getResources().getDrawable(R.drawable.border_error_edittext));
        }

        if (codProv.equals("Seleccionar")) {
            spinnerProvincia.setBackground(getResources().getDrawable(R.drawable.border_error_edittext));
        }

        if (codDistr.equals("Seleccionar")) {
            spinnerDistrito.setBackground(getResources().getDrawable(R.drawable.border_error_edittext));
        }

        if (codLocl.equals("Seleccionar")) {
            spinnerLocalidad.setBackground(getResources().getDrawable(R.drawable.border_error_edittext));
        }
    }

    @Override
    public void clickItem(OperadoraEntity verificarLineaEntity) {

    }

}
