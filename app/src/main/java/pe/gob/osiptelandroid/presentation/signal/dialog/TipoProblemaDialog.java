package pe.gob.osiptelandroid.presentation.signal.dialog;


import android.app.AlertDialog;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;

import pe.gob.osiptelandroid.R;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class TipoProblemaDialog extends AlertDialog {
    @BindView(R.id.im_close)
    ImageView imClose;

    public TipoProblemaDialog(Context context) {
        super(context);

        LayoutInflater inflater = LayoutInflater.from(getContext());
        final View view = inflater.inflate(R.layout.dialog_tipo_problema, null);
        setView(view);
        ButterKnife.bind(this, view);
    }

    @OnClick(R.id.im_close)
    public void onViewClicked() {
        dismiss();
    }
}
