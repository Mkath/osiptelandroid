package pe.gob.osiptelandroid.presentation.main;

import android.content.Context;
import okhttp3.MediaType;
import okhttp3.RequestBody;
import pe.gob.osiptelandroid.data.entities.APIError;
import pe.gob.osiptelandroid.data.entities.AccessTokenEntity;
import pe.gob.osiptelandroid.data.entities.EnlaceAppMovilEntity;
import pe.gob.osiptelandroid.data.local.SessionManager;
import pe.gob.osiptelandroid.data.remote.ServiceFactory;
import pe.gob.osiptelandroid.data.remote.request.ListRequest;
import pe.gob.osiptelandroid.data.remote.request.LoginRequest;
import pe.gob.osiptelandroid.utils.ErrorUtils;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MenuPresenter implements MenuContract.Presenter {
    private MenuContract.View mView;
    private Context context;
    private SessionManager mSessionManager;

    public MenuPresenter(MenuContract.View mView, Context context) {
        this.context = context;
        this.mView = mView;
        this.mView.setPresenter(this);
        this.mSessionManager = new SessionManager(context);
    }

    @Override
    public void getEnlacesAppMovil(String aplicacion,String global) {
        mView.setLoadingIndicator(true);
        ListRequest listRequest = ServiceFactory.createService(ListRequest.class);
        Call<EnlaceAppMovilEntity> orders = listRequest.getEnlacesAppmovil(
                "Bearer "+mSessionManager.getUserToken(), aplicacion, global);
        orders.enqueue(new Callback<EnlaceAppMovilEntity>() {
            @Override
            public void onResponse(Call<EnlaceAppMovilEntity> call, Response<EnlaceAppMovilEntity> response) {

                if (!mView.isActive()) {
                    return;
                }
                if (response.isSuccessful()) {
                    mView.setLoadingIndicator(false);
                    if(response.body().getNombre().equals("ENLACE_COMPARATEL")){
                        mSessionManager.setEnlaceComparatel(response.body().getValor());
                    }
                    if(response.body().getNombre().equals("ENLACE_COMPARAMOVIL")){
                        mSessionManager.setEnlaceComparamovil(response.body().getValor());
                    }
                    if(response.body().getNombre().equals("ENLACE_PUNKU")){
                        mSessionManager.setEnlacePunku(response.body().getValor());
                    }

                } else {
                    mView.setLoadingIndicator(false);
                    APIError apiError = ErrorUtils.coberturaParserError(response);
                    switch (response.code()){
                        case 400:
                            mView.showErrorMessage(apiError.getMessages().get(0));
                            break;
                        case 401:
                            getRefreshToken();
                            break;
                        case 500:
                            mView.showErrorMessage("Error al realizar la operación");
                            break;
                        default:
                            mView.showErrorMessage("Error desconocido");
                            break;
                    }
                }
            }

            @Override
            public void onFailure(Call<EnlaceAppMovilEntity> call, Throwable t) {
                if (!mView.isActive()) {
                    return;
                }
                mView.setLoadingIndicator(false);
                mView.showErrorMessage("Error al conectar con el servidor");
            }
        });
    }

    @Override
    public void getRefreshToken() {
        String text = "client_id=OsitelApp&client_secret=d60d6b88-bf36-463f-ae90-b423188bf1bf&grant_type=client_credentials";
        RequestBody body =
                RequestBody.create(MediaType.parse("application/x-www-form-urlencoded"), text);
        LoginRequest loginRequest = ServiceFactory.createTokenService(LoginRequest.class);
        Call<AccessTokenEntity> orders = loginRequest.login(body);
        orders.enqueue(new Callback<AccessTokenEntity>() {
            @Override
            public void onResponse(Call<AccessTokenEntity> call, Response<AccessTokenEntity> response) {
                if (!mView.isActive()) {
                    return;
                }

                if (response.isSuccessful()) {
                    mSessionManager.setUserToken(response.body().getAccess_token());
                }else{
                    mView.showErrorMessage("Error al auntenticar su sesión, intente nuevamente por favor");
                }
            }

            @Override
            public void onFailure(Call<AccessTokenEntity> call, Throwable t) {
                if (!mView.isActive()) {
                    return;
                }
                mView.showErrorMessage("Error al conectar con el servidor");
            }
        });
    }

    @Override
    public void start() {

    }
}
