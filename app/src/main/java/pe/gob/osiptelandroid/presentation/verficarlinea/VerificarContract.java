package pe.gob.osiptelandroid.presentation.verficarlinea;

import pe.gob.osiptelandroid.core.BasePresenter;
import pe.gob.osiptelandroid.core.BaseView;
import pe.gob.osiptelandroid.data.entities.OperadoraEntity;

import java.util.ArrayList;

/**
 * Created by katherine on 12/05/17.
 */

public interface VerificarContract {
    interface View extends BaseView<Presenter> {
        void getListOperadores(ArrayList<OperadoraEntity> list);
        boolean isActive();
    }

    interface Presenter extends BasePresenter {

        void getOperadores();
        void getRefreshToken();

      /*  void loadOrdersFromPage(int page, int id);

        void loadfromNextPage(int id);

        void startLoad(int id);

        void getPromos(int page, int id);*/

    }
}
