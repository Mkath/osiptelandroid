package pe.gob.osiptelandroid.presentation.splash.dialogs;

public interface SplashInterface {

    void resend();

    void finishActivity();
}
