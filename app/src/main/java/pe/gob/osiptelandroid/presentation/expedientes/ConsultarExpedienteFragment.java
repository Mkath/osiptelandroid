package pe.gob.osiptelandroid.presentation.expedientes;

import android.content.DialogInterface;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Patterns;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.mobsandgeeks.saripaar.ValidationError;
import com.mobsandgeeks.saripaar.Validator;
import com.mobsandgeeks.saripaar.annotation.Email;
import com.mobsandgeeks.saripaar.annotation.NotEmpty;
import pe.gob.osiptelandroid.R;
import pe.gob.osiptelandroid.core.BaseFragment;
import pe.gob.osiptelandroid.data.entities.ConsultaExpedienteEntity;
import pe.gob.osiptelandroid.data.entities.EstadoEntity;
import pe.gob.osiptelandroid.data.entities.UserTrasuEntity;
import pe.gob.osiptelandroid.data.local.SessionManager;
import pe.gob.osiptelandroid.presentation.expedientes.misexpedientes.ExpedientesContract;
import pe.gob.osiptelandroid.presentation.expedientes.misexpedientes.ExpedientesFragment;
import pe.gob.osiptelandroid.presentation.expedientes.misexpedientes.ExpedientesPresenter;
import pe.gob.osiptelandroid.presentation.expedientes.registro_trasu.RegistroTrasufragment;
import pe.gob.osiptelandroid.presentation.register.dialogs.DialogConfirm;
import pe.gob.osiptelandroid.utils.ActivityUtils;
import pe.gob.osiptelandroid.utils.ProgressDialogCustom;
import pe.gob.osiptelandroid.utils.ValidateUtils;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Pattern;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;

/**
 * Created by junior on 27/08/16.
 */
public class ConsultarExpedienteFragment extends BaseFragment implements Validator.ValidationListener, ExpedientesContract.View {

    private static final String TAG = ConsultarExpedienteFragment.class.getSimpleName();
    public static final String LISTA_EXPEDIENTES = "lista_expedientes";

    @BindView(R.id.button_ingresar)
    RelativeLayout buttonIngresar;
    @BindView(R.id.button_registrar)
    RelativeLayout buttonRegistrar;
    Unbinder unbinder;
    @NotEmpty
    @Email
    @BindView(R.id.et_correo)
    EditText etCorreo;
    @NotEmpty
    @BindView(R.id.et_imei)
    EditText etConstrasenia;

    private Validator validator;
    private SessionManager mSessionManager;
    private ExpedientesContract.Presenter mPresenter;
    private ProgressDialogCustom mProgressDialogCustom;
    private String nroDoc, nombreUsuario, numeroCel, correo, contrasenia, nombreCopleto, idUsuario;
    private ValidateUtils validateUtils;

    public ConsultarExpedienteFragment() {
        // Requires empty public constructor
    }

    public static ConsultarExpedienteFragment newInstance() {
        return new ConsultarExpedienteFragment();
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        validator = new Validator(this);
        mSessionManager = new SessionManager(getContext());
        mPresenter = new ExpedientesPresenter(this, getContext());
        validateUtils = new ValidateUtils(getContext(), getActivity());
    }

    @Override
    public void onResume() {
        super.onResume();

    }


    @Override
    public void onStart() {
        super.onStart();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.consultar_expedientes_fragment, container, false);
        unbinder = ButterKnife.bind(this, root);
        mProgressDialogCustom = new ProgressDialogCustom(getContext(), "Iniciando sesión...");
        return root;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        validator.setValidationListener(this);

        if (mSessionManager.getUserTrasu() != null) {
            etCorreo.setText(mSessionManager.getUserTrasu().getCorreo());
        }

        if (mSessionManager.getUserEntity() != null) {
            if (etCorreo.getText().toString().isEmpty()) {
                etCorreo.setText(mSessionManager.getUserEntity().getCorreoElectronico());
            }
            nombreUsuario = mSessionManager.getUserEntity().getNombreCompleto();
            numeroCel = mSessionManager.getUserEntity().getNroTelefono();
        } else {
            nombreUsuario = "";
            numeroCel = "";
        }


        if (mSessionManager.getLoginTrasu()) {
            mPresenter.getMisExpedientes(
                    mSessionManager.getUserTrasu().getNroDoc(), "0",
                    "", "", nombreUsuario, numeroCel);
        }

        validateUtils.cleanUI(etCorreo, null, false, 0);
        validateUtils.cleanUI(etConstrasenia, null, false, 0);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @OnClick({R.id.button_ingresar, R.id.button_registrar})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.button_ingresar:
                correo = etCorreo.getText().toString();
                if (!validarEmail(correo)) {
                    etCorreo.setBackground(getResources().getDrawable(R.drawable.border_error_edittext));
                }
                validator.validate();
                break;
            case R.id.button_registrar:
                activateButtons(false);
                RegistroTrasufragment registroTrasufragment = RegistroTrasufragment.newInstance();
                ActivityUtils.addFragmentToFragment(getActivity().getSupportFragmentManager(),
                        registroTrasufragment, R.id.body_consultar, "ActivateExpedientes");
                break;
        }
    }

    public void activateButtons(boolean isActive) {
        buttonIngresar.setEnabled(isActive);
        buttonRegistrar.setEnabled(isActive);
        etCorreo.setEnabled(isActive);
        etConstrasenia.setEnabled(isActive);
        etConstrasenia.setText("");
        etCorreo.setBackground(getResources().getDrawable(R.drawable.border_edittext));
        etConstrasenia.setBackground(getResources().getDrawable(R.drawable.border_edittext));
    }

    @Override
    public void onValidationSucceeded() {
//  Toast.makeText(getContext(), "Yay! we got it right!", Toast.LENGTH_SHORT).show();
        if (!validarEmail(correo)) {
            etCorreo.setBackground(getResources().getDrawable(R.drawable.border_error_edittext));
        } else {
            mPresenter.validarLogin(correo,
                    etConstrasenia.getText().toString(), mSessionManager.getMac(),
                    nombreUsuario, numeroCel);
            activateButtons(false);
        }

    }

    private boolean validarEmail(String email) {
        Pattern pattern = Patterns.EMAIL_ADDRESS;
        return pattern.matcher(email).matches();
    }

    @Override
    public void onValidationFailed(List<ValidationError> errors) {
        String msg = "Por favor ingrese sus datos correctamente";
        Toast.makeText(getContext(), msg, Toast.LENGTH_LONG).show();

        for (ValidationError error : errors) {
            View view = error.getView();
            String message = error.getCollatedErrorMessage(getContext());

            // Display error messages ;)
            if (view instanceof EditText) {
                ((EditText) view).setBackground(getResources().getDrawable(R.drawable.border_error_edittext));

            } else {
                Toast.makeText(getContext(), message, Toast.LENGTH_LONG).show();
            }
        }
    }

    public void cleanView() {
        etConstrasenia.setText("");
        etCorreo.setBackground(getResources().getDrawable(R.drawable.border_edittext));
        etConstrasenia.setBackground(getResources().getDrawable(R.drawable.border_edittext));
    }

    @Override
    public void isLogin(UserTrasuEntity userTrasuEntity) {
        if (userTrasuEntity != null) {
            nroDoc = userTrasuEntity.getNroDoc();
            idUsuario = userTrasuEntity.getIdUsuario();
            nombreCopleto = userTrasuEntity.getNombre() + " " + userTrasuEntity.getApePaterno();
            userTrasuEntity.setCorreo(etCorreo.getText().toString());
            mSessionManager.setUserTrasu(userTrasuEntity);
            mPresenter.getMisExpedientes(nroDoc, "0", "", "",
                    nombreUsuario, numeroCel);
        } else {
            setLoadingIndicator(false);
        }
    }

    @Override
    public void getListMisExpedientes(ArrayList<ConsultaExpedienteEntity> list) {
        if (list != null) {
            Bundle bundle = new Bundle();
            bundle.putString("nroDoc", nroDoc);
            bundle.putString("idUsuario", idUsuario);
            bundle.putString("nombreCompleto", nombreCopleto);
            bundle.putSerializable(LISTA_EXPEDIENTES, list);
            ExpedientesFragment expedientesFragment = ExpedientesFragment.newInstance(bundle);
            // ActivityUtils.removeFragment( getActivity().getSupportFragmentManager(),
            //    this);
            ActivityUtils.addFragmentToFragment(getActivity().getSupportFragmentManager(),
                    expedientesFragment, R.id.body_consultar, "CloseSession");
            setLoadingIndicator(false);

        } else {
            activateButtons(true);
            showErrorMessage("No cuenta con expedientes registrados");
        }
    }

    @Override
    public void getMyExpediente(String urlExpediente) {

    }

    @Override
    public void loadingPdf(boolean isLoading) {

    }

    @Override
    public void getListEstados(ArrayList<EstadoEntity> list) {

    }

    @Override
    public boolean isActive() {
        return isAdded();
    }

    @Override
    public void setPresenter(ExpedientesContract.Presenter presenter) {
        this.mPresenter = presenter;
    }

    @Override
    public void setLoadingIndicator(boolean active) {
        if (active) {
            mProgressDialogCustom.show();
        } else {
            if (mProgressDialogCustom.isShowing()) {
                mProgressDialogCustom.dismiss();
            }
        }
    }

    @Override
    public void showMessage(String message) {

    }

    @Override
    public void showErrorMessage(String message) {
        DialogConfirm dialogConfirm = new DialogConfirm(getContext(), message, true, false);
        dialogConfirm.show();
        dialogConfirm.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialog) {
                activateButtons(true);
            }
        });
        dialogConfirm.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
    }
}
