package pe.gob.osiptelandroid.presentation.signal.reportar;

import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.location.LocationProvider;
import android.os.Build;
import android.os.Bundle;
import android.os.Looper;
import android.provider.Settings;
import android.support.annotation.Nullable;
import android.support.design.widget.AppBarLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.UiSettings;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import pe.gob.osiptelandroid.R;
import pe.gob.osiptelandroid.core.BaseFragment;
import pe.gob.osiptelandroid.data.entities.DistritoEntity;
import pe.gob.osiptelandroid.data.entities.LocalidadEntity;
import pe.gob.osiptelandroid.data.entities.OperadoraEntity;
import pe.gob.osiptelandroid.data.entities.ProblemaCoberturaEntity;
import pe.gob.osiptelandroid.data.entities.ProvinciaEntity;
import pe.gob.osiptelandroid.data.entities.ReportarCoberturaResponse;
import pe.gob.osiptelandroid.data.entities.ServicioOperadorEntity;
import pe.gob.osiptelandroid.data.entities.UbicationLatLongEntity;
import pe.gob.osiptelandroid.data.entities.body.BodyReporteCobertura;
import pe.gob.osiptelandroid.data.local.SessionManager;
import pe.gob.osiptelandroid.presentation.register.dialogs.DialogConfirm;
import pe.gob.osiptelandroid.utils.ActivityUtils;
import pe.gob.osiptelandroid.utils.ProgressDialogCustom;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;

/**
 * Created by junior on 27/08/16.
 */
public class MapaReportarSignalFragment extends BaseFragment implements ReportarSignalContract.View, OnMapReadyCallback, LocationListener {

    private static final String TAG = MapaReportarSignalFragment.class.getSimpleName();
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.appBarLayout)
    AppBarLayout appBarLayout;
    @BindView(R.id.im_center_location)
    ImageView imCenterLocation;
    Unbinder unbinder;
    GoogleMap mMap;
    SupportMapFragment mapFragment;
    LatLng center;
    Geocoder geocoder;
    Location mLastLocation;
    LocationRequest mLocationRequest;
    @BindView(R.id.tv_ubicacion)
    TextView tvUbicacion;
    private LocationManager mlocManager;
    private FusedLocationProviderClient mFusedLocationClient;

    private ReportarSignalContract.Presenter mPresenter;
    private SessionManager mSessionManager;
    private double latitude, longitude;
    private double latitudeSelectes, longitudeSelected;
    private ProgressDialogCustom mProgressDialogCustom;
    private isCosultarUpdateView mCallback;
    private boolean first = true;
    private BodyReporteCobertura bodyReporteCobertura;
    private AlertDialog dialogogps, dialognetwork;
    private Boolean isConsultar;

    public MapaReportarSignalFragment() {
        // Requires empty public constructor
    }

    public interface isCosultarUpdateView {
        void sendCleanView(Boolean cleanView);
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        // This makes sure that the container activity has implemented
        // the callback interface. If not, it throws an exception
        try {
            mCallback = (MapaReportarSignalFragment.isCosultarUpdateView) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement TextClicked");
        }
    }

    @Override
    public void onDetach() {
        mCallback = null; // => avoid leaking, thanks @Deepscorn
        super.onDetach();
    }


    public static MapaReportarSignalFragment newInstance(Bundle bundle) {
        MapaReportarSignalFragment fragment = new MapaReportarSignalFragment();
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mPresenter = new ReportarSignalPresenter(this, getContext());
        mSessionManager = new SessionManager(getContext());

        bodyReporteCobertura = (BodyReporteCobertura) getArguments().getSerializable("bodyReporteCobertura");
        isConsultar = getArguments().getBoolean("isConsultar");
        mFusedLocationClient = LocationServices.getFusedLocationProviderClient(getContext());
        latitudeSelectes = Double.valueOf(bodyReporteCobertura.getLatitudReporte());
        longitudeSelected = Double.valueOf(bodyReporteCobertura.getLongitudReporte());

    }

    @Override
    public void onResume() {
        super.onResume();
    }


    @Override
    public void onStart() {
        super.onStart();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.mapa_reportar_cobertura_fragment, container, false);
        unbinder = ButterKnife.bind(this, root);
        mProgressDialogCustom = new ProgressDialogCustom(getContext(), "Enviando...");
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                remove();
            }
        });
        return root;
    }

    private void remove() {
        ActivityUtils.removeFragment(getActivity().getSupportFragmentManager(),
                this);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mapFragment = (SupportMapFragment) getChildFragmentManager().findFragmentById(R.id.map);
        if (mapFragment == null) {
            FragmentManager fr = getFragmentManager();
            FragmentTransaction ft = fr.beginTransaction();
            mapFragment = SupportMapFragment.newInstance();
            ft.replace(R.id.map, mapFragment).commit();
        }
        mapFragment.getMapAsync(this);
    }

    public void getDireccion() {
        mMap.setOnCameraChangeListener(new GoogleMap.OnCameraChangeListener() {
            @Override
            public void onCameraChange(CameraPosition cameraPosition) {

                center = mMap.getCameraPosition().target;
                geocoder = new Geocoder(getActivity(), Locale.getDefault());
                try {
                    List<Address> addresses = geocoder.getFromLocation(
                            center.latitude, center.longitude, 1);
                    if (addresses.size() > 0) {
                        Address address = addresses.get(0);


                        StringBuilder sb = new StringBuilder(128);
                        sb.append(" ");
                        sb.append(address.getAddressLine(0));
                        if (addresses.size() > 2) {
                            sb.append(", ");
                            sb.append(address.getAddressLine(2));
                        }
                        tvUbicacion.setText(sb.toString());
                        longitudeSelected = center.longitude;
                        latitudeSelectes = center.latitude;
                    }
                } catch (Exception ex) {
                }
            }
        });

    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }


    @Override
    public void getListOperadores(ArrayList<OperadoraEntity> list) {

    }

    @Override
    public void getListProblemasCobertura(ArrayList<ProblemaCoberturaEntity> list) {

    }

    @Override
    public void reporteResponse(ReportarCoberturaResponse reportarCoberturaResponse) {

    }

    @Override
    public void getListProvincia(ArrayList<ProvinciaEntity> list) {

    }

    @Override
    public void getListDistrito(ArrayList<DistritoEntity> list) {

    }

    @Override
    public void getListLocalidades(ArrayList<LocalidadEntity> list) {

    }

    @Override
    public void getListServiciosOperador(ArrayList<ServicioOperadorEntity> list) {

    }

    @Override
    public void getLangLong(ArrayList<UbicationLatLongEntity> list) {

    }

    @Override
    public boolean isActive() {
        return isAdded();
    }

    @Override
    public void setPresenter(ReportarSignalContract.Presenter presenter) {
        this.mPresenter = presenter;
    }

    @Override
    public void setLoadingIndicator(boolean active) {
        if (active) {
            mProgressDialogCustom.show();
        } else {
            if (mProgressDialogCustom.isShowing()) {
                mProgressDialogCustom.dismiss();
            }
        }
    }

    @Override
    public void showMessage(String message) {
        DialogConfirm dialogConfirm = new DialogConfirm(getContext(), message, false, false);
        dialogConfirm.show();
        dialogConfirm.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialog) {
                //mSessionManager.setSignalReport(true);
                removeThis();
            }
        });
        dialogConfirm.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
    }

    private void removeThis(){
        Fragment fragment = getActivity().getSupportFragmentManager().findFragmentByTag("Reportar Signal");
        ActivityUtils.removeFragment(getActivity().getSupportFragmentManager(),
                fragment);
        ActivityUtils.removeFragment(getActivity().getSupportFragmentManager(),
                this);
        if(isConsultar){
            mCallback.sendCleanView(true);
        }
    }

    @Override
    public void showErrorMessage(String message) {
        DialogConfirm dialogConfirm = new DialogConfirm(getContext(), message, true, false);
        dialogConfirm.show();
        dialogConfirm.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
    }


    @OnClick(R.id.button_next)
    public void onViewClicked() {
        bodyReporteCobertura.setLatitudUbicacionPersona(String.valueOf(latitude));
        bodyReporteCobertura.setLongitudUbicacionPersona(String.valueOf(longitude));
        bodyReporteCobertura.setLatitudReporte(String.valueOf(latitudeSelectes));
        bodyReporteCobertura.setLongitudReporte(String.valueOf(longitudeSelected));
        bodyReporteCobertura.setSistemaOperativo("Android");
        bodyReporteCobertura.setMac(mSessionManager.getMac());
        bodyReporteCobertura.setMarcaCelular(Build.BRAND);
        bodyReporteCobertura.setModeloCelular(Build.MODEL);
        if(mSessionManager.getUserEntity()!= null){
            bodyReporteCobertura.setNombreUsuario(mSessionManager.getUserEntity().getNombreCompleto());
            bodyReporteCobertura.setNúmeroCelular(mSessionManager.getUserEntity().getNroTelefono());
        }else{
            bodyReporteCobertura.setNombreUsuario(" ");
            bodyReporteCobertura.setNúmeroCelular(" ");
        }
        mPresenter.sendReport(bodyReporteCobertura);
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        // Rest of the stuff you need to do with the map
        mMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);
        UiSettings mapUiSettings = mMap.getUiSettings();
        mapUiSettings.setCompassEnabled(false);
        mMap.getUiSettings().setRotateGesturesEnabled(false);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (ContextCompat.checkSelfPermission(getContext(), Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
                //locationStart();
                mLocationRequest = LocationRequest.create();
                mLocationRequest.setInterval(1000);
                mLocationRequest.setFastestInterval(1000);
                mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
                mFusedLocationClient.requestLocationUpdates(mLocationRequest, mLocationCallback, Looper.myLooper());
                mMap.setMyLocationEnabled(true);

            } else {
              //  if(!bodyReporteCobertura.getLatitudReporte().equals("")){
                    LatLng punto = new LatLng(Double.valueOf(bodyReporteCobertura.getLatitudReporte()), Double.valueOf(bodyReporteCobertura.getLongitudReporte()));
                    mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(punto, 12));
                    getDireccion();
              //  }else{
              //      setMapToCenter();

              //  }
                //checkLocationPermission();
            }
        }


        mMap.setOnCameraMoveListener(new GoogleMap.OnCameraMoveListener() {
            @Override
            public void onCameraMove() {
                getDireccion();
                // Toast.makeText(getContext(), "HOLIÇ", Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void setMapToCenter() {
        latitude = -12.0469294;
        longitude = -77.0437909;
        LatLng latLng = new LatLng(latitude, longitude);
        mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(latLng, 12));
        first = false;
    }

    LocationCallback mLocationCallback = new LocationCallback() {
        @Override
        public void onLocationResult(LocationResult locationResult) {
            for (Location location : locationResult.getLocations()) {
                if (getContext() != null) {
                    mLastLocation = location;
                    latitude = location.getLatitude();
                    longitude = location.getLongitude();
                    if (first) {

                        LatLng latLng = new LatLng(location.getLatitude(), location.getLongitude());
                        if(bodyReporteCobertura.getLatitudReporte().equals("")){
                            mMap.moveCamera(CameraUpdateFactory.newLatLng(latLng));
                            mMap.animateCamera(CameraUpdateFactory.zoomTo(12));
                        }else{
                            LatLng punto = new LatLng(Double.valueOf(bodyReporteCobertura.getLatitudReporte()), Double.valueOf(bodyReporteCobertura.getLongitudReporte()));
                            mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(punto, 12));
                        }

                        first = false;
                    }
                    getDireccion();
                }
            }
        }
    };


    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode) {
            case 1: {
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    if (ContextCompat.checkSelfPermission(getContext(), Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
                        mFusedLocationClient.requestLocationUpdates(mLocationRequest, mLocationCallback, Looper.myLooper());
                        mMap.setMyLocationEnabled(true);
                    }
                } else {
                    Toast.makeText(getContext(), "Please provide the permission", Toast.LENGTH_LONG).show();
                }
                break;
            }
        }
    }

    public void alertafalta() {
        AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
        builder.setTitle("Advertencia");
        builder.setMessage("El sistema GPS esta desactivado, para continuar presione el boton activar?");
        builder.setCancelable(false);
        builder.setPositiveButton("Activar", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialogo1, int id) {
                startActivity(new Intent
                        (Settings.ACTION_LOCATION_SOURCE_SETTINGS));

            }
        });
        dialogogps = builder.create();
        dialogogps.show();
    }

    public void alertafaltaNetwork() {
        AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
        builder.setTitle("Advertencia");
        builder.setMessage("El Datos desactivados, para continuar presione el boton activar?");
        builder.setCancelable(false);
        builder.setPositiveButton("Activar", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialogo1, int id) {
                startActivity(new Intent
                        (Settings.ACTION_NETWORK_OPERATOR_SETTINGS));

            }
        });
        dialognetwork = builder.create();
        dialognetwork.show();
    }

    @Override
    public void onLocationChanged(Location location) {

    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {
        switch (status) {
            case LocationProvider.AVAILABLE:
                Log.d("debug", "LocationProvider.AVAILABLE");
                break;
            case LocationProvider.OUT_OF_SERVICE:
                Log.d("debug", "LocationProvider.OUT_OF_SERVICE");
                break;
            case LocationProvider.TEMPORARILY_UNAVAILABLE:
                Log.d("debug", "LocationProvider.TEMPORARILY_UNAVAILABLE");
                break;
        }

    }

    @Override
    public void onProviderEnabled(String provider) {
        //Toast.makeText(getContext(), "GPS ACTIVADO", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onProviderDisabled(String provider) {

    }
}
