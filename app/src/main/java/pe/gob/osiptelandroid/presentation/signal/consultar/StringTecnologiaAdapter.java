package pe.gob.osiptelandroid.presentation.signal.consultar;

import android.content.Context;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import pe.gob.osiptelandroid.R;
import pe.gob.osiptelandroid.core.LoaderAdapter;
import pe.gob.osiptelandroid.data.local.SessionManager;
import pe.gob.osiptelandroid.presentation.encuestas.EncuestaItem;
import pe.gob.osiptelandroid.utils.OnClickListListener;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;


/**
 * Created by katherine on 15/05/17.
 */

public class StringTecnologiaAdapter extends LoaderAdapter<String> {



    private Context context;
    private EncuestaItem encuestaItem;
    private SessionManager mSessionManager;
    // private QuestionbyTypesAdapter mAdapter;
    private LinearLayoutManager mLayoutManager;

    public StringTecnologiaAdapter(ArrayList<String> strings, Context context) {
        super(context);
        setItems(strings);
        this.context = context;
        mSessionManager = new SessionManager(context);
    }

    public ArrayList<String> getItems() {
        return (ArrayList<String>) getmItems();
    }

    @Override
    public long getYourItemId(int position) {
        return 0;
    }

    @Override
    public RecyclerView.ViewHolder getYourItemViewHolder(ViewGroup parent) {
        View root = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_name_tecnologias, parent, false);
        return new ViewHolder(root);
    }

    @Override
    public void bindYourViewHolder(final RecyclerView.ViewHolder holder, final int position) {
        final String s = getItems().get(position);
        // Picasso.get().load
        if(!s.equals("")){
            ((ViewHolder) holder).tvLogo.setText(s);
        }else{
            ((ViewHolder) holder).tvLogo.setVisibility(View.GONE);

        }
        // mAdapter = new QuestionbyTypesAdapter(new ArrayList<String>(), context, activity);
        // ((ViewHolder)holder).rvList.setAdapter(mAdapter);
    }


    static class ViewHolder extends RecyclerView.ViewHolder {

        private OnClickListListener onClickListListener;

        @BindView(R.id.tv_logo)
        TextView tvLogo;

        ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
