package pe.gob.osiptelandroid.presentation.imei.consultar;

import android.content.Context;
import android.support.constraint.ConstraintLayout;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import pe.gob.osiptelandroid.R;
import pe.gob.osiptelandroid.core.LoaderAdapter;
import pe.gob.osiptelandroid.data.entities.ConsultaImeiEntity;
import pe.gob.osiptelandroid.data.local.SessionManager;
import pe.gob.osiptelandroid.presentation.encuestas.EncuestaItem;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;


/**
 * Created by katherine on 15/05/17.
 */

public class ImeiAdapter extends LoaderAdapter<ConsultaImeiEntity> {

    private Context context;
    private EncuestaItem encuestaItem;
    private SessionManager mSessionManager;
    private String cadena;
    private boolean init = true;
    private int row_index;

    public ImeiAdapter(ArrayList<ConsultaImeiEntity> preguntaEntities, Context context) {
        super(context);
        setItems(preguntaEntities);
        this.context = context;
        mSessionManager = new SessionManager(context);
    }

    public ArrayList<ConsultaImeiEntity> getItems() {
        return (ArrayList<ConsultaImeiEntity>) getmItems();
    }

    @Override
    public long getYourItemId(int position) {
        return getmItems().get(position).getIdTermMov();
    }

    @Override
    public RecyclerView.ViewHolder getYourItemViewHolder(ViewGroup parent) {
        View root = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_list_consulta_imei, parent, false);
        return new ViewHolder(root);
    }

    @Override
    public void bindYourViewHolder(final RecyclerView.ViewHolder holder, final int position) {
        final ConsultaImeiEntity consultaImeiEntity = getItems().get(position);
        ((ViewHolder) holder).tvEmpresaOperadora.setText(consultaImeiEntity.getOperador());
        ((ViewHolder) holder).tvTitle.setText("IMEI N° "+ consultaImeiEntity.getNe());
        ((ViewHolder) holder).tvTitleToolbar.setText("IMEI N° "+ consultaImeiEntity.getNe());
        ((ViewHolder) holder).tvEstadoEquipo.setText(consultaImeiEntity.getReporte());
        ((ViewHolder) holder).tvFecha.setText(consultaImeiEntity.getFechaHora());
        ((ViewHolder) holder).rlToolbar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((ViewHolder) holder).rlToolbar.setVisibility(View.GONE);
                ((ViewHolder) holder).rlToolbarList.setVisibility(View.VISIBLE);
                ((ViewHolder) holder).content.setVisibility(View.VISIBLE);
                row_index = position;
                notifyDataSetChanged();
                //globalPosition=getAdapterPosition();
                //notifyDataSetChanged();

            }
        });
        ((ViewHolder) holder).rlToolbarList.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((ViewHolder) holder).rlToolbar.setVisibility(View.VISIBLE);
                ((ViewHolder) holder).rlToolbarList.setVisibility(View.GONE);
                ((ViewHolder) holder).content.setVisibility(View.GONE);
            }
        });

        if (row_index == position) {
            ((ViewHolder) holder).rlToolbar.setVisibility(View.GONE);
            ((ViewHolder) holder).rlToolbarList.setVisibility(View.VISIBLE);
            ((ViewHolder) holder).content.setVisibility(View.VISIBLE);

        } else {
            ((ViewHolder) holder).rlToolbar.setVisibility(View.VISIBLE);
            ((ViewHolder) holder).rlToolbarList.setVisibility(View.GONE);
            ((ViewHolder) holder).content.setVisibility(View.GONE);
        }

    }


    static class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.tv_title_toolbar)
        TextView tvTitleToolbar;
        @BindView(R.id.rl_toolbar)
        RelativeLayout rlToolbar;
        @BindView(R.id.tv_title)
        TextView tvTitle;
        @BindView(R.id.rl_toolbar_list)
        RelativeLayout rlToolbarList;
        @BindView(R.id.tv_estado_equipo)
        TextView tvEstadoEquipo;
        @BindView(R.id.tv_empresa_operadora)
        TextView tvEmpresaOperadora;
        @BindView(R.id.tv_fecha)
        TextView tvFecha;
        @BindView(R.id.content)
        ConstraintLayout content;

        ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

    }
}
