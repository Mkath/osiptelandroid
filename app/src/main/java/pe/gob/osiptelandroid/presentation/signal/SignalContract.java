package pe.gob.osiptelandroid.presentation.signal;

import pe.gob.osiptelandroid.core.BasePresenter;
import pe.gob.osiptelandroid.core.BaseView;
import pe.gob.osiptelandroid.data.entities.EnlaceAppMovilEntity;

public interface SignalContract {
    interface View extends BaseView<Presenter> {
        void setShareLink(EnlaceAppMovilEntity response);
    }

    interface Presenter extends BasePresenter {
        void getShareLink(String aplicacion, String global);
    }
}

