package pe.gob.osiptelandroid.presentation.guia.formatos;
import pe.gob.osiptelandroid.core.BasePresenter;
import pe.gob.osiptelandroid.core.BaseView;
import pe.gob.osiptelandroid.data.entities.FormatoReclamoEntity;

import java.util.ArrayList;

/**
 * Created by katherine on 12/05/17.
 */

public interface FormatoReclamoContract {
    interface View extends BaseView<Presenter> {

        void getListFormatos(ArrayList<FormatoReclamoEntity> list);

        boolean isActive();
    }

    interface Presenter extends BasePresenter {


        void getFormatos();

        void getRefreshToken();


      /*  void loadOrdersFromPage(int page, int id);

        void loadfromNextPage(int id);

        void startLoad(int id);

        void getPromos(int page, int id);*/

    }
}
