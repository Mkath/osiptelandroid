package pe.gob.osiptelandroid.presentation.guia.formatos;

import android.Manifest;
import android.annotation.TargetApi;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.DownloadManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.FileProvider;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.MimeTypeMap;
import android.webkit.URLUtil;
import android.widget.ImageView;
import android.widget.Toast;
import pe.gob.osiptelandroid.R;
import pe.gob.osiptelandroid.core.BaseFragment;
import pe.gob.osiptelandroid.data.entities.FormatoReclamoEntity;
import pe.gob.osiptelandroid.utils.ActivityUtils;
import pe.gob.osiptelandroid.utils.ProgressDialogCustom;

import java.io.File;
import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;

/**
 * Created by junior on 27/08/16.
 */
public class FormatosReclamoFragment extends BaseFragment implements FormatoReclamoContract.View {

    private static final String TAG = FormatosReclamoFragment.class.getSimpleName();
    public static final int MY_PERMISSIONS_REQUEST_WRITE_EXTERNAL_STORAGE = 1024;

    @BindView(R.id.toolbar)
    Toolbar toolbar;

    Unbinder unbinder;
    @BindView(R.id.my_image)
    ImageView myImage;
    @BindView(R.id.pdf_fija)
    ImageView pdfFija;
    @BindView(R.id.ic_word_reclamo)
    ImageView icWordReclamo;
    @BindView(R.id.pdf_movil)
    ImageView pdfMovil;
    @BindView(R.id.word_movil)
    ImageView wordMovil;
    @BindView(R.id.pdf_otros)
    ImageView pdfOtros;
    @BindView(R.id.word_otros)
    ImageView wordOtros;
    @BindView(R.id.pdf_apelaciones)
    ImageView pdfApelaciones;
    @BindView(R.id.word_apelaciones)
    ImageView wordApelaciones;
    @BindView(R.id.pdf_quejas)
    ImageView pdfQuejas;
    @BindView(R.id.word_quejas)
    ImageView wordQuejas;

    private String urlApelaciones, urlFija, urlMovil, urlOtros, urlQuejas, url;
    private String urlApelacionesWord, urlFijaWord, urlMovilWord, urlOtrosWord, urlQuejasWord, urlWord;

    String nameOfFile;
    Long enq;
    private FormatoReclamoContract.Presenter mPresenter;
    private ProgressDialogCustom mProgressDialogCustom;
    private boolean isPDF = false;
    private boolean getPermission = true;
    public FormatosReclamoFragment() {
        // Requires empty public constructor
    }

    public static FormatosReclamoFragment newInstance() {
        return new FormatosReclamoFragment();
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mPresenter = new FormatoReclamoPresenter(this, getContext());
        getActivity().registerReceiver(onComplete, new IntentFilter(DownloadManager.ACTION_DOWNLOAD_COMPLETE));
    }

    BroadcastReceiver onComplete = new BroadcastReceiver() {
        public void onReceive(Context context, Intent intent) {
            try {
                DownloadManager.Query query = new DownloadManager.Query();
                query.setFilterById(intent.getLongExtra(DownloadManager.EXTRA_DOWNLOAD_ID, 0L));
                DownloadManager manager = (DownloadManager) context.getSystemService(Context.DOWNLOAD_SERVICE);
                Cursor cursor = manager.query(query);
                while (cursor.moveToNext()) {
                    if (cursor.getCount() > 0) {
                        int status = cursor.getInt(cursor.getColumnIndex(DownloadManager.COLUMN_STATUS));
                        if (status == DownloadManager.STATUS_SUCCESSFUL) {
                            String file = cursor.getString(cursor.getColumnIndex(DownloadManager.COLUMN_LOCAL_URI));
                            if (file != null) {
                                File mFile = new File(Uri.parse(file).getPath());
                                if (mFile.exists()) {
                                    Toast.makeText(getContext(), "Archivo descargado", Toast.LENGTH_LONG).show();
                                    if (mProgressDialogCustom.isShowing()) {
                                        mProgressDialogCustom.dismiss();
                                    }
                                    if (isPDF) {
                                        if (Build.VERSION.SDK_INT < 24) {
                                            intent = new Intent(Intent.ACTION_VIEW);
                                            intent.setDataAndType(Uri.fromFile(mFile), "application/pdf");
                                            intent.setFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
                                        } else {
                                            intent = new Intent(Intent.ACTION_VIEW);
                                            Uri photoURI = FileProvider.getUriForFile(getContext(),
                                                    getContext().getApplicationContext().getPackageName() + ".com.gob.osiptelandroid.provider", mFile);
                                            intent.setDataAndType(photoURI, "application/pdf");
                                            intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
                                            intent.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
                                            startActivity(intent);
                                        }

                                        try {
                                            if (intent.resolveActivity(getActivity().getPackageManager()) != null) {
                                                startActivity(intent);
                                            } else
                                                Toast.makeText(context, "No se encontraron aplicaciones para abrir el archivo", Toast.LENGTH_SHORT).show();
                                        } catch (Exception e) {
                                            Toast.makeText(context, e.getMessage(), Toast.LENGTH_SHORT).show();
                                        }

                                    } else {
                                        if (Build.VERSION.SDK_INT < 24) {
                                            intent = new Intent(Intent.ACTION_VIEW);
                                            intent.setDataAndType(Uri.fromFile(mFile), "application/msword");
                                            intent.setFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);

                                            try {
                                                if (intent.resolveActivity(getActivity().getPackageManager()) != null) {
                                                    startActivity(intent);
                                                } else
                                                    Toast.makeText(context, "No se encontraron aplicaciones para abrir el archivo", Toast.LENGTH_SHORT).show();
                                            } catch (Exception e) {
                                                Toast.makeText(context, e.getMessage(), Toast.LENGTH_SHORT).show();
                                            }

                                        } else {
                                            intent = new Intent(Intent.ACTION_VIEW);
                                            Uri photoURI = FileProvider.getUriForFile(getContext(),
                                                    getContext().getApplicationContext().getPackageName() + ".com.gob.osiptelandroid.provider", mFile);
                                            intent.setDataAndType(photoURI, "application/msword");
                                            intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
                                            intent.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
                                            startActivity(intent);
                                        }

                                    }

                                }
                            }
                            // So something here on success
                        } else if (status == DownloadManager.STATUS_FAILED) {
                            // int message = cursor.getInt(cursor.getColumnIndex(DownloadManager.COLUMN_REASON));
                            // So something here on failed.
                            if (mProgressDialogCustom.isShowing()) {
                                mProgressDialogCustom.dismiss();
                            }
                            Toast.makeText(getActivity(), "El archivo no se encuentra disponible", Toast.LENGTH_LONG).show();
                        }
                    }
                }
            }catch(Exception e){

            }
        }
    };

    @Override
    public void onResume() {
        super.onResume();
    }


    @Override
    public void onStart() {
        super.onStart();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.formato_reclamo_fragment, container, false);
        unbinder = ButterKnife.bind(this, root);
        mProgressDialogCustom = new ProgressDialogCustom(getContext(), "Cargando...");

        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                remove();
            }
        });
        mPresenter.getFormatos();
        return root;
    }

    private void remove() {
        ActivityUtils.removeFragment(getActivity().getSupportFragmentManager(),
                this);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @OnClick({R.id.pdf_fija, R.id.ic_word_reclamo, R.id.pdf_movil, R.id.word_movil, R.id.pdf_otros,
            R.id.word_otros, R.id.pdf_apelaciones, R.id.word_apelaciones, R.id.pdf_quejas, R.id.word_quejas})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.pdf_fija:
                url = urlFija;
                isPDF = true;
                blockUI(false);
                if (checkSelfPermission()) {
                  getDowloadUrl();
                }
                break;
            case R.id.ic_word_reclamo:
                url = urlFijaWord;
                isPDF = false;
                blockUI(false);
                if (checkSelfPermission()) {
                    getDowloadUrl();
                }
                break;
            case R.id.pdf_movil:
                url = urlMovil;
                isPDF = true;
                blockUI(false);
                if (checkSelfPermission()) {
                    getDowloadUrl();
                }
                break;
            case R.id.word_movil:
                url = urlMovilWord;
                isPDF = false;
                blockUI(false);
                if (checkSelfPermission()) {
                    getDowloadUrl();
                }
                break;
            case R.id.pdf_otros:
                url = urlOtros;
                isPDF = true;
                blockUI(false);
                if (checkSelfPermission()) {
                    getDowloadUrl();
                }
                break;
            case R.id.word_otros:
                url = urlOtrosWord;
                isPDF = false;
                blockUI(false);
                if (checkSelfPermission()) {
                    getDowloadUrl();
                }
                break;
            case R.id.pdf_apelaciones:
                url = urlApelaciones;
                isPDF = true;
                blockUI(false);
                if (checkSelfPermission()) {
                    getDowloadUrl();
                }
                break;
            case R.id.word_apelaciones:
                url = urlApelacionesWord;
                isPDF = false;
                blockUI(false);
                if (checkSelfPermission()) {
                    getDowloadUrl();
                }
                break;
            case R.id.pdf_quejas:
                url = urlQuejas;
                isPDF = true;
                blockUI(false);
                if (checkSelfPermission()) {
                    getDowloadUrl();
                }
                break;
            case R.id.word_quejas:
                url = urlQuejasWord;
                isPDF = false;
                blockUI(false);
                if (checkSelfPermission()) {
                    getDowloadUrl();
                }
                break;
        }
    }

    private void blockUI(boolean isenabled){
        pdfFija.setEnabled(isenabled);
        pdfApelaciones.setEnabled(isenabled);
        pdfMovil.setEnabled(isenabled);
        pdfOtros.setEnabled(isenabled);
        pdfQuejas.setEnabled(isenabled);
        wordApelaciones.setEnabled(isenabled);
        wordMovil.setEnabled(isenabled);
        wordOtros.setEnabled(isenabled);
        wordQuejas.setEnabled(isenabled);
        icWordReclamo.setEnabled(isenabled);
    }

    private void getDowloadUrl(){

        if (url != null) {
            downloadFile();
        } else {
            blockUI(true);
            Toast.makeText(getContext(), "El formato no se encuentra disponible", Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void getListFormatos(ArrayList<FormatoReclamoEntity> list) {
        for (int i = 0; i < list.size(); i++) {
            switch (list.get(i).getNombreArchivo()) {
                case "Apelaciones":
                    urlApelaciones = list.get(i).getDescripcionPDF();
                    urlApelacionesWord = list.get(i).getDescripcionWORD();

                    break;
                case "Movil":
                    urlMovil = list.get(i).getDescripcionPDF();
                    urlMovilWord = list.get(i).getDescripcionWORD();
                    break;
                case "Fija":
                    urlFija = list.get(i).getDescripcionPDF();
                    urlFijaWord = list.get(i).getDescripcionWORD();
                    break;
                case "Otros":
                    urlOtros = list.get(i).getDescripcionPDF();
                    urlOtrosWord = list.get(i).getDescripcionWORD();
                    break;
                case "Quejas":
                    urlQuejas = list.get(i).getDescripcionPDF();
                    urlQuejasWord = list.get(i).getDescripcionWORD();

                    break;
            }
        }
    }

    @Override
    public boolean isActive() {
        return isAdded();
    }

    @Override
    public void setPresenter(FormatoReclamoContract.Presenter presenter) {
        this.mPresenter = presenter;
    }

    @Override
    public void setLoadingIndicator(boolean active) {
        if (active) {
            mProgressDialogCustom.show();
        } else {
            if (mProgressDialogCustom.isShowing()) {
                mProgressDialogCustom.dismiss();
            }
        }
    }

    @Override
    public void showMessage(String message) {

    }

    @Override
    public void showErrorMessage(String message) {
    }

    private boolean checkSelfPermission() {
        int currentAPIVersion = Build.VERSION.SDK_INT;
        if (currentAPIVersion >= Build.VERSION_CODES.M) {
            if (ContextCompat.checkSelfPermission(getContext(), Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                if (ActivityCompat.shouldShowRequestPermissionRationale((Activity) getContext(), Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
                    AlertDialog.Builder alertBuilder = new AlertDialog.Builder(getContext());
                    alertBuilder.setCancelable(true);
                    alertBuilder.setTitle("Permisos Necesarios!");
                    alertBuilder.setMessage("Para descargar el archivo es necesario brindar los permisos correspondientes");
                    alertBuilder.setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                        @TargetApi(Build.VERSION_CODES.JELLY_BEAN)
                        public void onClick(DialogInterface dialog, int which) {
                            requestPermissions(new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, MY_PERMISSIONS_REQUEST_WRITE_EXTERNAL_STORAGE);
                        }
                    });
                    AlertDialog alert = alertBuilder.create();
                    alert.show();
                } else {
                    requestPermissions(new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, MY_PERMISSIONS_REQUEST_WRITE_EXTERNAL_STORAGE);
                }
                return false;
            } else {
                return true;
            }
        } else {
            return true;
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode) {
            case MY_PERMISSIONS_REQUEST_WRITE_EXTERNAL_STORAGE:
                if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    getDowloadUrl();
                } else {
                    blockUI(true);
                    Toast.makeText(getContext(), "Debe aceptar los permisos para iniciar la descarga", Toast.LENGTH_SHORT).show();
                    //code for deny
                }
                break;
        }
    }


    public void downloadFile() {
        mProgressDialogCustom.show();
        DownloadManager.Request request = new DownloadManager.Request(Uri.parse(url));
        request.setTitle("Descargando archivo");
        request.setDescription("File is being downloaded...");
        request.allowScanningByMediaScanner();
        nameOfFile = URLUtil.guessFileName(url, null, MimeTypeMap.getFileExtensionFromUrl(url));
        File file = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS).getPath(), nameOfFile);
        request.setNotificationVisibility(DownloadManager.Request.VISIBILITY_VISIBLE_NOTIFY_COMPLETED);
        request.setDestinationUri(Uri.fromFile(file));
        DownloadManager manager = (DownloadManager) getContext().getSystemService(Context.DOWNLOAD_SERVICE);
        manager.enqueue(request);
        blockUI(true);
    }

}
