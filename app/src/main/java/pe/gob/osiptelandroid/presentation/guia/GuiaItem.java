package pe.gob.osiptelandroid.presentation.guia;

import pe.gob.osiptelandroid.data.entities.GuiaEntity;

/**
 * Created by katherine on 24/04/17.
 */

public interface GuiaItem {

    void clickItem(GuiaEntity guiaEntity);
}
